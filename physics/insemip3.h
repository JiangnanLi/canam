#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

#if ((defined(pla) && defined(pam)) || ! defined(pla))
!
!     * jul 30, 2018 - m.lazare.     remove non-transient cpp if blocks.
!     * aug 10, 2017 - m.lazare.     no maskrow passed in call to xtemiss10.
!     * aug 07, 2017 - m.lazare.     initial git version.
!     * feb 12, 2015 - k.vonsalzen/  new version for gcm18:
!     *                m.lazare.     - modified call to new subroutine
!     *                                xtemiss10.
!     * jun 03, 2013 - k.vonsalzen/  previous version insemip2 for gcm17:
!     *                m.lazare.     - clean-up and merging with pla code.
!     *                              - revised calls to new xtemiss9,
!     *                                xtevolc4,xteob4,xteant4,xteaero4,
!     *                                xtewf5,xteob4,xtesoa3.
!     *                              - add diagnostic calculation of "ALTI".
!     *                              - removed "HISTEMI" fields.
!     * apr 28, 2012 - k.vonsalzen/  previous version insemip for gcm16.
!     *                y.peng/       - revised calls to new xtemiss8,
!     *                m.lazare.       xtevolc3,xteob3,xteant3,xteaero3,
!     *                                xttemi2,xtewf4.
!     * apr 27, 2010 - k.vonsalzen.  previous version insemi for gcm15i.
!     * apply tracer emissions.
!---------------------------------------------------------------------
!     * initialization.
!
      xemis(il1:il2,:,:)=0.
!
!---------------------------------------------------------------------
!     * calculate height and pressure.
!
      call pahgt2(pf,ph,zf,zh,throw,tfrow,shj,shtj,pressg,rgas, &
             grav,ilg,ilev,lev,il1,il2,ilevp1)
      do l=1,ilev
        dp(il1:il2,l)=pressg(il1:il2)*dshj(il1:il2,l)
      end do
      do l=1,ilev
        zfs(il1:il2,l)=zf(il1:il2,l)+phisrow(il1:il2)/grav
      end do
      zfs(il1:il2,ilev+1)=0.
!
!     * save altitude.
!
      do l=2,ilev
        altirow(il1:il2,l)=altirow(il1:il2,l) + &
                      0.5*(zfs(il1:il2,l)+zfs(il1:il2,l-1))*saverad
      end do
      l=1
      altirow(il1:il2,l)=altirow(il1:il2,l) + (zfs(il1:il2,l) + &
                    0.5*(zfs(il1:il2,l)-zfs(il1:il2,l+1)))*saverad
!
!---------------------------------------------------------------------
!     * natural emissions.
!
      call xtevolc4(xemis,pressg,dshj,zf, &
               escvrow,ehcvrow,esevrow,ehevrow,esvcrow, &
               esverow,dsuem2,psuef2,iso2,saverad,isvchem,ipam, &
               ilg,il1,il2,ilev,lev,ntrac,dtadv)
      call xtesoa3(xemis,pressg,dshj,eostrow,ipam,ogpp, &
              ioco,iocy,ilg,il1,il2,ilev,lev,ntrac,dtadv)
      call xtemiss10(xemis,xrow,ilg,il1,il2,ilev,lev,ntrac,dtadv,kount, &
                throw(1,2),gtrowbs, &
                ustarbs,gtrot(1,iowat),pressg,dshj,dmsorow,edmsrow, &
                shj,sicnrow,edsorow,edslrow,zspdso,zspdsbs, &
                flndrow,fwatrow,fnrol,spotrow,st01row, &
                st02row,st03row,st04row,st06row, &
                st13row,st14row,st15row,st16row, &
                st17row,suz0row,pdsfrow,isvdust, &
                fallrow,fa10row,fa2row,fa1row, &
                duwdrow,dustrow,duthrow,usmkrow, &
                bsfrac,smfrac,esdrow,ipam,sicn_crt, &
                saverad,isvchem)
!
!     * anthropogenic and anthropogenically influenced emissions.
!
#if defined emists
#ifndef pla
      call xttemi2(xemis,x2demisa,x2demiss,x2demisk,x2demisf, &
              fbbcrow,fairrow,pressg,dshj,zf,zfs,dtadv,levwf, &
              levair,il1,il2,lev,ilg,ilev,ntrac)
#else
      call xttemip(xemis,docem1,docem2,docem3,docem4,dbcem1, &
              dbcem2,dbcem3,dbcem4,dsuem1,dsuem2,dsuem3, &
              dsuem4,psuef1,psuef2,psuef3,psuef4, &
              sairrow,ssfcrow,sbiorow,sshirow, &
              sstkrow,sfirrow,bairrow,bsfcrow,bbiorow, &
              bshirow,bstkrow,bfirrow,oairrow,osfcrow, &
              obiorow,oshirow,ostkrow,ofirrow,fbbcrow, &
              fairrow,pressg,dshj,zf,zfs,dtadv,levwf, &
              levair,iso2,il1,il2,lev,ilg,ilev,ntrac)
#endif
#endif
!
!     * adjust tracer mixing ratios to account for emissions.
!
      xrow(il1:il2,2:lev,:)=xrow(il1:il2,2:lev,:) &
                                          +xemis(il1:il2,1:ilev,:)
!
!     * diagnose emissions.
!
#if defined xtrachem
#if defined emists
        eaisrow(il1:il2)=eaisrow(il1:il2)+sairrow(il1:il2)*saverad
        esfsrow(il1:il2)=esfsrow(il1:il2)+ssfcrow(il1:il2)*saverad
        estsrow(il1:il2)=estsrow(il1:il2)+sstkrow(il1:il2)*saverad
        efisrow(il1:il2)=efisrow(il1:il2)+sfirrow(il1:il2)*saverad
        eaibrow(il1:il2)=eaibrow(il1:il2)+bairrow(il1:il2)*saverad
        esfbrow(il1:il2)=esfbrow(il1:il2)+bsfcrow(il1:il2)*saverad
        estbrow(il1:il2)=estbrow(il1:il2)+bstkrow(il1:il2)*saverad
        efibrow(il1:il2)=efibrow(il1:il2)+bfirrow(il1:il2)*saverad
        eaiorow(il1:il2)=eaiorow(il1:il2)+oairrow(il1:il2)*saverad
        esforow(il1:il2)=esforow(il1:il2)+osfcrow(il1:il2)*saverad
        estorow(il1:il2)=estorow(il1:il2)+ostkrow(il1:il2)*saverad
        efiorow(il1:il2)=efiorow(il1:il2)+ofirrow(il1:il2)*saverad
#endif
#endif
#endif

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
