subroutine hydlabt(lc,lg,lct,lgt,lctem,lctg,lctemg, &
                         ican,icanp1,ignd,ictem,ictemp1,ntld)

  !     * feb 15/16 - m.lazare. new version for gcm19:
  !     *                       - ictemp1 passed in and used to dimension lctem
  !     *                         to handle extra "level" for soilcmas and litrcmas
  !     *                         for ctem now in class.
  !     *                       - ican also passed in to help define new "LCTG"
  !     *                         array, along with ignd and ntld..
  !     *                       - ictem,ntld,ignd used to define new "LCTEMG" array
  !     *                       - also, lctem is defined the usual non-tiled
  !     *                         way if only one land tile.
  !     * jan 26/14 - m.lazare. previous version for gcm18:
  !     *                       - ictem passed in and used to dimension lctem.
  !     *                       - im->ntld for land-only tiles.
  !     *                       - now implict none.
  !     * nov 08/11 - m.lazare. previous version hydlabt for gcm15j+ using
  !     *                       class_v3.5 and tiles.
  !     * aug 13/91 - m.lazare. previous version hydlab for gcm7 ihyd=2 (ican=4).
  !
  !     * defines level index values for canopy and sub-surface land-
  !     * surface scheme (ihyd=2) arrays.
  !     * canopy array is extended to include "URBAN" class.
  !
  implicit none

  integer, intent(in) :: ican !<
  integer, intent(in) :: icanp1 !<
  integer, intent(in) :: ignd !<
  integer, intent(in) :: ictem !<
  integer, intent(in) :: ictemp1 !<
  integer, intent(in) :: ntld !<
  integer :: k !<
  integer :: l !<
  integer :: m !<
  integer :: mlc !<
  integer :: mlg !<
  integer :: mlct !<
  integer :: mlcg !<

  integer, intent(inout) :: lc(icanp1) !<
  integer, intent(inout) :: lg(ignd) !<
  integer, intent(inout) :: lct(ntld*icanp1) !<
  integer, intent(inout) :: lgt(ntld*ignd) !<
  integer, intent(inout) :: lctem(ntld*ictemp1) !<
  integer, intent(inout) :: lctg(ntld*ignd*ican) !<
  integer, intent(inout) :: lctemg(ntld*ignd*ictem) !<
  !-----------------------------------------------------------------------
  !     * non-tiled usual input/output.
  !
  do l=1,icanp1
    lc(l)=l
  end do ! loop 10

  do l=1,ignd
    lg(l)=l
  end do ! loop 20
  !
  !     * tiled input/output.
  !
  mlc=0
  do l=1,icanp1
    do m=1,ntld
      mlc      = mlc+1
      lct(mlc) = 1000*l + m
    end do
  end do ! loop 100
  !
  mlg=0
  do l=1,ignd
    do m=1,ntld
      mlg      = mlg+1
      lgt(mlg) = 1000*l + m
    end do
  end do ! loop 200
  !
  mlcg=0
  do l=1,ican
    do k=1,ignd
      do m=1,ntld
        mlcg     = mlcg+1
        lctg(mlcg) = 1000*l + k*m
      end do
    end do
  end do
  !
  if (ntld==1) then
    do l=1,ictemp1
      lctem(l)=l
    end do
    !
    mlcg=0
    do l=1,ican
      do k=1,ignd
        mlcg         = mlcg+1
        lctemg(mlcg) = 1000*l + k
      end do
    end do
  else
    mlct=0
    do l=1,ictemp1
      do m=1,ntld
        mlct        = mlct+1
        lctem(mlct) = 1000*l + m
      end do
    end do
    !
    mlcg=0
    do l=1,ictem
      do k=1,ignd
        do m=1,ntld
          mlcg         = mlcg+1
          lctemg(mlcg) = 1000*l + k*m
        end do
      end do
    end do
  end if

  return
end
