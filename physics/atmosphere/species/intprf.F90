!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine intprf(fbbcrow,fbbcrol,fairrow,fairrol, &
                  ilg,il1,il2,levwf,levair,delt,gmt,iday,mday)
  !
  !     * knut von salzen - jul 27,2009. new routine to do interpolation
  !     *                                of fbbc and fair, based on inthem.
  implicit none
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmsec
  real, intent(in) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: levair      !< Number of vertical layers for aircraft emissions input data \f$[unitless]\f$
  integer, intent(in) :: levwf      !< Number of vertical layers for wildfire emissions input data \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: sec0
  real :: secsm
  !
  real, intent(inout), dimension(ilg,levwf)  :: fbbcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levwf)  :: fbbcrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,levair) :: fairrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levair) :: fairrol !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  sec0 = real(iday) * 86400. + gmt
  !
  fmsec = real(mday) * 86400.
  if (fmsec < sec0) fmsec = fmsec + 365. * 86400.
  !
  secsm = fmsec - sec0
  !
  !     * general interpolation.
  !
  do l = 1,levwf
    do i = il1,il2
      fbbcrow(i,l) = ((secsm - delt) * fbbcrow(i,l) + delt * fbbcrol(i,l)) &
                     /secsm
    end do
  end do ! loop 20
  do l = 1,levair
    do i = il1,il2
      fairrow(i,l) = ((secsm - delt) * fairrow(i,l) + delt * fairrol(i,l)) &
                     /secsm
    end do
  end do ! loop 30
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
