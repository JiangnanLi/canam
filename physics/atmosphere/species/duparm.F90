!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
module duparm
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     basic constants and arrays for dust emission.
  !
  !     history:
  !     --------
  !     * aug 10/2007 - y. peng   new.
  !
  !-----------------------------------------------------------------------
  !
  implicit none
  !
  !       * array dimensions
  !
  integer, parameter :: ntrace = 8 ! number of tracers
  integer, parameter :: nbin = 24 ! number of bins per tracer
  integer, parameter :: nclass = ntrace * nbin ! number of particle classes
  integer, parameter :: nats = 17 ! number of soil types
  integer, parameter :: nmode = 4 ! number of soil population mode
  integer, parameter :: nspe = nmode * 3 + 2
  !
  !       * size indices for accu. and coarse modes /cutoff radius between ai and ci is 0.68um, cutoff for ci is 9.24um
  !
  integer, parameter :: minai = 1
  integer, parameter :: maxai = 3
  integer, parameter :: minci = 4
  integer, parameter :: maxci = 6
  !
  !
  !       * soil particle size parameters (cm) /midified for gcm15i and pla (0.05~54.83um)
  !
  real, parameter :: dmin = 0.00001 ! minimum particules diameter [cm]
  real, parameter :: dmax = 0.011 ! maximum particules diameter [cm]
  real, parameter :: dstep = 0.0365 ! diameter increment [cm]
  !
  !       * reynolds constants and threshold value
  !
  real, parameter :: a_rnolds = 1331.647
  real, parameter :: b_rnolds = 0.38194
  real, parameter :: x_rnolds = 1.561228
  real, parameter :: d_thrsld = 0.00000231
  !
  !       * air and soil particle density (g/cm3)
  !
  real, parameter :: roa = 0.001227
  real, parameter :: rop = 2.65
  !
  real, parameter :: umin = 21. ! minimum threshold friction wind speed [cm/s]
  !
  real, parameter :: vk = 0.4 ! von karman constant
  !
  !       * efficient fraction
  !
  real, parameter :: aeff = 0.35
  real, parameter :: xeff = 10.
  !
  real, parameter :: zz = 1000. ! wind measure height [cm]
  !
  !       * surface roughness related parameters
  !
  real, parameter :: z0s = 0.001
  real, parameter :: z01 = 0.001
  real, parameter :: z02 = 0.001
  !
  real, parameter :: cuscale = 0.85 ! scale factor for wind stress threshold
  !
  real, parameter :: pi = 3.141592653589793
  real, parameter :: gravi = 9.8 * 100. ! gravity acceleration in [cm/s2]
  !
  real, parameter :: w0 = 0.25 ! soil moisture threshold
  logical         :: cccmabf = .true. ! use cccma bare soil fraction or not
  !
  !      * soil type data
  !
  !      solspe --> soil caracteristics:
  !      zobler texture classes
  !      solspe: for 4 populations : values = 3*(dmed sig p)
  !                                          ratio of fluxes
  !                                          residual moisture
  !      populations: coarse sand, medium/fine sand, silt, clay
  !
  !       soil type 1 : coarse
  !       soil type 2 : medium
  !       soil type 3 : fine
  !       soil type 4 : coarse medium
  !       soil type 5 : coarse fine
  !       soil type 6 : medium fine
  !       soil type 7 : coarse_dp, medium_dp, fine
  !       soil type 8 : organic
  !       soil type 9 : ice
  !       soil type 10 : potential lakes (additional)
  !       soil type 11 : potential lakes (clay)
  !       soil type 12 : potential lakes australia
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real,  dimension(nspe,nats) :: solspe
  !
  data solspe / &
      0.0707, 2.0, 0.43, 0.0158, 2.0, 0.40, 0.0015, &
      2.0, 0.17, 0.0002, 2.0, 0.00, 2.1e-06, 0.20, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.37, 0.0015, &
      2.0, 0.33, 0.0002, 2.0, 0.30, 4.0e-06, 0.25, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
      2.0, 0.33, 0.0002, 2.0, 0.67, 1.0e-07, 0.50, &
      0.0707, 2.0, 0.10, 0.0158, 2.0, 0.50, 0.0015, &
      2.0, 0.20, 0.0002, 2.0, 0.20, 2.7e-06, 0.23, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.50, 0.0015, &
      2.0, 0.12, 0.0002, 2.0, 0.38, 2.8e-06, 0.25, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.27, 0.0015, &
      2.0, 0.25, 0.0002, 2.0, 0.48, 1.0e-07, 0.36, &
      0.0707, 2.0, 0.23, 0.0158, 2.0, 0.23, 0.0015, &
      2.0, 0.19, 0.0002, 2.0, 0.35, 2.5e-06, 0.25, &
      0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015, &
      2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50, &
      0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015, &
      2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
      2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015, &
      2.0, 0.00, 0.0002, 2.0, 1.00, 1.0e-05, 0.25, &
      0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0027, &
      2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25, &
      0.0442, 1.5, 0.03, 0.0084, 1.5, 0.85, 0.0015, &
      2.0, 0.11, 0.0002, 2.0, 0.02, 1.9e-06, 0.12, &
      0.0450, 1.5, 0.00, 0.0070, 1.5, 0.33, 0.0015, &
      2.0, 0.50, 0.0002, 2.0, 0.17, 1.9e-04, 0.15, &
      0.0457, 1.8, 0.31, 0.0086, 1.5, 0.22, 0.0015, &
      2.0, 0.34, 0.0002, 2.0, 0.12, 3.9e-05, 0.13, &
      0.0293, 1.8, 0.39, 0.0090, 1.5, 0.16, 0.0015, &
      2.0, 0.35, 0.0002, 2.0, 0.10, 3.1e-05, 0.13, &
      0.0305, 1.5, 0.46, 0.0101, 1.5, 0.41, 0.0015, &
      2.0, 0.10, 0.0002, 2.0, 0.03, 2.8e-06, 0.12/
  !
end module duparm
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
