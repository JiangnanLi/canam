!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine dustemi3(flndrow,gtrow,pressg,dshj,smfrac,fnrol,bsfrac, &
                    zspdsbs,ustarbs,suz0row,pdsfrow, &
                    spotrow,st01row,st02row,st03row,st04row,st06row, &
                    st13row,st14row,st15row,st16row,st17row, &
                    esdrow,xemis,idua,iduc,saverad,isvdust, &
                    uth,srel,srelv,su_srelv,fallrow,fa10row,fa2row,fa1row, &
                    duwdrow,dustrow,duthrow,usmkrow, &
                    ilg,il1,il2,ilev,lev,ntrac,ztmst,cuscale)
  !
  !     * this routine calculates the surface dust emission flux
  !     * in accumulation and coarse modes
  !
  !     * sep 13/2014 - k. vonsalzen/ new version for gcm18+:
  !     *               m. lazare:    - ustarbs passed in and used
  !     *                               to define "ustar", instead of
  !     *                               previous definition.
  !     *                             - passes in and uses flndrow
  !     *                               instead of {gcrow,maskrow}.
  !     *                             - bare soil surface wind speed
  !     *                               (with gustiness effects) passed in
  !     *                               (zspdsbs) instead of calculating
  !     *                               via {sfcurow,sfcvrow,gustrol}
  !     *                               which are now removed.
  !     * jun 18/2013 - k. vonsalzen/ previous version dustemi2 for gcm17+:
  !     *               m. lazare:    - uses "duparm1" insted of "duparm",
  !     *                               which has "cuscale" moved else where
  !     *                               in the code allowing "duparm1" to
  !     *                               be used by both bulk and pla aerosols.
  !     *                               "cuscale" is now passed in the call.
  !     * aug 20/2007 - y. peng.      previous version dustemi for gcm16:
  !     *                             new scheme incl. soil texture:
  !     *                             - use zobler soil properties
  !     *                             - use marticorena et al 1997 to
  !     *                               calculate the threshold wind speed
  !     *                             - work arrays now local.
  !     *
  !
  use duparm1
  !
  implicit none
  real :: asq
  real :: cd
  real :: cpres
  real, intent(in) :: cuscale
  real :: fac
  real :: grav
  integer, intent(in) :: idua
  integer, intent(in) :: iduc
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer, intent(in) :: isvdust
  integer, intent(in) :: lev
  integer :: n
  integer, intent(in) :: ntrac
  real :: rayon
  real :: rgas
  real :: rgoasq
  real :: rgocp
  real, intent(in) :: saverad
  real :: tw
  real :: ww
  real, intent(in) :: ztmst
  !
  !      * i/o fields.
  !
  real, intent(in), dimension(ilg) :: flndrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gtrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: smfrac !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: fnrol !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bsfrac !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zspdsbs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ustarbs !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: suz0row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pdsfrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: spotrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st01row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st02row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st03row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st04row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st06row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st13row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st14row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st15row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st16row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: st17row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: esdrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !
  real, intent(inout), dimension(ilg) :: fallrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fa10row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fa2row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fa1row !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: duwdrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: usmkrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dustrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: duthrow !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !      * internal work fields.
  !
  real, intent(in), dimension(nclass) :: uth
  real, intent(in), dimension(nats,nclass) :: srel
  real, intent(in), dimension(nats,nclass) :: srelv
  real, intent(in), dimension(nats,nclass) :: su_srelv
  !
  real, dimension(ilg) :: c_eff
  real, dimension(ntrace) :: dpk
  real, dimension(ntrace) :: dbmin
  real, dimension(ntrace) :: dbmax
  real, dimension(ilg) :: barefrac
  real, dimension(ilg) :: dust_mask
  real, dimension(ilg) :: ustar_d
  real, dimension(ilg) :: du_wind
  real, dimension(ilg) :: du_th
  real, dimension(ilg) :: ustar_acrit
  real, dimension(ilg) :: duz01
  real, dimension(ilg) :: duz02
  !
  real, dimension(ilg,ntrace) :: flux_6h
  real, dimension(ilg,ntrace) :: fluxbin
  real, dimension(ilg) :: fluxall
  real, dimension(ilg) :: fluxa10
  real, dimension(ilg) :: fluxa2
  real, dimension(ilg) :: fluxa1
  real, dimension(ilg) :: flux_ai
  real, dimension(ilg) :: flux_ci
  real, dimension(ilg,nclass) :: fluxtyp
  real, dimension(ilg,nclass) :: fluxdiam1
  real, dimension(ilg,nclass) :: fluxdiam2
  real, dimension(ilg,nclass) :: fluxdiam3
  real, dimension(ilg,nclass) :: fluxdiam4
  real, dimension(ilg,nclass) :: fluxdiam6
  real, dimension(ilg,nclass) :: fluxdiam_pf
  real, dimension(ilg,nclass) :: fluxdiam13
  real, dimension(ilg,nclass) :: fluxdiam14
  real, dimension(ilg,nclass) :: fluxdiam15
  real, dimension(ilg,nclass) :: fluxdiam16
  real, dimension(ilg,nclass) :: fluxdiam17
  !
  !      * auxiliary variables
  !
  real :: aaa
  real :: bb
  real :: ccc
  real :: ddd
  real :: ee
  real :: ff
  real :: d1
  real :: feff
  real :: rdp
  real :: dlast
  real :: ustar
  real, dimension(ilg,nats) :: uthp
  real :: alpha
  real, dimension(nats) :: waa
  real, dimension(ilg) :: wbb
  real :: fdp1
  real :: fdp2
  !
  integer :: il
  integer :: j
  integer :: kk
  integer :: nn
  integer :: i_soil
  integer :: kkmin
  integer :: kkk
  integer :: dk(nclass - 1)
  !
  common /params/ ww,tw,rayon,asq,grav,rgas,rgocp,rgoasq,cpres
  !
  integer :: zschm
  integer :: wschm
  integer :: solschm
  !        z0 scheme, 1: new scheme prigent et al 2002, 0: old scheme z01=z02=0.001
  data zschm /1/
  !        soil water scheme, 1: new scheme fecan, 0: old scheme 0.99
  data wschm /1/
  !        soil property scheme, 1: new scheme cheng, 0: old scheme
  data solschm /1/
  !
  !    --------------------------------------------------------------
  !
  !     * initial calculation (c_eff)
  !
  !      ! effective fraction calculation (roughness z0 related)
  !      ! constant z0 temporarily
  !      ! marticorena et al 1997 equations (15)-(17)
  !
  !      ! initial values ! d1 is set to zero temporarily
  !
  d1 = 0.
  feff = 0.
  aaa = 0.
  bb = 0.
  ccc = 0.
  ddd = 0.
  ee = 0.
  ff = 0.
  do il = il1,il2
    c_eff(il)       = 0.0
    duz01(il)       = 0.0
    duz02(il)       = 0.0
  end do
  !
  do il = il1,il2
    !
    if (zschm == 1) then
      !       new z0 scheme, monthly average
      duz01(il) = suz0row(il)
      duz02(il) = suz0row(il)
    else
      !       old z0 scheme
      duz01(il) = z01
      duz02(il) = z02
    end if
    !
    if (duz01(il) == 0. .or. duz01(il) < z0s .or. duz02(il) < z0s) then
      feff = 0.
    else
      aaa = log(duz01(il)/z0s)
      bb = log(aeff * (xeff/z0s) ** 0.8)
      ccc = 1. - aaa/bb
      if (d1 == 0.) then
        ff = 1.
      else
        ddd = log(duz02(il)/duz01(il))
        ee = log(aeff * (d1/duz01(il)) ** 0.8)
        ff = 1. - ddd/ee
      end if  ! d1=0
      feff = ff * ccc
      if (feff < 0.) feff = 0.
      if (feff > 1.) feff = 1.
    end if  ! z01=0
    !
    c_eff(il) = feff
    !
  end do  ! il
  !
  !     * initial calculation (c_eff) done
  !
  !   --------------------------------------------------------------
  !
  !     * initial calculation (winds and masks)
  !
  !      ! initial values
  !
  ustar = 0.0
  rdp = 0.0
  dlast = 0.0
  do il = il1,il2
    uthp(il,:)      = 0.0
    dust_mask(il)   = 0.0
    ustar_d(il)     = 0.0
    du_wind(il)     = 0.0
    du_th(il)       = 0.0
    ustar_acrit(il) = 0.0
    !
    !      bsfrac is the bare soil fraction passed from cccma gcm land scheme class (default).
    !      bsfrac is combined with the soil moisture criterion (w0) to obtain the potential dust
    !      source area.
    !      pdsfrow is a prescribed potential dust source areal :: fraction obtained from an offline
    !      terrestrial model, in which the soil moisture & snow cover criteria were satisfied.
    !      pdsfrow is only applied for the purpose of dust parameterization development.
    !
    if (cccmabf) then
      barefrac(il) = bsfrac(il)
    else
      barefrac(il) = pdsfrow(il)
    end if
    !
  end do
  !
  !      ! calculate bin minimum/maximum radius in um
  !      ! rdp is diameter in cm
  !
  rdp = dmin
  dlast = dmin
  do nn = 1,ntrace
    dpk(nn) = 0.
    dbmin(nn) = 0.
    dbmax(nn) = 0.
  end do
  !
  nn = 1
  do kk = 1,nclass
    if (mod(kk,nbin) == 0) then
      dbmax(nn) = rdp * 5000.
      dbmin(nn) = dlast * 5000.
      dpk(nn) = sqrt(dbmax(nn) * dbmin(nn))
      nn = nn + 1
      dlast = rdp
    end if
    rdp = rdp * exp(dstep)
  end do
  !
  !      ! calculate the friction wind speed ustar
  !
  do il = il1,il2
    du_wind(il) = zspdsbs(il)
    if (duz02(il) == 0.) then
      ustar = 0.
    else
      ustar = 100. * ustarbs(il)
    end if
    ustar_d(il) = ustar
    !
    !         if (c_eff(il) == 0.) then
    du_th(il) = 0.
    !         else
    !          du_th(il)=umin*cuscale/c_eff(il)
    !         end if
    !
    !      ! check critical wind speed (wind stress threshold scaled)
    if (ustar > 0. .and. ustar >= du_th(il)) then
      !      ! check if the grid cell is a potential dust source (exclude vegetated area)
      if (barefrac(il) > 0.0 .and. duz02(il) > 0.0) then
        !      ! set grid point as a potential dust source point
        dust_mask(il) = 1.
      end if  ! bare fraction
      !      ! store the time fraction with ustar > dust threshold for output
      ustar_acrit(il) = 1.
    else
      ustar_acrit(il) = 0.
    end if   ! ustar
    !
  end do    ! il
  !
  !     * initial calculation (winds and masks) done
  !
  !   --------------------------------------------------------------
  !
  !     * emission flux calculation
  !
  !      ! initialization
  !
  alpha = 0.0
  fdp1 = 0.0
  fdp2 = 0.0
  do j = 1,nats
    waa(j) = 0.0
  end do
  do il = il1,il2
    wbb(il) = 0.0
    !
    do nn = 1,ntrace
      fluxbin(il,nn) = 0.
      flux_6h(il,nn) = 0.
    end do
    !
    do kk = 1,nclass
      fluxtyp(il,kk) = 0.
      fluxdiam1(il,kk) = 0.
      fluxdiam2(il,kk) = 0.
      fluxdiam3(il,kk) = 0.
      fluxdiam4(il,kk) = 0.
      fluxdiam6(il,kk) = 0.
      fluxdiam_pf(il,kk) = 0.
      fluxdiam13(il,kk) = 0.
      fluxdiam14(il,kk) = 0.
      fluxdiam15(il,kk) = 0.
      fluxdiam16(il,kk) = 0.
      fluxdiam17(il,kk) = 0.
    end do
    !
  end do ! il
  !
  !      ! calculation
  !
  cd = 1. * roa/gravi
  !
  do kk = 1,nclass
    do il = il1,il2
      if (dust_mask(il) == 1 .and. c_eff(il) > 0.) then
        !
        !   ! soil moisture modification
        !
        if (wschm == 1) then
          !          new ws scheme
          if (flndrow(il) > 0. .and. gtrow(il) > 273.15) then
            wbb(il) = min(smfrac(il)/rop,1.) * 100.
          else
            wbb(il) = 0.
          end if
          do j = 1,nats
            waa(j) = 0.0014 * (solspe(nspe - 2,j) * 100.) ** 2 + 0.17 * &
                     (solspe(nspe - 2,j) * 100.)
            if (wbb(il) <= waa(j) .or. waa(j) == 0.) then
              uthp(il,j) = uth(kk)
            else
              uthp(il,j) = uth(kk) * sqrt(1. + 1.21 * (wbb(il) - waa(j)) ** 0.68)
            end if
          end do ! j
        else ! soil moisture
          !          old ws scheme
          do j = 1,nats
            uthp(il,j) = uth(kk)
          end do ! j
        end if ! soil moisture
        !
        !    ! fluxdiam calculation (fdp1,fdp2 follows marticorena)
        !
        !          ! flux for soil type #1
        i_soil = 1
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam1(il,kk) = max(0.,srel(i_soil,kk) &
                             * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! flux for soil type #2
        i_soil = 2
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam2(il,kk) = max(0.,srel(i_soil,kk) &
                             * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! flux for soil type #3
        i_soil = 3
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam3(il,kk) = max(0.,srel(i_soil,kk) &
                             * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! flux for soil type #4
        i_soil = 4
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam4(il,kk) = max(0.,srel(i_soil,kk) &
                             * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! flux for soil type #6
        i_soil = 6
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam6(il,kk) = max(0.,srel(i_soil,kk) &
                             * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! flux for soil type in preferential source area
        i_soil = 10
        fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
        fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
        if (fdp1 > 0.) then
          alpha = solspe(nmode * 3 + 1,i_soil)
          fluxdiam_pf(il,kk) = max(0.,srel(i_soil,kk) &
                               * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
        end if
        !
        !          ! add asian soil types
        if (solschm == 1) then
          !          ! flux for soil type #13
          i_soil = 13
          fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
          fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
          if (fdp1 > 0.) then
            alpha = solspe(nmode * 3 + 1,i_soil)
            fluxdiam13(il,kk) = max(0.,srel(i_soil,kk) &
                                * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
          end if
          !
          !          ! flux for soil type #14
          i_soil = 14
          fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
          fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
          if (fdp1 > 0.) then
            alpha = solspe(nmode * 3 + 1,i_soil)
            fluxdiam14(il,kk) = max(0.,srel(i_soil,kk) &
                                * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
          end if
          !
          !          ! flux for soil type #15
          i_soil = 15
          fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
          fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
          if (fdp1 > 0.) then
            alpha = solspe(nmode * 3 + 1,i_soil)
            fluxdiam15(il,kk) = max(0.,srel(i_soil,kk) &
                                * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
          end if
          !
          !          ! flux for soil type #16
          i_soil = 16
          fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
          fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
          if (fdp1 > 0.) then
            alpha = solspe(nmode * 3 + 1,i_soil)
            fluxdiam16(il,kk) = max(0.,srel(i_soil,kk) &
                                * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
          end if
          !
          !          ! flux for soil type #17
          i_soil = 17
          fdp1 = (1. - (uthp(il,i_soil)/(c_eff(il) * ustar_d(il))))
          fdp2 = (1. + (uthp(il,i_soil)/(c_eff(il) * ustar_d(il)))) ** 2
          if (fdp1 > 0.) then
            alpha = solspe(nmode * 3 + 1,i_soil)
            fluxdiam17(il,kk) = max(0.,srel(i_soil,kk) &
                                * fdp1 * fdp2 * cd * ustar_d(il) ** 3 * alpha)
          end if
          !
        end if ! asian soil
        !
        !    ! initialize fluxtyp of all soil types at first bin (kk=1)
        !
        if (kk == 1) then
          if (solschm == 1) then
            fluxtyp(il,kk) = fluxtyp(il,kk) &
                             + fluxdiam1(il,kk) * st01row(il) &
                             + fluxdiam2(il,kk) * st02row(il) &
                             + fluxdiam3(il,kk) * st03row(il) &
                             + fluxdiam4(il,kk) * st04row(il) &
                             + fluxdiam6(il,kk) * st06row(il) &
                             + fluxdiam13(il,kk) * st13row(il) &
                             + fluxdiam14(il,kk) * st14row(il) &
                             + fluxdiam15(il,kk) * st15row(il) &
                             + fluxdiam16(il,kk) * st16row(il) &
                             + fluxdiam17(il,kk) * st17row(il) &
                             + fluxdiam_pf(il,kk) * spotrow(il)
          else
            fluxtyp(il,kk) = fluxtyp(il,kk) &
                             + fluxdiam1(il,kk) * st01row(il) &
                             + fluxdiam2(il,kk) * st02row(il) &
                             + fluxdiam3(il,kk) * st03row(il) &
                             + fluxdiam4(il,kk) * st04row(il) &
                             + fluxdiam6(il,kk) * st06row(il) &
                             + fluxdiam_pf(il,kk) * spotrow(il)
          end if ! asian soil
        else
          dk(kk - 1) = kk          ! store dislocated particle class
        end if  ! kk=1
        !
      end if     ! dust_mask=1 and c_eff>1
    end do      ! il
  end do       ! kk
  !
  !      ! calculate fluxtyp of all soil types for all 192 bins (kkk=2~192)
  !
  kkmin = 1
  do il = il1,il2
    if (dust_mask(il) == 1 .and. c_eff(il) > 0.) then
      do n = 1,nclass - 1
        do kkk = 1,dk(n)           ! scaling with relative contribution of dust size fraction
          i_soil = 1
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + st01row(il) &
                            * fluxdiam1(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          i_soil = 2
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + st02row(il) &
                            * fluxdiam2(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          i_soil = 3
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + st03row(il) &
                            * fluxdiam3(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          i_soil = 4
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + st04row(il) &
                            * fluxdiam4(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          i_soil = 6
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + st06row(il) &
                            * fluxdiam6(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          if (du_wind(il) > 10.)  i_soil = 11
          if (du_wind(il) <= 10.)  i_soil = 10
          fluxtyp(il,kkk) = fluxtyp(il,kkk) + spotrow(il) &
                            * fluxdiam_pf(il,dk(n)) * srelv(i_soil,kkk)/ &
                            ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
          !
          if (solschm == 1) then
            i_soil = 13
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + st13row(il) &
                              * fluxdiam13(il,dk(n)) * srelv(i_soil,kkk)/ &
                              ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
            !
            i_soil = 14
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + st14row(il) &
                              * fluxdiam14(il,dk(n)) * srelv(i_soil,kkk)/ &
                              ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
            !
            i_soil = 15
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + st15row(il) &
                              * fluxdiam15(il,dk(n)) * srelv(i_soil,kkk)/ &
                              ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
            !
            i_soil = 16
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + st16row(il) &
                              * fluxdiam16(il,dk(n)) * srelv(i_soil,kkk)/ &
                              ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
            !
            i_soil = 17
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + st17row(il) &
                              * fluxdiam17(il,dk(n)) * srelv(i_soil,kkk)/ &
                              ((su_srelv(i_soil,dk(n)) - su_srelv(i_soil,kkmin)))
            !
          end if ! asian soil
          !
        end do  ! kkk
      end do   ! n
    end if    ! dust_mask
  end do      ! il
  !
  !      ! calculate fluxes in each internal tracer classes (8)
  !
  do nn = 1,ntrace
    do il = il1,il2
      if (dust_mask(il) == 1 .and. c_eff(il) > 0.) then
        do kk = (nn - 1) * nbin + 1,nn * nbin
          fluxbin(il,nn) = fluxbin(il,nn) + fluxtyp(il,kk)
        end do
        ! mask out dust fluxes, where soil moisture threshold is reached
        if (smfrac(il) > w0) fluxbin(il,nn) = 0.
        ! fluxbin: g/cm2/sec; flux_6h: g/m2/sec
        ! exclude snow covered region and weighted by vegetation cover
        if (fnrol(il) > 0.1) fluxbin(il,nn) = 0.
        flux_6h(il,nn) = fluxbin(il,nn) * 10000. * barefrac(il)
      end if ! dust_mask
    end do  ! il
  end do   ! nn
  !
  !     * emission flux calculation done
  !
  !   --------------------------------------------------------------
  !
  !     * diagnostic calculation for model output
  !
  !      ! initialization for output arrays
  !
  do il = il1,il2
    fluxall(il) = 0.
    fluxa10(il) = 0.
    fluxa2(il) = 0.
    fluxa1(il) = 0.
    flux_ai(il) = 0.
    flux_ci(il) = 0.
  end do
  !
  do il = il1,il2
    if (dust_mask(il) == 1 .and. c_eff(il) > 0.) then
      !
      !       ! unit of emission flux: flux_6h [g/m2/sec]
      !
      do nn = 1,ntrace
        fluxall(il) = fluxall(il) + flux_6h(il,nn)
        if (dbmax(nn) <= 10.) fluxa10(il) = fluxa10(il) + flux_6h(il,nn)
        if (dbmax(nn) <= 2.) fluxa2(il) = fluxa2(il) + flux_6h(il,nn)
        if (dbmax(nn) <= 1.) fluxa1(il) = fluxa1(il) + flux_6h(il,nn)
      end do
      !
      !       ! sum over internal tracer classes for ai and ci modes
      !
      !       ! accumulation mode
      !
      do nn = minai,maxai
        flux_ai(il) = flux_ai(il) + flux_6h(il,nn)
      end do
      !
      !       ! coarse mode
      !
      do nn = minci,maxci
        flux_ci(il) = flux_ci(il) + flux_6h(il,nn)
      end do
      !
      !       ! emission mass flux: g/m2/sec -> kg/m2/sec
      !
      flux_ai(il) = max(flux_ai(il) * 1.e-3,0.)
      flux_ci(il) = max(flux_ci(il) * 1.e-3,0.)
      fluxall(il) = max(fluxall(il) * 1.e-3,0.)
      fluxa10(il) = max(fluxa10(il) * 1.e-3,0.)
      fluxa2(il) = max(fluxa2(il) * 1.e-3,0.)
      fluxa1(il) = max(fluxa1(il) * 1.e-3,0.)
      !
      !       ! save dust emission fluxes
      !
      fac = ztmst * grav * flndrow(il)/(dshj(il,ilev) * pressg(il))
      xemis(il,ilev,idua) = xemis(il,ilev,idua) + flux_ai(il) * fac
      xemis(il,ilev,iduc) = xemis(il,ilev,iduc) + flux_ci(il) * fac
      !
      if (isvdust > 0) then
        esdrow (il) = esdrow (il) + flux_ai(il) * flndrow(il) * saverad
        esdrow (il) = esdrow (il) + flux_ci(il) * flndrow(il) * saverad
        !
        fallrow(il) = fallrow(il) + fluxall(il) * flndrow(il) * saverad
        fa10row(il) = fa10row(il) + fluxa10(il) * flndrow(il) * saverad
        fa2row(il) = fa2row (il) + fluxa2 (il) * flndrow(il) * saverad
        fa1row(il) = fa1row (il) + fluxa1 (il) * flndrow(il) * saverad
        !
      end if
      !        write(0,*) il,saverad,xemis(il,ilev,idua)
      !        write(0,*) il,xemis(il,ilev,idua),xemis(il,ilev,iduc),
      !     &         flux_ai(il),flux_ci(il),fac,esdrow(il),saverad
      !       write(0,*) "dustem: ",il,fac,ztmst,grav
      !
    end if ! (dust_mask and c_eff)
    !
    !       ! save dust wind components
    !
    if (isvdust > 0) then
      duwdrow(il) = duwdrow(il) + du_wind(il) * saverad
      dustrow(il) = dustrow(il) + ustar_d(il) * saverad
      duthrow(il) = duthrow(il) + du_th(il) * saverad
      usmkrow(il) = usmkrow(il) + ustar_acrit(il) * saverad
    end if
    !
  end do ! il
  !
  !   --------------------------------------------------------------
  !
  return
  !
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
