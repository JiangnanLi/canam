!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine mamrad_init3(xvf, pvf, o2vf, cn2vf, ovf, amuvf, &
                              cpvf, gvf, co2vf, &
                              effh, effsrc, to2, effo2nir, &
                              x1chem, x2chem, dxchem, wchem, k1chem, &
                              wwvs2, wwvs3, wco2s, wco2st, &
                              wwvl3, wch4l, wo3l, k1co2s, k2co2s, &
                              pdr, pcd, idr, icd, &
                              sg,ptoit,co2_ppm,ilev)
  !
  !     * apr 30/2011 - m.lazare. new version for gcm15j, based
  !     *                         on mamrad_init:
  !     *                         - modified to interface with
  !     *                           CK SCHEME AND VICTOR'S MODIFIED
  !     *                           middle atmosphere code.
  !
  !     * this routine is called in section 0 of the model and
  !     * calculates the 1-d, invariant data for radiative cooling/
  !     * heating in the middle atmosphere.
  !
  !     * input data for mamrad2 (determined in thermdat2,vmrco2 and precl2).
  !
  implicit none
  !
  real, intent(in) :: ptoit !< Variable description\f$[units]\f$
  real, intent(in) :: co2_ppm !< Variable description\f$[units]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer :: l !< Variable description\f$[units]\f$
  !
  !     * output fields:
  !
  real, intent(in)  , dimension(3,ilev) :: pcd !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(3,67)  :: pdr !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: xvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: pvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: o2vf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: cn2vf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: ovf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: amuvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: cpvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: gvf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: co2vf !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: effh !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: effsrc !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: to2 !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: effo2nir !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wwvs2 !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wwvs3 !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wco2s !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wco2st !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wwvl3 !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wch4l !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wo3l !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilev)  :: wchem !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilev)  :: icd !< Variable description\f$[units]\f$
  integer, intent(in),dimension(67)    :: idr !< Variable description\f$[units]\f$
  real, intent(in) :: x1chem !< Variable description\f$[units]\f$
  real, intent(in) :: x2chem !< Variable description\f$[units]\f$
  real, intent(in) :: dxchem !< Variable description\f$[units]\f$
  integer, intent(in) :: k1chem !< Variable description\f$[units]\f$
  integer, intent(in) :: k1co2s !< Variable description\f$[units]\f$
  integer, intent(in)  :: k2co2s !< Variable description\f$[units]\f$
  !
  !     * input fields.
  !
  real, intent(in), dimension(ilev) :: sg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !========================================================================
  !     * determine atmospheric composition used to calculate the radiative
  !     * heating/cooling in the middle atmosphere. note that 1-d fields
  !     * are used because at high enough altitudes, model surfaces are
  !     * essentially constant-pressure independant of location.
  !
  call thermdat2(xvf, pvf, o2vf, cn2vf, ovf, amuvf, &
                     cpvf, gvf, &
                     effh, effsrc, to2, effo2nir, &
                     x1chem, x2chem, dxchem, wchem, k1chem, &
                     wwvs2, wwvs3, wco2s, wco2st, &
                     wwvl3, wch4l, wo3l, k1co2s, k2co2s, &
                     sg, ilev)
  call dpmco2
  call pco2nir
  call precl2(pdr, pcd, idr, icd, xvf, ilev)
  call vmrco2(co2vf,xvf,co2_ppm,ilev)
  call cco2gr(co2_ppm)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
