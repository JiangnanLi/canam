!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vmrco2(co2vf,xvf,cco2,kmx)

  !***********************************************************************
  !*                                                                     *
  !*              subroutine vmrco2                                      *
  !*                                                                     *
  !***********************************************************************

  ! to determine a vertical profile of the co2 volume mixing ratio (vmr)
  ! by scaling the "standart" co2 vmr profile using the ratio of the co2
  ! vmr in the troposphere.
  !                                             v. fomichev, november, 1997
  ! c called by mamrad
  ! calls a18lin

  implicit none
  real :: a18lin
  real, intent(in) :: cco2
  integer :: i
  integer :: k
  integer, intent(in) :: kmx
  real :: x

  ! input: xvf - vertical array of scale heights
  !        cco2 - co2 vmr in the troposphere

  real, intent(in), dimension(kmx) :: xvf !< Variable description\f$[units]\f$

  ! output: co2vf

  real, intent(inout), dimension(kmx) :: co2vf !< Variable description\f$[units]\f$

  ! the standard co2 vmr profile
  real, dimension(82) :: xo !< Variable description\f$[units]\f$
  real, dimension(82) :: co2o !< Variable description\f$[units]\f$

  ! internal array:
  real, dimension(82) :: aco2o !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  data (xo(i), i=1,82)/ &
  0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, &
  3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5, 4.75, 5.0, 5.25, 5.5, 5.75, &
  6.0, 6.25, 6.5, 6.75, 7.0, 7.25, 7.5, 7.75, 8.0, 8.25, 8.5, 8.75, &
  9.0, 9.25, 9.5, 9.75,10.0,10.25,10.5,10.75,11.0,11.25,11.5,11.75, &
 12.0,12.25,12.5,12.75,13.0,13.25,13.5,13.75,14.0,14.25,14.5,14.75, &
 15.0,15.25,15.5,15.75,16.0,16.25,16.5,16.75,17.0,17.25,17.5,17.75, &
 18.0,18.25,18.5,18.75,19.0,19.25,19.5,19.75,20.0,21.5/
  data (co2o(i), i=1,82)   / 50*3.600e-04, &
  3.58e-04, 3.54e-04, 3.50e-04, 3.41e-04, 3.28e-04, 3.11e-04, &
  2.93e-04, 2.75e-04, 2.56e-04, 2.37e-04, 2.18e-04, 1.99e-04, &
  1.80e-04, 1.61e-04, 1.42e-04, 1.24e-04, 1.06e-04, 9.00e-05, &
  7.80e-05, 6.80e-05, 5.90e-05, 5.10e-05, 4.40e-05, 3.70e-05, &
  3.00e-05, 2.40e-05, 1.90e-05, 1.40e-05, 1.00e-05, 7.00e-06, &
  5.00e-06, 1.00e-10/

  ! to scale the co2 vmr by cco2 and take a logarithm

  do k = 1, 82
    aco2o(k) = log(co2o(k)*cco2/co2o(1))
  end do ! loop 1

  ! to determine co2 vmr at the model mid-layer levels

  do k=1,kmx
    x = xvf(k)
    if (x>21.5) then
      co2vf(k) = exp(aco2o(82))
    else
      co2vf(k) = exp(a18lin(x,xo,aco2o,1,82))
    end if
  end do ! loop 2

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
