!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine evalsc3(t,q,z,p,dz,mb,qfxrow,tfxrow,maxi,lpbl, &
                   iclos,ishall,ilg,ilgg,il1,il2,ilev,msg,grav, &
                   eps1,rgas,rgasv,rgocp,tvfa,wst)
  !-------------------------------------------------------------------------
  !     * may 27/2006 - m.lazare.    new version for gcm15f:
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     *                            - "ZERO" data constant added and used
  !     *                              in call to intrinsics.
  !     * dec 12/05 - k.vonsalzen.   previous version evalsc2 for gcm15d/gcm15e:
  !     *                            - wstar passed out to physics (as wst,
  !     *                              becoming wstar in physics, for use in
  !     *                              gustiness parameterization in physics
  !     *                              driver.
  !     * may 05/03 - k.vonsalzen.   final frozen version evalsc for gcm15b/c.
  !     *                            correct problems with moisture and
  !     *                            energy conservation (i.e. air density).
  !     * sep 09/02 - k. von salzen. cloud-base mass flux uses 0.03
  !     *                            instead of 0.045.
  !     * jun 13/01 - k. von salzen. tuned version for gcm15.
  !     * may 16/01 - k. von salzen. expression for mb now uses 0.045*...
  !     *                            instead of 0.03*...
  !     * oct  3/00 - k. von salzen. revised and adapted for gcm.
  !     * nov 24/99 - k. von salzen/ test version for bomex.
  !                   n. mcfarlane.
  !
  !     * calculates cloud-base mass flux in shallow cumulus cloud
  !     * ensemble and other parameters needed in subroutine transc.
  !     * parameterization of cloud-base mass flux according to
  !     * grant (2001) from pbl instability.
  !-------------------------------------------------------------------------
  use phys_parm_defs, only : ap_scale_scmbf
  implicit none
  real :: drdqv
  real, intent(in) :: eps1
  real, intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: ilgg    !< Total number of gathered atmospheric columns with shallow convection \f$[unitless]\f$
  integer :: jno
  integer :: jyes
  integer :: l
  integer, intent(in) :: msg
  real :: rfx
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: rgasv  !< Ideal gas constant for moist air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: rgocp  !< Ideal gas constant divided by heat capacity for dry air \f$[unitless]\f$
  real :: tpot
  real, intent(in) :: tvfa
  real :: tvp
  real :: vptflx
  real :: wstar
  real :: zero
  real :: zrho
  !
  !     * multi-level i/o fields.
  !

  real, intent(in)  , dimension(ilg,ilev)  :: t !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)  :: z !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)  :: q !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)  :: p !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg,ilev)  :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$

  !
  !     * single-level i/o fields.
  !
  real, intent(inout)  , dimension(ilg)  :: mb !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg)  :: qfxrow !< Variable description\f$[units]\f$
  real, intent(in)  , dimension(ilg)  :: tfxrow !< Variable description\f$[units]\f$
  real, intent(inout)  , dimension(ilg)  :: wst !< Variable description\f$[units]\f$

  integer, intent(inout),dimension(ilg)  :: maxi !< Variable description\f$[units]\f$
  integer, intent(inout),dimension(ilg)  :: iclos !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg)  :: lpbl !< Variable description\f$[units]\f$
  integer, intent(inout),dimension(ilg)  :: ishall !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * internal workspace.
  !
  real  , dimension(ilg)  :: tvppbl
  real  , dimension(ilg)  :: dpbl
  integer,dimension(ilg)  :: lptmp
  !
  data zero   / 0.    /
  !-------------------------------------------------------------------------
  !
  !     * initializations.
  !
  do il = il1,il2
    maxi(il)  = ilev
    lptmp(il) = ilev
    mb(il)    = 0.
    tvppbl(il) = 0.
    dpbl(il)  = 0.
    iclos(il) = 1
  end do ! loop 20
  !
  !     * put upper bound on pbl height.
  !
  do l = ilev,msg + 1, - 1
    do il = il1,il2
      if (z(il,l) < 2000. ) then
        lptmp(il) = l
      end if
    end do
  end do ! loop 70
  do il = il1,il2
    lptmp(il) = max(lpbl(il),lptmp(il))
  end do ! loop 80
  !
  !     * mean virtual potential temperature in pbl.
  !
  do l = ilev,msg + 1, - 1
    do il = il1,il2
      if (l >= lptmp(il) ) then
        tvp = t(il,l) * (1. + tvfa * q(il,l)) * (1000./p(il,l)) ** rgocp
        tvppbl(il) = tvppbl(il) + tvp * dz(il,l)
        dpbl(il) = dpbl(il) + dz(il,l)
      end if
    end do
  end do ! loop 140
  do il = il1,il2
    tvppbl(il) = tvppbl(il)/dpbl(il)
  end do ! loop 150
  !
  !     * calculate w* from buoyancy flux at surface and
  !     * use this to get the cloud base mass flux mb.
  !
  do il = il1,il2
    l = lptmp(il)
    zrho = 100. * p(il,l)/(rgas * t(il,l))
    drdqv = (1. + q(il,ilev)) ** 2
    rfx = qfxrow(il) * drdqv
    tpot = t(il,ilev) * (1000./p(il,ilev)) ** rgocp
    vptflx = - tfxrow(il) * (1. + tvfa * q(il,ilev)) - tvfa * rfx * tpot
    wstar = max(grav * vptflx * dpbl(il)/tvppbl(il),zero) ** (1./3.)
    wst(il) = wstar
    mb(il) = ap_scale_scmbf * zrho * wstar
    if (mb(il) == 0. ) iclos(il) = 0
  end do ! loop 160
  !
  !     * determine grid points at which the situation permits
  !     * shallow convection to occur.
  !
  jyes = 0
  jno = il2 - il1 + 2
  do il = il1,il2
    if (iclos(il) == 1) then
      jyes        = jyes + 1
      ishall(jyes) = il
    else
      jno         = jno - 1
      ishall(jno) = il
    end if
  end do ! loop 200
  ilgg = jyes
  !
  return
end

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
