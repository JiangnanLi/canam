!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rdswet_seaice(snow,dens,reff,gc,gt, &
                         reff0,delt,ilg,il1,il2,gc_min,gc_max)

  !
  !     * 26 april 2018 - j. cole -> modified version of rdswet for sea-ice
  !     *                            that estimate amount of liquid in snow.

  !
  implicit none

  !     * input scalars
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  real, intent(in) :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in) :: gc_min !< Variable description\f$[units]\f$
  real, intent(in) :: gc_max !< Variable description\f$[units]\f$
  real, intent(inout) :: reff0 !< Variable description\f$[units]\f$

  !     * input arrays.
  real, intent(in), dimension(ilg) :: snow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: dens !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gt !< Variable description\f$[units]\f
  !     * input/output arrays.
  real, intent(inout), dimension(ilg) :: reff !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !     * internal scalar variables
  real :: fliq
  real :: fliq_max
  real :: melt
  real :: hf
  real :: hc
  integer :: i

  !     * parameters
  real, parameter :: c1 = 4.22e-13
  real, parameter :: cr_min = 0.03 ! kg/m^3
  real, parameter :: cr_max = 0.1 ! kg/m^3
  real, parameter :: rho_t = 200.0 ! kg/m^3
  real, parameter :: tfrez_lim = 273.15 ! k
  real, parameter :: eps = 1.0e-7

  real :: pi
  real :: rvord
  real :: tfrez
  real :: hs
  real :: hv
  real :: daylnt
  real :: csno
  real :: cpack
  real :: gtfsw
  real :: rkhi
  real :: sbc
  real :: snomax

  common /param1/pi,rvord,tfrez,hs,hv,daylnt
  common /param3/csno,cpack,gtfsw,rkhi,sbc,snomax

  !----------------------------------------------------------------------

  ! latent heat of fusion (j kg-1) of snow or sea ice = hs-hv
  hf = hs - hv

  ! heat capacity of pack ice (normalized by sqrt(2)).
  !
  hc = cpack * sqrt(2.)

  do i = il1,il2

    if (gc(i) > gc_min .and. gc(i) < gc_max) then

      if (snow(i) > 0.0) then

        if (gt(i) >= (tfrez_lim - eps)) then ! the snow is melting

          ! reuse some code from oifpst10 to compute the amount of melt that can occur.

          melt = (gt(i) - tfrez_lim) * hc/hf

          ! andersen, 1975 provides a simple parameterization of maximum liquid water
          ! retention for a snow pack as a function of snow density.

          if (dens(i) >= rho_t) then
            fliq_max = cr_min
          else
            fliq_max = cr_min &
                       + (cr_max - cr_min) * (rho_t - dens(i))/rho_t
          end if

          fliq = melt/snow(i)

          !               if (fliq > fliq_max) fliq=fliq_max

          ! as a sensitivity test set fliq=fliq_max since the max gt=trez_lim
          fliq = fliq_max

          ! compute the change in grain size due to wet growth.
          reff(i) = max(reff0,reff(i))

          reff(i) = reff(i) * 1.e+6

          reff(i) = ((0.75e+18 * c1/pi) * (fliq ** 3) * delt &
                    + reff(i) ** 3) ** (1./3.)

          reff(i) = reff(i) * 1.e-6

        end if  ! gt

      end if ! snow

    end if ! gc

  end do ! i

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
