!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine strat_aerosol_lab(lsa,levsa,ivtau)

  !     * dec 20/2016 - j. cole
  !
  !     * defines level index values for input stratospheric aerosol fields.

  implicit none

  ! output data
  integer, intent(out), dimension(levsa) :: lsa !< Variable description\f$[units]\f$

  ! input data
  integer, intent(in) :: levsa   !< Variable description\f$[units]\f$
  integer, intent(in) :: ivtau   !< Switch for type of stratospheric aerosol \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ! local data
  integer :: l

  do l = 1,levsa
    lsa(l) = l
  end do ! l

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
