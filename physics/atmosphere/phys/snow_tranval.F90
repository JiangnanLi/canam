!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine snow_tranval(trandif, & ! output
                        trandir, &
                        smu, & ! input
                        salb, &
                        bc_conc, &
                        snow_reff, &
                        swe, &
                        c_ind, &
                        il1, &
                        il2, &
                        ilg, &
                        nbnd)
  !
  !     * feb 10/2015 - j.cole. new version for gcm18:
  !                             - nbc increased from 12 to 20.
  !                               therefor lbc_conc data statement
  !                               changed accordingly.
  !     * jan 24/2013 - j.cole. previous version for gcm17:
  !                    - computes the direct and diffuse snow transmission
  !                      using lookup table and current snow conditions.
  !

  implicit none

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ! this subroutine compute the direct and diffuse snow transmission using a
  ! lookup table and information about the current snow pack state.
  ! transmission are computed for each solar radiation wavelength intervals
  ! so a total of 8 albedos will be returned.  these transmissions can then be
  ! used to compute the total snow tramission by weighting
  ! the results by the direct beam fraction of the incident solar radiation.
  !
  ! inputs
  ! smu:       cosine of the solar zenith angle [unitless]
  ! salb :     albedo of the underlying surface [unitless]
  ! bc_conc:   concentration of black carbon in the snow pack [ng (bc)/kg (snow)]
  ! snow_reff: effective radius of the snow grain [microns]
  ! swe:       snow water equivalent (snowpack density*snow pack depth) [kg/m^2]
  ! c_ind:     indicator that a calculation should be performed for this point
  !            1-yes, 0-no
  ! il1:       starting point for transmission calculations
  ! il2:       ending point for transmission calculations
  ! ilg:       number of points for which to compute transmissions
  ! nbnd:      number of wavelength intervals for which to compute the transmissions
  !
  ! outputs
  ! trandif: diffuse snow transmission (aka white sky transmission)
  ! trandir: direct beam snow transmission (aka black sky transmission)
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  !
  ! input
  !
  real, intent(in), dimension(ilg) :: smu !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: bc_conc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: snow_reff !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: swe !< Variable description\f$[units]\f$

  real, intent(in), dimension(ilg,nbnd) :: salb !< Variable description\f$[units]\f$

  integer, intent(in), dimension(ilg) :: c_ind !< Variable description\f$[units]\f$

  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: nbnd !< Variable description\f$[units]\f$

  !
  ! output
  !
  real, intent(out), dimension(ilg,nbnd) :: trandif !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,nbnd) :: trandir !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! local
  !
  real, dimension(ilg,2) :: wsmu
  real, dimension(ilg,2) :: wbc
  real, dimension(ilg,2) :: wreff
  real, dimension(ilg,2) :: wswe

  real, dimension(2) :: wsalb

  real :: wtt

  integer, dimension(ilg) :: ismu
  integer, dimension(ilg) :: ibc
  integer, dimension(ilg) :: ireff
  integer, dimension(ilg) :: iswe

  integer :: ib
  integer :: i
  integer :: isalb

  integer :: iismu
  integer :: iisalb
  integer :: iibc
  integer :: iireff
  integer :: iiswe

  integer :: mvidx

  !
  ! constants
  !
  integer, parameter :: nsmu = 10
  integer, parameter :: nsalb    = 11
  integer, parameter :: nbc      = 20
  integer, parameter :: nreff    = 10
  integer, parameter :: nswe     = 11
  integer, parameter :: nbnd_lut = 4

  !ignoreLint(6)
  real, parameter :: lsalb (nsalb) = (/0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0/)
  real, parameter :: lsmu(nsmu) = (/0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0/)
  real, parameter :: lsnow_reff(nreff) = (/50.0,75.0,100.0,150.0,200.0,275.0,375.0,500.0,700.0,1000.0/)
  real, parameter :: lswe(nswe) = (/0.1,0.25,0.65,1.7,4.4,12.0,30.0,75.0,200.0,500.0,5000.0/)
  real, parameter :: lbc_conc(nbc) = (/0.0,1.0,5.0,10.0,50.0,100.0,500.0,1000.0,5000.0,10000.0,50000.0,100000.0, &
                                     250000.0,500000.0,750000.0,1000000.0,2500000.0,5000000.0,7500000.0,10000000.0/)

  real, dimension(nbc,nswe,nreff,nsmu,nsalb,nbnd_lut) :: trandif_lut
  real, dimension(nbc,nswe,nreff,nsmu,nsalb,nbnd_lut) :: trandir_lut

  integer :: snow_tran_lut_init

  common /snowtranlut/ trandif_lut,trandir_lut,snow_tran_lut_init

  ! abort if the lut has not been read in
  if (snow_tran_lut_init /= 1) then
    write(6, * ) 'SNOW TRANSMISSION LUT HAS NOT BEEN INITIALIZED', &
               snow_tran_lut_init
    call xit('SNOW_TRANVAL', - 1)
  end if

  ! abort if the number of bands in the lut does not match size passed in
  if (nbnd_lut /= nbnd) then
    write(6, * ) 'MISMATCH IN NUMBER OF WAVELENGTH INTERVALS'
    call xit('SNOW_TRANVAL', - 2)
  end if

  ! compute the transmissions using linear interpolation

  ! compute the interpolation weights and points once and reuse for
  ! transmission interpolation for each band.
  ! have a check to set the weights depending if the input is
  ! outside or inside the lookup table range

  do i = il1, il2
    if (c_ind(i) == 1) then
      ismu(i)  = mvidx(lsmu,       nsmu,  smu(i))
      ibc(i)   = mvidx(lbc_conc,   nbc,   bc_conc(i))
      ireff(i) = mvidx(lsnow_reff, nreff, snow_reff(i))
      iswe(i)  = mvidx(lswe,       nswe,  swe(i))

      if (smu(i) <= lsmu(1)) then
        wsmu(i,2) = 0.0
        wsmu(i,1) = 1.0 - wsmu(i,2)
      else if (smu(i) > lsmu(nsmu)) then
        wsmu(i,2) = 1.0
        wsmu(i,1) = 1.0 - wsmu(i,2)
      else
        wsmu(i,2) = (smu(i) - lsmu(ismu(i))) &
                    / (lsmu(ismu(i) + 1) - lsmu(ismu(i)))
        wsmu(i,1) = 1.0 - wsmu(i,2)
      end if

      if (bc_conc(i) <= lbc_conc(1)) then
        wbc(i,2) = 0.0
        wbc(i,1) = 1.0 - wbc(i,2)
      else if (bc_conc(i) > lbc_conc(nbc)) then
        wbc(i,2) = 1.0
        wbc(i,1) = 1.0 - wbc(i,2)
      else
        wbc(i,2) = (bc_conc(i) - lbc_conc(ibc(i))) &
                   / (lbc_conc(ibc(i) + 1) - lbc_conc(ibc(i)))
        wbc(i,1) = 1.0 - wbc(i,2)
      end if

      if (snow_reff(i) <= lsnow_reff(1)) then
        wreff(i,2) = 0.0
        wreff(i,1) = 1.0 - wreff(i,2)
      else if (snow_reff(i) > lsnow_reff(nreff)) then
        wreff(i,2) = 1.0
        wreff(i,1) = 1.0 - wreff(i,2)
      else
        wreff(i,2) = (snow_reff(i) - lsnow_reff(ireff(i))) &
                     / (lsnow_reff(ireff(i) + 1) &
                     - lsnow_reff(ireff(i)))
        wreff(i,1) = 1.0 - wreff(i,2)
      end if

      if (swe(i) <= lswe(1)) then
        wswe(i,2) = 0.0
        wswe(i,1) = 1.0 - wswe(i,2)
      else if (swe(i) > lswe(nswe)) then
        wswe(i,2) = 1.0
        wswe(i,1) = 1.0 - wswe(i,2)
      else
        wswe(i,2) = (swe(i) - lswe(iswe(i))) &
                    / (lswe(iswe(i) + 1) - lswe(iswe(i)))
        wswe(i,1) = 1.0 - wswe(i,2)
      end if
    end if
  end do ! i

  do ib = 1, nbnd
    do i = il1, il2
      if (c_ind(i) == 1) then

        isalb = mvidx(lsalb,    nsalb, salb(i,ib))

        if (salb(i,ib) <= lsalb(1)) then
          wsalb(2) = 0.0
          wsalb(1) = 1.0 - wsalb(2)
        else if (salb(i,ib) > lsalb(nsalb)) then
          wsalb(2) = 1.0
          wsalb(1) = 1.0 - wsalb(2)
        else
          wsalb(2) = (salb(i,ib) - lsalb(isalb)) &
                     / (lsalb(isalb + 1) - lsalb(isalb))
          wsalb(1) = 1.0 - wsalb(2)
        end if

        trandir(i,ib) = 0.0
        trandif (i,ib) = 0.0

        do iisalb = isalb,isalb + 1
          do iismu = ismu(i),ismu(i) + 1
            do iireff = ireff(i),ireff(i) + 1
              do iiswe = iswe(i), iswe(i) + 1
                do iibc = ibc(i), ibc(i) + 1

                  wtt = wsmu(i,iismu - ismu(i) + 1) &
                        * wreff(i,iireff - ireff(i) + 1) &
                        * wswe(i,iiswe - iswe(i) + 1) &
                        * wbc(i,iibc - ibc(i) + 1) &
                        * wsalb(iisalb - isalb + 1)

                  trandif (i,ib) = trandif (i,ib) + wtt &
                                   * trandif_lut(iibc,iiswe,iireff,iismu,iisalb,ib)
                  trandir(i,ib) = trandir(i,ib) + wtt &
                                  * trandir_lut(iibc,iiswe,iireff,iismu,iisalb,ib)

                end do ! iibc
              end do  ! iiswe
            end do     ! iireff
          end do        ! iismu
        end do           ! iisalb

        if (trandif (i,ib) > 1.3 .or. &
            trandif (i,ib) < 0.0) then
          write(6, * ) 'Bad trandif ',i,ib,smu(i),bc_conc(i), &
                      snow_reff(i),swe(i),salb(i,ib),trandif (i,ib)
          write(6, * ) i,ib,ismu(i),ibc(i),ireff(i),iswe(i),isalb
          call xit('SNOW_TRANVAL', - 3)
        end if
        if (trandir(i,ib) > 1.3 .or. &
            trandir(i,ib) < 0.0) then
          write(6, * ) 'Bad trandir ',i,ib,smu(i),bc_conc(i), &
                       snow_reff(i),swe(i),salb(i,ib),trandir(i,ib)
          write(6, * ) i,ib,ismu(i),ibc(i),ireff(i),iswe(i),isalb
          call xit('SNOW_TRANVAL', - 3)
        end if
      else
        trandif (i,ib) = - 999.0
        trandir(i,ib) = - 999.0
      end if
    end do                 ! i
  end do                    ! ib

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
