!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine abcvdm6 (a,b,c, cl,cdvlm,grav,il1,il2,ilg,ilev,rgas, &
                    rkm,sgj,sgbj,shj,tsgb,todt,nogwdrg)

  !     * jul 15/88 - m.lazare : reverse order of local sigma arrays.
  !     * jan 29/88 - r.laprise: previous hybrid version for gcm3h.

  !     * generalize for hybrid version of model
  !     * made from abcvdmu.
  !     * calculates the three vectors forming the tri-diagonal matrix
  !     * for the implicit vertical diffusion of momentum on full levels.
  !     * a,b,c are the lower,main,upper diagonal respectively.

  implicit none
  real :: d
  real, intent(inout) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer :: i
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  real :: ovds
  real, intent(inout) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(inout) :: todt
  logical, intent(inout) :: nogwdrg

  real, intent(inout), dimension(ilg,ilev) :: a !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: b !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: c !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: rkm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tsgb !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: cdvlm !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: cl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------

  do i = il1,il2
    ovds  = sgbj(i,1) * (grav/rgas) ** 2/(sgj(i,2) - sgj(i,1))
    c(i,1) = ovds * (1./tsgb(i,1)) ** 2 * rkm(i,1)
    cl(i) = 0.
  end do ! loop 50

  do i = il1,il2
    b(i,1) = - c(i,1)
  end do ! loop 75

  do l = 2,ilev
    do i = il1,il2
      ovds  = (grav * sgbj(i,l - 1)/rgas) ** 2 &
              /( (sgbj(i,l) - sgbj(i,l - 1)) * (sgj(i,l) - sgj(i,l - 1)) )
      a(i,l) = ovds * (1./tsgb(i,l - 1)) ** 2 * rkm(i,l - 1)
    end do
  end do ! loop 100

  do l = 2,ilev - 1
    do i = il1,il2
      d     = (sgbj(i,l + 1) - sgbj(i,l)) / (sgbj(i,l) - sgbj(i,l - 1))
      c(i,l) = a(i,l + 1) * d
      b(i,l) = - a(i,l + 1) * d - a(i,l)
    end do
  end do ! loop 200

  l = ilev
  do i = il1,il2
    b(i,l) =  - a(i,l) &
             - grav * cdvlm(i) * shj(i,l) &
             /(rgas * tsgb(i,l) * (sgbj(i,l) - sgbj(i,l - 1)) )
  end do ! loop 250

  !     * define matrix to invert = i-2 *dt *mat(a,b,c).

  if (nogwdrg) then

    do l = 1,ilev - 1
      do i = il1,il2
        a(i,l + 1) = - todt * a(i,l + 1)
        c(i,l) =  - todt * c(i,l)
      end do
    end do ! loop 500

    do l = 1,ilev
      do i = il1,il2
        b(i,l) = 1. - todt * b(i,l)
      end do
    end do ! loop 550

  end if

  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
