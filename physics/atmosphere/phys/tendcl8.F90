!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tendcl8 (estg,esg,rmrow, &
                    ilg,il1,il2,ilev,levs, &
                    ztmst,moist,sref,spow)

  !     * may 24/2017 - m.lazare. for conversion to new xc40:
  !     *                         - replace ibm intrinsics by generic
  !     *                           for portability.
  !     * apr 27/2012 - m.lazare. new version for gcm16:
  !     *                         ensure mixing ratio ("RMROW") is
  !     *                         posiive-definite before conversion
  !     *                         to hybrid, by imposing "TINY" value.
  !     * oct 13/2007 - m.lazare. previous version tendcl7 for gcm15g/h/i:
  !     *                         - ztmst (2.*delt or delt)
  !     *                           passed in instead of
  !     *                           delt and used accordingly.
  !     *                         - temperature removed.
  !     *                         - "RMOON" removed.
  !     * sep 13/2006 - m.lazare/ previous version tendcl6 for gcm15f:
  !     *               f.majaess.- work arrays now local.
  !     *                         - unused ah,bh variables and
  !     *                           eps common block removed.
  !     *                         - unused prespa removed.
  !
  !     * calculate tendencies estg from adjusted rmrow
  !     * and original values esg of model moisture variable.
  !     * also update original copies.
  !     * moist controls the type of moisture variable.
  !     * rmrow corresponds to specific humidity.
  !
  implicit none
  real :: esnew
  real :: esnewx
  real :: esold
  real :: humsp
  integer :: i
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer :: m
  integer, intent(inout) :: moist
  integer :: msg
  integer :: nc4to8
  real :: pinv
  real :: rmnew
  real, intent(inout) :: spow
  real, intent(inout) :: sref

  real,    intent(out),   dimension(ilg,levs) :: estg !< Variable description\f$[units]\f$
  real,    intent(in),    dimension(ilg,levs) :: esg !< Variable description\f$[units]\f$
  real  ,  intent(in),    dimension(ilg,ilev) :: rmrow !< Variable description\f$[units]\f$
  real, intent(in)     :: ztmst !< Variable description\f$[units]\f$
  real    :: rztmst !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * work arrays.
  !
  real,                   dimension(ilg,ilev) :: fact
  real,                   dimension(ilg,ilev) :: efact
  real :: r
  real :: rlim
  !
  integer*4 :: len
  integer :: machine
  integer :: intsize
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !-----------------------------------------------------------------------
  if (levs > ilev)        call xit('TENDCL8', - 1)
  rztmst = 1./ztmst
  !
  !     * process moisture under control of moist.
  !
  msg = ilev - levs
  !
  !     * evaluate smallest possible number which can exist in this
  !     * (32-bit or 64-bit) mode.
  !
  rlim = tiny(r)

  if (moist == nc4to8("   Q") .or. moist == nc4to8("SL3D")) then
    !
    do l = 1,levs
      m = l + msg
      do i = il1,il2
        esold     = esg(i,l)
        humsp     = rmrow(i,m)
        esnew     = humsp
        estg(i,l) = estg(i,l) + (esnew - esold) * rztmst
      end do
    end do

  else if (moist == nc4to8("QHYB") .or. moist == nc4to8("SLQB")) then
    !
    fact (1:ilg, 1:levs) = 1.
    efact(1:ilg, 1:levs) = 0.
    do l = 1,levs
      m = l + msg
      do i = il1,il2
        rmnew = rmrow(i,m)
        if (rmnew <= 0.) rmnew = 10. * rlim
        fact (i,l) = sref/rmnew
        efact(i,l) = log(fact(i,l))
      end do
    end do
    !
    if (spow == 1.) then
      do l = 1,levs
        m = l + msg
        do i = il1,il2
          rmnew = rmrow(i,m)
          if (rmnew >= sref) then
            esnewx = rmnew
          else
            esnewx = sref/(1. + efact(i,l))
          end if
          estg(i,l) = estg(i,l) + (esnewx - esg(i,l)) * rztmst
        end do
      end do
    else
      pinv = 1./spow
      do l = 1,levs
        m = l + msg
        do i = il1,il2
          rmnew = rmrow(i,m)
          if (rmnew >= sref) then
            esnewx = rmnew
          else
            esnewx = sref/((1. + spow * efact(i,l)) ** pinv)
          end if
          estg(i,l) = estg(i,l) + (esnewx - esg(i,l)) * rztmst
        end do
      end do
    end if

  else
    call xit('TENDCL8', - 2)
  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
