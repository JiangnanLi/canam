!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine pahgt2(pf,ph,zf,zh,throw,tfrow,shj,shtj,pressg,rgas, &
                  grav,ilg,ilev,lev,il1,il2,ilevp1)
  !
  !     * may 24/2017 - m.lazare.        for conversion to new xc40:
  !     *                                - replace ibm intrinsics by generic
  !     *                                  for portability.
  !     * knut von salzen - jun 03,2013. new version for gcm17+ which
  !     *                                also calculates mid-layer
  !     *                                information (ph,zh).
  !     * knut von salzen - jul 27,2009. previous version pahgt2 for gcm15h+
  !     *                                for calculation of pressure and
  !     *                                height at model levels.
  !
  implicit none
  real, intent(inout) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer :: i
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ilevm
  integer, intent(inout) :: ilevp1  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: ilt
  integer :: l
  integer, intent(inout) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  real, intent(inout) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real :: rog
  !
  real, intent(out), dimension(ilg,ilev)   :: pf !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev)   :: ph !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev)   :: zf !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg,ilev)   :: zh !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilg,ilevp1) :: throw   !< Physics internal field for temperature, including moon layer at atmospheric model top \f$[K]\f$
  real, intent(in),  dimension(ilg,ilevp1) :: tfrow !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilg,lev)    :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in),  dimension(ilg,ilev)   :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in),  dimension(ilg)        :: pressg   !< Surface pressure \f$[Pa]\f$
  real, allocatable, dimension(:,:)        :: term1         !< Variable description\f$[units]\f$
  real, allocatable, dimension(:,:)        :: term2         !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  integer*4 :: len
  integer :: machine
  integer :: intsize
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !
  !---------------------------------------------------------------------
  !     * calculate height and pressure.
  !
  do l = 1,ilev
    pf(il1:il2,l) = shtj(il1:il2,l + 1) * pressg(il1:il2)
    ph(il1:il2,l) = shj (il1:il2,l) * pressg(il1:il2)
  end do
  rog = rgas/grav
  ilevm = ilev - 1
  zf(il1:il2,ilev) = 0.
  ilt = il2 - il1 + 1
  allocate(term1(ilt,ilevm))
  allocate(term2(ilt,ilevm))
  do l = 1,ilevm
    term1(1:ilt,l) = pf(il1:il2,l + 1)/pf(il1:il2,l)
  end do
  do l = 1,ilevm
    do i = 1,ilt
      term2(i,l) = log(term1(i,l))
    end do
  end do
  do l = ilevm,1, - 1
    zf(il1:il2,l) = zf(il1:il2,l + 1) + rog * throw(il1:il2,l + 2) &
                    * term2(1:ilt,l)
  end do
  do l = 1,ilevm
    term1(1:ilt,l) = ph(il1:il2,l + 1)/ph(il1:il2,l)
  end do
  do l = 1,ilevm
    do i = 1,ilt
      term2(i,l) = log(term1(i,l))
    end do
  end do
  l = ilev
  zh(il1:il2,l) = rog * tfrow(il1:il2,l + 1) &
                  * log(pressg(il1:il2)/ph(il1:il2,ilev))
  do l = ilevm,1, - 1
    zh(il1:il2,l) = zh(il1:il2,l + 1) + rog * tfrow(il1:il2,l + 1) &
                    * term2(1:ilt,l)
  end do
  deallocate (term1)
  deallocate (term2)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
