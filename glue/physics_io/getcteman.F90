subroutine getcteman(nuan,wetfpak,wetspak,lghtpak, &
                     month,nlon,nlat,ijpak,gg)
  !
  !     * feb 26/2018 - v.arora remove reading of wetf (prescribed
  !                             wetland fraction and set it to zero)
  !                             read in lightning lgt1 (lis/otd lightning)
  !
  !     * feb 23/2018 - v.arora remove reading of prob. of fire
  !                             due to humans & fire extn. prob.
  !
  !     * mar 01/2016 - m.lazare. new version for gcm19:
  !     *                         read in ctem invariant fields.
  !
  !     * read in ctem invariant fields.
  !
  implicit none
  !
  !     * fields read:
  !
  real, intent(inout), dimension(ijpak) :: wetfpak
  real, intent(in), dimension(ijpak) :: wetspak
  real, intent(in), dimension(ijpak) :: lghtpak
  !
  real, intent(in) :: gg( * )
  !
  integer, intent(in) :: nlon
  integer, intent(in) :: nlat
  integer, intent(in) :: ijpak
  integer, intent(in) :: nuan
  integer, intent(in) :: month
  !
  integer*4 :: mynode
  common /mpinfo/ mynode
  !
  !     * local data.
  !
  integer :: nc4to8
  integer :: machine
  integer :: intsize
  integer :: ibuf
  integer :: idat
  integer :: maxx
  logical :: ok

  common /icom/ ibuf(8),idat(1)
  common /machtyp/ machine, intsize
  !----------------------------------------------------------------------
  maxx = ( nlon * nlat + 8) * machine
  !
  !     * read some time-invariant quantities from ctems an file, nucteman
  !
  !     * specified wetland fraction.
  !
  !     call recget(nuan,nc4to8("LABL"),-1,nc4to8("LABL"),-1,ibuf,maxx,ok)
  !     call getggbx(wetfpak,nc4to8("WETF"),nuan,nlon,nlat,1,1,gg)
  !     IF (.NOT. OK)                           CALL XIT('GETCTEMAN',-3)
  wetfpak(:) = 0

  !
  !     * slope based wetland fraction.
  !
  call recget(nuan,nc4to8("LABL"), - 1,nc4to8("LABL"), - 1,ibuf,maxx,ok)
  call getggbx(wetspak,nc4to8("WETS"),nuan,nlon,nlat,1,1,gg)
  if (.not. ok)                           call xit('GETCTEMAN', - 1)
  !
  !     * lightning frequency for the current month.
  !
  call recget(nuan,nc4to8("LABL"), - 1,nc4to8("LABL"), - 1,ibuf,maxx,ok)
  call getggbx(lghtpak,nc4to8("LGT1"),nuan,nlon,nlat,month,1,gg)
  if (.not. ok)                           call xit('GETCTEMAN', - 2)
  !
  close(nuan)

  return
end
