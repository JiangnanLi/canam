#include "cppdef_config.h"
!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtviros for sampled burdens.
!     * aug 23/2018 - vivek arora - remove pfhc and pexf
!     * aug 21/2018 - m.lazare.     remove {alsw,allw}.
!     * aug 14/2018 - m.lazare.     remove maskrow,gtrol.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}. also phierow.
!     * nov 01/2017 - j. cole.     - add arrays to support cmip6 stratospheric
!     *                              aerosols.
!     * aug 10/2017 - m.lazare.    - add fnrot.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     *                            - wsnorot dimension changed from
!     *                              to im to support use over
!     *                              sea/lake ice.
!     * aug 07/2017 - m.lazare.    - initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstrol,cltrol} removed.
!     *                            - add (for nemo/cice support):
!     *                              hsearol,rainsrol,snowsrol
!     *                              obegrol,obwgrol,begorol,bwgorol,
!     *                              begirol,hflirol,
!     *                              hsearol,rainsrol,snowsrol.
!     *                            - remove: ftilrow.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class): thlwrot.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvrot,algdnrot,algwvrot,algwnrot,
!     *                              socirot.
!     *                            - add (for fractional land/water/ice):
!     *                              salbrot,csalrot,emisrow,emisrot,
!     *                              wrkarol,wrkbrol,snorowo.
!     *                            - add (for pla):
!     *                              psvvrow,pdevrow,pdivrow,prevrow,privrow.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - ntld used instead of im for land-only
!     *                              mosaic fields.
!     *                            - flakrow,flndrow,gtrot,licnrow added.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "ROT" arrays.
!     * jul 10/2013 - m.lazare/    previous version compak11 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "IM" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well:{fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 19/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - for selected variables, add new tile
!     *                              versions (rot) or convert from row to rot.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 07/2012 - m.lazare/    previous version comrow10 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     *                            - "FLGROL_R" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "FDLROL_R-SIGMA*T**4".
!     *                            - include "ISEED" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version comrow9i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("FSOL").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "COUPLER_CTEM".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "HISTEMI"
!     *                              converted to cpp:
!     *                              "TRANSIENT_AEROSOL_EMISSIONS" with
!     *                              further choice of cpp directives
!     *                              "HISTEMI" for historical or
!     *                              "EMISTS" for future emissions.
!     *                              the "HISTEMI" fields are the
!     *                              same as previously. for "EMISTS",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("B" for black
!     *                              carbon, "O" for organic carbon and
!     *                              "S" for sulfur), while for each of
!     *                              these, there are "AIR" for aircraft,
!     *                              "SFC" for surface, "STK" for stack
!     *                              and "FIR" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 20/2009 - m.lazare.    previous version comrow9h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%DF EXPLVOL". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version comrow9g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2007 - m.lazare/    previous version comrow9f for gcm15f:
!     *               j.cole.      - add isccp simulator fields from
!     *                              jason.
!     *                            - two extra new fields ("dmc" and "smc")
!     *                              under control of "%if def,xtraconv".
!     *                            - remove "buoy" (now in physicff).
!===================================================================
!     * general physics arrays.
!
real :: luinrow !<
real :: luimrow !<
real :: lrinrow !<
real :: lrimrow !<
real :: ilmorow !<
real :: llakrow !<
real :: ldmxrow !<
real :: lzicrow !<
#if defined (flake)
real :: ltavrow !<
real :: lticrow !<
real :: ltsnrow !<
real :: lshprow !<
real :: ltmxrow !<
real :: ltwbrow !<
real :: lzsnrow !<
#endif
#if defined (cslm)
real :: nlklrow !<
#endif
real :: lamnrot !<
real :: lamxrot !<
real :: lnz0rot !<
real :: mvrot !<
real :: mvrow !<
real :: no3row !<
real :: nh3row !<
real :: nh4row !<
real :: no3rol !<
real :: nh3rol !<
real :: nh4rol !<
real :: lw_ext_sa_row !<
real :: lw_ext_sa_rol !<
real :: lw_ssa_sa_row !<
real :: lw_ssa_sa_rol !<
real :: lw_g_sa_row !<
real :: lw_g_sa_rol !<
  real :: abb1rol
  real :: abb1row
  real :: abb2rol
  real :: abb2row
  real :: abb3rol
  real :: abb3row
  real :: abb4rol
  real :: abb4row
  real :: abb5rol
  real :: abb5row
  real :: abbtrol
  real :: abbtrow
  real :: abs1rol
  real :: abs1row
  real :: abs2rol
  real :: abs2row
  real :: abs3rol
  real :: abs3row
  real :: abs4rol
  real :: abs4row
  real :: abs5rol
  real :: abs5row
  real :: abstrol
  real :: abstrow
  real :: ailcbrot
  real :: ailcgrot
  real :: ailcrot
  real :: algdnrot
  real :: algdvrot
  real :: algwnrot
  real :: algwvrot
  real :: alicrot
  real :: almcrow
  real :: almxrow
  real :: alphrow
  real :: altirow
  real :: alvcrot
  real :: ancgrtl
  real :: ancsrtl
  real :: anrot
  real :: anrow
  real :: bairrol
  real :: bairrow
  real :: baltrol
  real :: bbiorol
  real :: bbiorow
  real :: bcdrol
  real :: bcdrow
  real :: bcsnrot
  real :: bcsnrow
  real :: bcsrow
  real :: bdtfrow
  real :: begirol
  real :: begkrol
  real :: beglrol
  real :: begorol
  real :: begrol
  real :: betfrow
  real :: bfirrol
  real :: bfirrow
  real :: birot
  real :: blakrow
  real :: bleafcrot
  real :: bmasvrot
  real :: brfrrow
  real :: bsfcrol
  real :: bsfcrow
  real :: bshirol
  real :: bshirow
  real :: bstkrol
  real :: bstkrow
  real :: bwgirol
  real :: bwgkrol
  real :: bwglrol
  real :: bwgorol
  real :: bwgrol
  real :: c3crrow
  real :: c3grrow
  real :: c3pfrow
  real :: c4crrow
  real :: c4grrow
  real :: c4pfrow
  real :: calicrot
  real :: calvcrot
  real :: caperow
  real :: cbmfrol
  real :: cbrnrow
  real :: cdcbrol
  real :: cdcbrow
  real :: cdebrot
  real :: cdebrow
  real :: cfcanrot
  real :: cffdrow
  real :: cffvrow
  real :: cfgprol
  real :: cfgprow
  real :: cfhtrow
  real :: cfldrow
  real :: cflfrow
  real :: cflhrow
  real :: cfluxcgrot
  real :: cfluxcsrot
  real :: cflvrow
  real :: cfnbrow
  real :: cfnerow
  real :: cfnlrow
  real :: cfnprow
  real :: cfnrrow
  real :: cfnsrow
  real :: cfrdrol
  real :: cfrdrow
  real :: cfrgrow
  real :: cfrhrol
  real :: cfrhrow
  real :: cfrmrow
  real :: cfrvrol
  real :: cfrvrow
  real :: ch4hrow
  real :: ch4nrow
  real :: chfxrow
  real :: chumrot
  real :: chumrow
  real :: cicrow
  real :: cict3hrow
  real :: cictrol
  real :: cictrow
  real :: cinhrow
  real :: clairow
  real :: clayrot
  real :: clbrol
  real :: clbrot
  real :: clcvrow
  real :: cldarol
  real :: cldarow
  real :: cldorol
  real :: cldorow
  real :: cldrow
  real :: cldt3hrow
  real :: cldtrol
  real :: cldtrow
  real :: clndrow
  real :: clwrow
  real :: clwt3hrow
  real :: clwtrol
  real :: clwtrow
  real :: cmascrot
  real :: cmasrot
  real :: co2cg1rot
  real :: co2cg2rot
  real :: co2cs1rot
  real :: co2cs2rot
  real :: cqfxrow
  real :: crpfrow
  real :: csalrol
  real :: csalrot
  real :: csbrol
  real :: csbrot
  real :: cscbrol
  real :: cscbrow
  real :: csdbrol
  real :: csdbrot
  real :: csdrol
  real :: csdrot
  real :: csfbrol
  real :: csfbrot
  real :: csfrol
  real :: csfrot
  real :: cszrol
  real :: curfrow
  real :: cvarrow
  real :: cvegrot
  real :: cvegrow
  real :: cvglrow
  real :: cvgrrow
  real :: cvgsrow
  real :: cvmcrow
  real :: cvsgrow
  real :: cw1drow
  real :: cw2drow
  real :: dd4row
  real :: dd6row
  real :: ddbrow
  real :: dddrow
  real :: ddorow
  real :: ddsrow
  real :: deltrow
  real :: delurow
  real :: depbrow
  real :: dlzwrot
  real :: dmcdrow
  real :: dmcrol
  real :: dmcrow
  real :: dmcurow
  real :: dmsorol
  real :: dmsorow
  real :: dox4row
  real :: doxdrow
  real :: dpthrot
  real :: dpthrow
  real :: drnrot
  real :: drrow
  real :: dtmprow
  real :: dustrow
  real :: duthrow
  real :: duwdrow
  real :: dzgrow
  real :: ea55rol
  real :: ea55row
  real :: eaibrow
  real :: eaiorow
  real :: eaisrow
  real :: edmsrol
  real :: edmsrow
  real :: edslrow
  real :: edsorow
  real :: efibrow
  real :: efiorow
  real :: efisrow
  real :: ehcvrow
  real :: ehevrow
  real :: emisrot
  real :: emisrow
  real :: envrow
  real :: eostrol
  real :: eostrow
  real :: escvrow
  real :: esdrow
  real :: esevrow
  real :: esfbrow
  real :: esforow
  real :: esfsrow
  real :: estbrow
  real :: estorow
  real :: estsrow
  real :: esvcrow
  real :: esverow
  real :: exb1rol
  real :: exb1row
  real :: exb2rol
  real :: exb2row
  real :: exb3rol
  real :: exb3row
  real :: exb4rol
  real :: exb4row
  real :: exb5rol
  real :: exb5row
  real :: exbtrol
  real :: exbtrow
  real :: expwrow
  real :: exs1rol
  real :: exs1row
  real :: exs2rol
  real :: exs2row
  real :: exs3rol
  real :: exs3row
  real :: exs4rol
  real :: exs4row
  real :: exs5rol
  real :: exs5row
  real :: exstrol
  real :: exstrow
  real :: fa10row
  real :: fa1row
  real :: fa2row
  real :: fairrol
  real :: fairrow
  real :: fallhrot
  real :: fallrow
  real :: farerot
  real :: fbbcrol
  real :: fbbcrow
  real :: fcancrot
  real :: fcanrot
  real :: fcaprow
  real :: fcolrot
  real :: fcolrow
  real :: fdl3hrow
  real :: fdlc3hrow
  real :: fdlcrol
  real :: fdlcrot
  real :: fdlcrow
  real :: fdlrol
  real :: fdlrot
  real :: fdlrow
  real :: fladrol
  real :: flamrol
  real :: flamrow
  real :: flanrol
  real :: flanrow
  real :: flaurol
  real :: flcdrol
  real :: flcurol
  real :: flg3hrow
  real :: flgcrow
  real :: flggrow
  real :: flglrow
  real :: flgnrow
  real :: flgrol
  real :: flgrot
  real :: flgrow
  real :: flgvrow
  real :: flkrrow
  real :: flkurow
  real :: flndrow
  real :: fnlarow
  real :: fnlrow
  real :: fnrot
  real :: fnrow
  real :: fsadrol
  real :: fsamrol
  real :: fsamrow
  real :: fsanrol
  real :: fsanrow
  real :: fsaurol
  real :: fscdrol
  real :: fscurol
  real :: fsd3hrow
  real :: fsdbrol
  real :: fsdbrot
  real :: fsdcrow
  real :: fsdrol
  real :: fsdrot
  real :: fsdrow
  real :: fsfbrol
  real :: fsfbrot
  real :: fsfrol
  real :: fsfrot
  real :: fsg3hrow
  real :: fsgc3hrow
  real :: fsgcrol
  real :: fsgcrow
  real :: fsggrow
  real :: fsgirol
  real :: fsglrow
  real :: fsgnrow
  real :: fsgorol
  real :: fsgrol
  real :: fsgrot
  real :: fsgrow
  real :: fsgvrow
  real :: fsirol
  real :: fsirot
  real :: fslorol
  real :: fslorow
  real :: fsnowrtl
  real :: fso3hrow
  real :: fsorol
  real :: fsorow
  real :: fsr3hrow
  real :: fsrc3hrow
  real :: fsrcrol
  real :: fsrcrow
  real :: fsrrol
  real :: fsrrow
  real :: fss3hrow
  real :: fssbrol
  real :: fssbrot
  real :: fssc3hrow
  real :: fsscbrol
  real :: fsscbrot
  real :: fsscrol
  real :: fsscrow
  real :: fssrol
  real :: fssrow
  real :: fsvrol
  real :: fsvrot
  real :: fsvrow
  real :: ftoxrow
  real :: ftoyrow
  real :: fvgrow
  real :: fvnrow
  real :: gamrow
  real :: garow
  real :: gflxrow
  real :: gicnrow
  real :: gleafcrot
  real :: gredrow
  real :: grksrot
  real :: growtrot
  real :: grsfrow
  real :: gsnorow
  real :: gtarow
  real :: gtrol
  real :: gtrot
  real :: gtrow
  real :: gtrox
  real :: gustrow
  real :: h2o2rol
  real :: h2o2row
  real :: hblrow
  real :: hcpsrot
  real :: hfcgrow
  real :: hfclrow
  real :: hfcnrow
  real :: hfcvrow
  real :: hfl3hrow
  real :: hflgrow
  real :: hflirol
  real :: hfllrow
  real :: hflnrow
  real :: hflrow
  real :: hflvrow
  real :: hfs3hrow
  real :: hfsgrow
  real :: hfsirol
  real :: hfslrow
  real :: hfsnrow
  real :: hfsrow
  real :: hfsvrow
  real :: hlakrow
  real :: hmfgrow
  real :: hmflrow
  real :: hmfnrow
  real :: hmfvrow
  real :: hno3rol
  real :: hno3row
  real :: hrlcrow
  real :: hrlrow
  real :: hrscrow
  real :: hrsrow
  real :: hsearol
  integer :: igdrrot
  integer :: iseedrow
  integer :: isndrot
  integer :: ndtfrow
  integer :: netfrow
  real :: noxdrow !<
  real :: o3crol
  real :: o3crow
  real :: o3rol
  real :: o3row
  real :: oairrol
  real :: oairrow
  real :: obiorol
  real :: obiorow
  real :: odb1rol
  real :: odb1row
  real :: odb2rol
  real :: odb2row
  real :: odb3rol
  real :: odb3row
  real :: odb4rol
  real :: odb4row
  real :: odb5rol
  real :: odb5row
  real :: odbtrol
  real :: odbtrow
  real :: odbvrol
  real :: odbvrow
  real :: ods1rol
  real :: ods1row
  real :: ods2rol
  real :: ods2row
  real :: ods3rol
  real :: ods3row
  real :: ods4rol
  real :: ods4row
  real :: ods5rol
  real :: ods5row
  real :: odstrol
  real :: odstrow
  real :: odsvrol
  real :: odsvrow
  real :: ofb1rol
  real :: ofb1row
  real :: ofb2rol
  real :: ofb2row
  real :: ofb3rol
  real :: ofb3row
  real :: ofb4rol
  real :: ofb4row
  real :: ofb5rol
  real :: ofb5row
  real :: ofbtrol
  real :: ofbtrow
  real :: ofirrol
  real :: ofirrow
  real :: ofs1rol
  real :: ofs1row
  real :: ofs2rol
  real :: ofs2row
  real :: ofs3rol
  real :: ofs3row
  real :: ofs4rol
  real :: ofs4row
  real :: ofs5rol
  real :: ofs5row
  real :: ofsgrol
  real :: ofstrol
  real :: ofstrow
  real :: ohrol
  real :: ohrow
  real :: olr3hrow
  real :: olrc3hrow
  real :: olrcrol
  real :: olrcrow
  real :: olrrol
  real :: olrrow
  real :: ometrow
  real :: orgmrot
  real :: osfcrol
  real :: osfcrow
  real :: oshirol
  real :: oshirow
  real :: ostkrol
  real :: ostkrow
  real :: ozrol
  real :: ozrow
  real :: paicrot
  real :: parrol
  real :: parrot
  real :: pblhrow
  real :: pbltrow
  real :: pchfrow
  real :: pcp3hrow
  real :: pcpc3hrow
  real :: pcpcrol
  real :: pcpcrow
  real :: pcpl3hrow
  real :: pcpn3hrow
  real :: pcpnrol
  real :: pcpnrow
  real :: pcprol
  real :: pcprow
  real :: pcps3hrow
  real :: pcpsrow
  real :: pdsfrol
  real :: pdsfrow
  real :: petrow
  real :: phdrow
  real :: phierow
  real :: phisrow
  real :: phlrow
  real :: phsrow
  real :: pigrow
  real :: pilrow
  real :: pinrow
  real :: pivfrow
  real :: pivlrow
  real :: plhfrow
  real :: pmsl3hrow
  real :: pmslirol
  real :: pmslrol
  real :: porgrot
  real :: porgrow
  real :: posphrot
  real :: prefrot
  real :: pressure_sa_rol
  real :: pressure_sa_row
  real :: psarow
  real :: pshfrow
  real :: psirow
  real :: psisrot
  real :: psiwrot
  real :: psrol
  real :: pwat3hrow
  real :: pwatirol
  real :: pwatrom
  real :: pwatrow
  real :: qarol
  real :: qavrot
  real :: qfgrow
  real :: qflrow
  real :: qfnrol
  real :: qfnrow
  real :: qfsirol
  real :: qfslrol
  real :: qfsorol
  real :: qfsrow
  real :: qfvfrol
  real :: qfvfrow
  real :: qfvgrow
  real :: qfvlrow
  real :: qfxrow
  real :: qtpcrol
  real :: qtpfrom
  real :: qtphrom
  real :: qtpmrol
  real :: qtpprol
  real :: qtprol
  real :: qtptrom
  real :: qtpvrol
  real :: qwf0rol
  real :: qwfmrol
  real :: rainsrol
  real :: refrot
  real :: refrow
  real :: rhomrow
  real :: rhonrot
  real :: rhonrow
  real :: rhrol
  real :: rhrow
  real :: rkhrol
  real :: rkhrow
  real :: rkmrol
  real :: rkmrow
  real :: rkqrol
  real :: rkqrow
  real :: rmatcrot
  real :: rmlcgrtl
  real :: rmlcsrtl
  real :: rof3hrol
  real :: rofbrow
  real :: rofnrow
  real :: roforol
  real :: roforow
  real :: rofrol
  real :: rofrow
  real :: rofsrow
  real :: rofvrow
  real :: rootcrot
  real :: rootdrot
  real :: roothrot
  real :: rootrot
  real :: rovgrow
  real :: rtctmrot
  real :: sairrol
  real :: sairrow
  real :: salbrol
  real :: salbrot
  real :: sandrot
  real :: sbiorol
  real :: sbiorow
  real :: scdnrow
  real :: sclfrow
  real :: sdhprow
  real :: sdo3row
  real :: sfirrol
  real :: sfirrow
  real :: sfrcrol
  real :: sicnrow
  real :: sicrow
  real :: sigxrow
  real :: skygrow
  real :: skynrow
  real :: slaicrot
  real :: slairot
  real :: slhprow
  real :: slimrlh
  real :: slimrlw
  real :: slimrol
  real :: slimrsh
  real :: slo3row
  real :: slwcrow
  real :: smcrow
  real :: smfrrow
  real :: smltrow
  real :: snorot
  real :: snorow
  real :: snowsrol
  real :: so4crol
  real :: so4crow
  real :: socirot
  real :: soilcrot
  real :: spotrow
  real :: sq3hrow
  real :: sqrow
  real :: srhnrow
  real :: srhrol
  real :: srhrow
  real :: srhxrow
  real :: ssfcrol
  real :: ssfcrow
  real :: sshirol
  real :: sshirow
  real :: sshprow
  real :: sso3row
  real :: sstkrol
  real :: sstkrow
  real :: st01row
  real :: st02row
  real :: st03row
  real :: st04row
  real :: st06row
  real :: st13row
  real :: st14row
  real :: st15row
  real :: st16row
  real :: st17row
  real :: st3hrow
  real :: stemcrot
  real :: stemhrot
  real :: stmnrow
  real :: stmxrow
  real :: strol
  real :: strow
  real :: su3hrow
  real :: surol
  real :: surow
  real :: suz0rol
  real :: suz0row
  real :: sv3hrow
  real :: svrol
  real :: svrow
  real :: sw_ext_sa_rol
  real :: sw_ext_sa_row
  real :: sw_g_sa_rol
  real :: sw_g_sa_row
  real :: sw_ssa_sa_rol
  real :: sw_ssa_sa_row
  real :: swarol
  real :: swarow
  real :: swrol
  real :: swxrow
  real :: swxurow
  real :: swxvrow
  real :: t0lkrow
  real :: tacnrow
  real :: tarol
  real :: tartl
  real :: tavrot
  real :: tbarcrtl
  real :: tbarcsrtl
  real :: tbargrtl
  real :: tbargsrtl
  real :: tbarrtl
  real :: tbasrot
  real :: tbasrow
  real :: tbndrol
  real :: tbndrow
  real :: tcanortl
  real :: tcansrtl
  real :: tcdrol
  real :: tcdrow
  real :: tcsrot
  real :: tcsrow
  real :: tcvrow
  real :: tdoxrow
  real :: tdoyrow
  real :: tempcrot
  real :: tfxrow
  real :: tgrot
  real :: tgrow
  real :: thfcrot
  real :: thicecrtl
  real :: thicrot
  real :: thisylrot
  real :: thliqcrtl
  real :: thliqgrtl
  real :: thlqrot
  real :: thlwrot
  real :: thmrot
  real :: thrarot
  real :: thrrot
  real :: tkelrow
  real :: tlakrow
  real :: tnrot
  real :: tnrow
  real :: todfcrot
  real :: tpndrot
  real :: treerow
  real :: troprol
  real :: troprow
  real :: tsfsrot
  real :: ttlcrol
  real :: ttpcrol
  real :: ttplrol
  real :: ttpmrol
  real :: ttpprol
  real :: ttprol
  real :: ttpsrol
  real :: ttpvrol
  real :: ttrot
  real :: ttrow
  real :: ttscrol
  real :: tvrot
  real :: tvrow
  real :: uarol
  real :: uerow
  real :: ufsinstrol
  real :: ufsirol
  real :: ufsorol
  real :: ufsrol
  real :: ufsrow
  real :: usmkrow
  real :: varol
  real :: vegfrow
  real :: veghrot
  real :: vfsinstrol
  real :: vfsirol
  real :: vfsorol
  real :: vfsrol
  real :: vfsrow
  real :: vgfrrow
  real :: vtaurol
  real :: vtaurow
  real :: w055_ext_gcm_sa_rol
  real :: w055_ext_gcm_sa_row
  real :: w055_ext_sa_rol
  real :: w055_ext_sa_row
  real :: w055_vtau_sa_rol
  real :: w055_vtau_sa_row
  real :: w110_ext_gcm_sa_rol
  real :: w110_ext_gcm_sa_row
  real :: w110_ext_sa_rol
  real :: w110_ext_sa_row
  real :: w110_vtau_sa_rol
  real :: w110_vtau_sa_row
  real :: wdd4row
  real :: wdd6row
  real :: wddbrow
  real :: wdddrow
  real :: wddorow
  real :: wddsrow
  real :: wdl4row
  real :: wdl6row
  real :: wdlbrow
  real :: wdldrow
  real :: wdlorow
  real :: wdlsrow
  real :: wds4row
  real :: wds6row
  real :: wdsbrow
  real :: wdsdrow
  real :: wdsorow
  real :: wdssrow
  real :: wetfrow
  real :: wetsrow
  real :: wfrarow
  real :: wgf3hrow
  real :: wgfrow
  real :: wgl3hrow
  real :: wglrow
  real :: wrkarol
  real :: wrkbrol
  real :: wsnorot
  real :: wsnorow
  real :: wtabrow
  real :: wtrgrow
  real :: wtrnrow
  real :: wtrvrow
  real :: wvfrot
  real :: wvfrow
  real :: wvlrot
  real :: wvlrow
  real :: xfsrow
  real :: xsfxrol
  real :: xsfxrow
  real :: xsrfrol
  real :: xsrfrow
  real :: xtpfrom
  real :: xtphrom
  real :: xtptrom
  real :: xtvirom
  real :: xtviros
  real :: xtvirow
  real :: xwf0rol
  real :: xwfmrol
  real :: zbtwrot
  real :: zdetrow
  real :: zgrol
  real :: znrow
  real :: zolncrot
  real :: zpndrot
  real :: zpndrow
  real :: zspdrow
!#if defined (cslm)
  real :: fsaurow_r
  real :: fsadrow_r
  real :: flaurow_r
  real :: fladrow_r
  real :: fscurow_r
  real :: fscdrow_r
  real :: flcurow_r
  real :: flcdrow_r
  real :: fsaurol_r
  real :: fsadrol_r
  real :: flaurol_r
  real :: fladrol_r
  real :: fscurol_r
  real :: fscdrol_r
  real :: flcurol_r
  real :: flcdrol_r
  real :: rdtrow_r
  real :: rdtrol_r
  real :: rdtrom_r
  real :: hrsrow_r
  real :: hrlrow_r
  real :: hrscrow_r
  real :: hrlcrow_r
  real :: fsgrol_r
  real :: csbrol_r
  real :: fssrol_r
  real :: fsscrol_r
  real :: clbrol_r
  real :: flgrol_r
  real :: fdlrol_r
  real :: fdlcrol_r
  real :: fsrrol_r
  real :: fsrcrol_r
  real :: fsorol_r
  real :: olrrol_r
  real :: olrcrol_r
  real :: fslorol_r
  real :: csbrot_r
  real :: clbrot_r
  real :: fsgrot_r
  real :: flgrot_r
  integer :: kthrol
!#endif
#if defined tprhs
  real :: ttprow
#endif
#if defined qprhs
  real :: qtprow
#endif
#if defined uprhs
  real :: utprow
#endif
#if defined vprhs
  real :: vtprow
#endif
#if defined qconsav
  real :: qaddrow
  real :: qtphrow
  real :: qtpfrow
#endif
#if defined tprhsc
  real :: ttpcrow
  real :: ttpkrow
  real :: ttpmrow
  real :: ttpnrow
  real :: ttpprow
  real :: ttpvrow
#endif
#if (defined (tprhsc) || defined (radforce))
  real :: ttplrow
  real :: ttpsrow
  real :: ttscrow
  real :: ttlcrow
#endif
#if defined qprhsc
  real :: qtpcrow
  real :: qtpmrow
  real :: qtpprow
  real :: qtpvrow
#endif
#if defined uprhsc
  real :: utpcrow
  real :: utpgrow
  real :: utpnrow
  real :: utpsrow
  real :: utpvrow
#endif
#if defined vprhsc
  real :: vtpcrow
  real :: vtpgrow
  real :: vtpnrow
  real :: vtpsrow
  real :: vtpvrow
#endif
#if defined xconsav
  real :: xaddrow
  real :: xtphrow
  real :: xtpfrow
#endif
#if defined xprhs
  real :: xtprow
#endif
#if defined xprhsc
  real :: xtpcrow
  real :: xtpmrow
  real :: xtpprow
  real :: xtpvrow
#endif
!
common /row/ almcrow (ilg,ilev)
common /row/ almxrow (ilg,ilev)
common /row/ cicrow  (ilg,ilev)
common /row/ clcvrow (ilg,ilev)
common /row/ cldrow  (ilg,ilev)
common /row/ clwrow  (ilg,ilev)
common /row/ csalrol (ilg,nbs)
common /row/ csalrot (ilg,im,nbs)
common /row/ cvarrow (ilg,ilev)
common /row/ cvmcrow (ilg,ilev)
common /row/ cvsgrow (ilg,ilev)
common /row/ h2o2rol (ilg,levox)
common /row/ h2o2row (ilg,levox)
common /row/ hno3rol (ilg,levox)
common /row/ hno3row (ilg,levox)
common /row/ hrlrow  (ilg,ilev)
common /row/ hrsrow  (ilg,ilev)
common /row/ hrlcrow (ilg,ilev)
common /row/ hrscrow (ilg,ilev)
common /row/ nh3rol  (ilg,levox)
common /row/ nh3row  (ilg,levox)
common /row/ nh4rol  (ilg,levox)
common /row/ nh4row  (ilg,levox)
common /row/ no3rol  (ilg,levox)
common /row/ no3row  (ilg,levox)
common /row/ o3rol   (ilg,levox)
common /row/ o3row   (ilg,levox)
common /row/ o3crol  (ilg,levozc)
common /row/ o3crow  (ilg,levozc)
common /row/ ohrol   (ilg,levox)
common /row/ ohrow   (ilg,levox)
common /row/ ometrow (ilg,ilev)
common /row/ ozrol   (ilg,levoz)
common /row/ ozrow   (ilg,levoz)
common /row/ qwf0rol (ilg,ilev)
common /row/ qwfmrol (ilg,ilev)
common /row/ rhrow   (ilg,ilev)
common /row/ salbrol (ilg,nbs)
common /row/ salbrot (ilg,im,nbs)
common /row/ scdnrow (ilg,ilev)
common /row/ sclfrow (ilg,ilev)
common /row/ slwcrow (ilg,ilev)
common /row/ tacnrow (ilg,ilev)
common /row/ tbndrol (ilg,ilev)
common /row/ tbndrow (ilg,ilev)
common /row/ ea55rol (ilg,ilev)
common /row/ ea55row (ilg,ilev)
common /row/ wrkarol (ilg,nbs)
common /row/ wrkbrol (ilg,nbs)
common /row/ zdetrow (ilg,ilev)
common /row/ sfrcrol (ilg,ilev,ntrac)
common /row/ altirow (ilg,ilev)
!
!     * ftoxrow,ftoyrow - x,y components of oro gw momentum flux.
!     * tdoxrow,tdoyrow - x,y components of oro gw tendencies.
!
common /row/ ftoxrow (ilg,ilev),ftoyrow (ilg,ilev)
common /row/ tdoxrow (ilg,ilev),tdoyrow (ilg,ilev)
!
!     * array for in angle from due east to local zonal direction
!     * here in the spectral model this is always zero, and so no pack arrays are needed
!     * this row array will be zeroed out in the core routine before physics is called.
!
common /row/ phierow(ilg)
!
common /row/ alphrow (ilg)
common /row/ begirol (ilg)
common /row/ begkrol (ilg)
common /row/ beglrol (ilg)
common /row/ begorol (ilg)
common /row/ begrol  (ilg)
common /row/ bwgirol (ilg)
common /row/ bwgkrol (ilg)
common /row/ bwglrol (ilg)
common /row/ bwgorol (ilg)
common /row/ bwgrol  (ilg)
common /row/ cbmfrol (ilg)
common /row/ chfxrow (ilg)
common /row/ cictrow (ilg)
common /row/ clbrol  (ilg)
common /row/ clbrot (ilg,im)
common /row/ cldtrow (ilg)
common /row/ cldorol (ilg)
common /row/ cldorow (ilg)
common /row/ cldarol (ilg)
common /row/ cldarow (ilg)
common /row/ clwtrow (ilg)
common /row/ cqfxrow (ilg)
common /row/ csbrol  (ilg)
common /row/ csbrot (ilg,im)
common /row/ csdrol  (ilg)
common /row/ csdrot (ilg,im)
common /row/ csfrol  (ilg)
common /row/ csfrot (ilg,im)
common /row/ cszrol  (ilg)
common /row/ deltrow (ilg)
common /row/ dmsorol (ilg)
common /row/ dmsorow (ilg)
common /row/ edmsrol (ilg)
common /row/ edmsrow (ilg)
common /row/ emisrow (ilg)
common /row/ emisrot (ilg,im)
common /row/ envrow  (ilg)
common /row/ fdlcrow (ilg)
common /row/ fdlcrot (ilg,im)
common /row/ fdlrot (ilg,im)
common /row/ fdlrow  (ilg)
common /row/ flamrol (ilg)
common /row/ flamrow (ilg)
common /row/ flanrow (ilg)
common /row/ flanrol (ilg)
common /row/ flgcrow (ilg)
common /row/ flgrow  (ilg)
common /row/ flgrot (ilg,im)
common /row/ flkrrow (ilg)
common /row/ flkurow (ilg)
common /row/ flndrow (ilg)
common /row/ fsamrol (ilg)
common /row/ fsamrow (ilg)
common /row/ fsanrow (ilg)
common /row/ fsanrol (ilg)
common /row/ fsdcrow (ilg)
common /row/ fsdrol  (ilg)
common /row/ fsdrot (ilg,im)
common /row/ fsdrow  (ilg)
common /row/ fsfrol  (ilg)
common /row/ fsfrot (ilg,im)
common /row/ fsgcrow (ilg)
common /row/ fsgirol(ilg)
common /row/ fsgorol(ilg)
common /row/ fsgrot (ilg,im)
common /row/ fsgrow  (ilg)
common /row/ fsirol  (ilg)
common /row/ fsirot (ilg,im)
common /row/ fslorol (ilg)
common /row/ fslorow (ilg)
common /row/ fsorow  (ilg)
common /row/ fsrrow  (ilg)
common /row/ fsrcrow (ilg)
common /row/ fsscrow (ilg)
common /row/ fssrow  (ilg)
common /row/ fsvrol  (ilg)
common /row/ fsvrow  (ilg)
common /row/ fsvrot (ilg,im)
common /row/ fsdbrol (ilg,nbs)
common /row/ fsdbrot (ilg,im,nbs)
common /row/ fsfbrol (ilg,nbs)
common /row/ fsfbrot (ilg,im,nbs)
common /row/ csdbrol (ilg,nbs)
common /row/ csdbrot (ilg,im,nbs)
common /row/ csfbrol (ilg,nbs)
common /row/ csfbrot (ilg,im,nbs)
common /row/ fssbrol (ilg,nbs)
common /row/ fssbrot (ilg,im,nbs)
common /row/ fsscbrol(ilg,nbs)
common /row/ fsscbrot(ilg,im,nbs)
common /row/ gamrow  (ilg)
common /row/ gicnrow (ilg)
common /row/ gtarow  (ilg)
common /row/ gtrow   (ilg)
common /row/ gtrot   (ilg,im)
common /row/ gtrox   (ilg,im)
common /row/ hflrow  (ilg)
common /row/ hfsrow  (ilg)
common /row/ hsearol (ilg)
common /row/ ofsgrol (ilg)
common /row/ olrrow  (ilg)
common /row/ olrcrow (ilg)
common /row/ parrol  (ilg)
common /row/ parrot (ilg,im)
common /row/ pblhrow (ilg)
common /row/ pbltrow (ilg)
common /row/ pchfrow (ilg)
common /row/ pcpcrow (ilg)
common /row/ pcprow  (ilg)
common /row/ pcpsrow (ilg)
common /row/ phisrow(ilg)
common /row/ plhfrow (ilg)
common /row/ pmslrol (ilg)
common /row/ psarow  (ilg)
common /row/ pshfrow (ilg)
common /row/ psirow  (ilg)
common /row/ pwatrom (ilg)
common /row/ pwatrow (ilg)
common /row/ qfsrow  (ilg)
common /row/ qfslrol (ilg)
common /row/ qfsorol (ilg)
common /row/ qfxrow  (ilg)
common /row/ qtpfrom (ilg)
common /row/ qtphrom (ilg)
common /row/ qtptrom (ilg)
common /row/ rainsrol(ilg)
common /row/ sicnrow (ilg)
common /row/ sicrow  (ilg)
common /row/ sigxrow (ilg)
common /row/ slimrol (ilg)
common /row/ slimrlw(ilg),slimrsh(ilg),slimrlh(ilg)
common /row/ snorow  (ilg)
common /row/ snorot  (ilg,im)
common /row/ snowsrol(ilg)
common /row/ sqrow   (ilg)
common /row/ srhrow  (ilg)
common /row/ srhnrow (ilg)
common /row/ srhxrow (ilg)
common /row/ stmnrow (ilg)
common /row/ stmxrow (ilg)
common /row/ strow   (ilg)
common /row/ surow   (ilg)
common /row/ svrow   (ilg)
common /row/ swarow  (ilg)
common /row/ swarol  (ilg)
common /row/ swxrow  (ilg)
common /row/ swxurow (ilg)
common /row/ swxvrow (ilg)
common /row/ tcvrow  (ilg)
common /row/ tfxrow  (ilg)
common /row/ ufsrow  (ilg)
common /row/ ufsrol  (ilg)
common /row/ ufsirol(ilg)
common /row/ ufsorol(ilg)
common /row/ vfsrow  (ilg)
common /row/ vfsrol  (ilg)
common /row/ vfsirol(ilg)
common /row/ vfsorol(ilg)
common /row/ rkmrow (ilg,ilev) ! momentum eddy diffusivities
common /row/ rkhrow (ilg,ilev) ! temperature eddy diffusivities
common /row/ rkqrow (ilg,ilev) ! water vapour eddy diffusivities

real :: tkemrow, xlmrow, svarrow, xlmrot

COMMON /ROW/ TKEMROW(ILG,ILEV)
COMMON /ROW/ XLMROW (ILG,ILEV)
COMMON /ROW/ SVARROW(ILG,ILEV)
COMMON /ROW/ XLMROT (ILG,IM,ILEV)



!     instantaneous variables for sampling

common /row/ swrol(ilg)      ! wind speed
common /row/ srhrol(ilg)     ! near-surface relative humidity
common /row/ pcprol(ilg)     ! total precip
common /row/ pcpnrol(ilg)    ! snow precip
common /row/ pcpcrol(ilg)    ! convective precip
common /row/ qfsirol(ilg)    ! evaporation including sublimation and transpiration
common /row/ qfnrol(ilg)     ! surface snow and ice sublimation flux
common /row/ qfvfrol(ilg)    !
common /row/ ufsinstrol(ilg) ! surface downward eastward wind stress
common /row/ vfsinstrol(ilg) ! surface downward northward wind stress
common /row/ hflirol(ilg)    ! surface upward latent heat flux
common /row/ hfsirol(ilg)    ! surface upward sensible heat flux
common /row/ fdlrol(ilg)     ! surface downwelling longwave radiation
common /row/ flgrol(ilg)     ! net longwave radiation (subtract from fdl to get upward flux)
common /row/ fssrol(ilg)     ! surface downwelling shortwave radiation
common /row/ fsgrol(ilg)     ! net shortwave radiation (subtract from fss to get upward flux)
common /row/ fsscrol(ilg)    ! surface downwelling clear-sky shortwave radiation
common /row/ fsgcrol(ilg)    ! surface upwelling clear-sky shortwave radiation (subtract from fssc to get upward flux)
common /row/ fdlcrol (ilg)   ! surface downwelling clear-sky longwave radiation
common /row/ fsorol  (ilg)   ! toa incident shortwave radiation
common /row/ fsrrol  (ilg)   ! top-of-atmosphere outgoing shortwave radiation
common /row/ olrrol  (ilg)   ! toa outgoing longwave radiation
common /row/ olrcrol (ilg)   ! toa outgoing clear-sky longwave radiation
common /row/ fsrcrol (ilg)   ! toa outgoing clear-sky shortwave radiation
common /row/ pwatirol(ilg)   ! water vapor path
common /row/ cldtrol (ilg)   ! total cloud cover percentage
common /row/ clwtrol (ilg)   ! condensed water path (liquid)
common /row/ cictrol(ilg)    ! condensed water path (ice)
common /row/ baltrol(ilg)    ! net downward radiative flux at top of model
common /row/ pmslirol(ilg)   ! sea level pressure
common /row/ cdcbrol(ilg)    ! fraction of time convection occurs in cell
common /row/ cscbrol(ilg)    ! fraction of time shallow convection occurs
common /row/ gtrol(ilg)      ! surface temperature

common /row/ tcdrol(ilg)     ! pressure at top of convection
common /row/ bcdrol(ilg)     ! pressure at bottom of convection
common /row/ psrol(ilg)      ! surface pressure

common /row/ strol  (ilg)    ! near surface temperature
common /row/ surol  (ilg)    ! near surface east wind
common /row/ svrol  (ilg)    ! near surface north wind

! instantaneous 3d fields
common /row/ dmcrol(ilg,ilev) ! net convective mass flux
common /row/ tarol(ilg,ilev)  ! temperature (from physics)
common /row/ uarol(ilg,ilev)  ! eastward wind (from physics)
common /row/ varol(ilg,ilev)  ! northward wind (from physics)
common /row/ qarol(ilg,ilev)  ! specific humidity (from physics)
common /row/ rhrol(ilg,ilev)  ! relative humidity (from physics)
common /row/ zgrol(ilg,ilev)  ! geopotential height (from physics)
common /row/ rkhrol(ilg,ilev)  ! eddy coefficient for temperature
common /row/ rkmrol(ilg,ilev)  ! eddy coefficient for momentum
common /row/ rkqrol(ilg,ilev)  ! water vapour eddy diffusivities
common /row/ ttlcrol(ilg,ilev) ! clear-sky lw heating rate
common /row/ ttscrol(ilg,ilev) ! clear-sky sw heating rate

common /row/ ttprol(ilg,ilev)  ! temperature tendency from model physics
common /row/ ttpprol(ilg,ilev)  !
common /row/ ttpvrol(ilg,ilev)  !
common /row/ ttplrol(ilg,ilev)  ! temperature tendency from lw heating
common /row/ ttpsrol(ilg,ilev)  ! temperature tendency from sw heating
common /row/ ttpcrol(ilg,ilev)  ! temperature tendency from convection
common /row/ ttpmrol(ilg,ilev)  ! temperature tendency from convection

common /row/ qtpcrol(ilg,ilev)  ! specific humidity tendency from convection
common /row/ qtpmrol(ilg,ilev)  ! specific humidity tendency from convection
common /row/ qtpprol(ilg,ilev)  !
common /row/ qtpvrol(ilg,ilev)  !
common /row/ qtprol(ilg,ilev)  ! specific humidity tendency from model physics

!
!     * 3-hour save fields.
!
common /row/ cldt3hrow(ilg)
common /row/ hfl3hrow (ilg)
common /row/ hfs3hrow (ilg)
common /row/ rof3hrol (ilg)
common /row/ wgl3hrow (ilg)
common /row/ wgf3hrow (ilg)
common /row/ pcp3hrow (ilg)
common /row/ pcpc3hrow(ilg)
common /row/ pcpl3hrow  (ilg)
common /row/ pcps3hrow(ilg)
common /row/ pcpn3hrow(ilg)
common /row/ fdl3hrow (ilg)
common /row/ fdlc3hrow(ilg)
common /row/ flg3hrow (ilg)
common /row/ fss3hrow (ilg)
common /row/ fssc3hrow(ilg)
common /row/ fsd3hrow (ilg)
common /row/ fsg3hrow (ilg)
common /row/ fsgc3hrow(ilg)
common /row/ sq3hrow  (ilg)
common /row/ st3hrow  (ilg)
common /row/ su3hrow  (ilg)
common /row/ sv3hrow  (ilg)

common /row/ olr3hrow  (ilg) ! all-sky outgoing thermal toa
common /row/ fsr3hrow  (ilg) ! all-sky outgoing solar toa
common /row/ olrc3hrow (ilg) ! clear-sky outgoing thermal toa
common /row/ fsrc3hrow (ilg) ! clear-sky outgoing solar toa
common /row/ fso3hrow  (ilg) ! incident solar at toa
common /row/ pwat3hrow (ilg) ! precipitable water
common /row/ clwt3hrow (ilg) ! vertically integrated non-precipitating liquid water
common /row/ cict3hrow (ilg) ! vertically integrated non-precipitating ice water
common /row/ pmsl3hrow (ilg) ! sea-level pressure

!
!     * others.
!
common /row/ iseedrow(ilg,4)
!
!     * lakes.
!
common /row/ blakrow(ilg)
common /row/ hlakrow(ilg)
common /row/ ldmxrow(ilg)
common /row/ llakrow(ilg)
common /row/ lrimrow(ilg)
common /row/ lrinrow(ilg)
common /row/ luimrow(ilg)
common /row/ luinrow(ilg)
common /row/ lzicrow(ilg)
#if defined (cslm)
      common /row/ delurow(ilg)
      common /row/ dtmprow(ilg)
      common /row/ expwrow(ilg)
      common /row/ gredrow(ilg)
      common /row/ nlklrow(ilg)
      common /row/ rhomrow(ilg)
      common /row/ t0lkrow(ilg)
      common /row/ tkelrow(ilg)
      common /row/ tlakrow(ilg,nlklm)
!
      common /row/ flglrow(ilg)
      common /row/ fnlrow (ilg)
      common /row/ fsglrow(ilg)
      common /row/ hfclrow(ilg)
      common /row/ hfllrow(ilg)
      common /row/ hfslrow(ilg)
      common /row/ hmflrow(ilg)
      common /row/ pilrow (ilg)
      common /row/ qflrow (ilg)
#endif
#if defined (flake)
      common /row/ lshprow (ilg)
      common /row/ ltavrow (ilg)
      common /row/ lticrow (ilg)
      common /row/ ltmxrow (ilg)
      common /row/ ltsnrow (ilg)
      common /row/ ltwbrow (ilg)
      common /row/ lzsnrow (ilg)
#endif
!
!     * emissions.
!
common /row/ eostrow (ilg)
common /row/ eostrol (ilg)
!
common /row/ escvrow (ilg)
common /row/ ehcvrow (ilg)
common /row/ esevrow (ilg)
common /row/ ehevrow (ilg)
!
#if defined transient_aerosol_emissions
      common /row/ fbbcrow (ilg,levwf)
      common /row/ fbbcrol (ilg,levwf)
      common /row/ fairrow (ilg,levair)
      common /row/ fairrol (ilg,levair)
#if defined emists
      common /row/  sairrow(ilg)
      common /row/  ssfcrow(ilg)
      common /row/  sbiorow(ilg)
      common /row/  sshirow(ilg)
      common /row/  sstkrow(ilg)
      common /row/  sfirrow(ilg)
      common /row/  sairrol(ilg)
      common /row/  ssfcrol(ilg)
      common /row/  sbiorol(ilg)
      common /row/  sshirol(ilg)
      common /row/  sstkrol(ilg)
      common /row/  sfirrol(ilg)
      common /row/  oairrow(ilg)
      common /row/  osfcrow(ilg)
      common /row/  obiorow(ilg)
      common /row/  oshirow(ilg)
      common /row/  ostkrow(ilg)
      common /row/  ofirrow(ilg)
      common /row/  oairrol(ilg)
      common /row/  osfcrol(ilg)
      common /row/  obiorol(ilg)
      common /row/  oshirol(ilg)
      common /row/  ostkrol(ilg)
      common /row/  ofirrol(ilg)
      common /row/  bairrow(ilg)
      common /row/  bsfcrow(ilg)
      common /row/  bbiorow(ilg)
      common /row/  bshirow(ilg)
      common /row/  bstkrow(ilg)
      common /row/  bfirrow(ilg)
      common /row/  bairrol(ilg)
      common /row/  bsfcrol(ilg)
      common /row/  bbiorol(ilg)
      common /row/  bshirol(ilg)
      common /row/  bstkrol(ilg)
      common /row/  bfirrol(ilg)
#endif
#endif
!
! pla and pam variables
!
#if (defined(pla) && (defined(pfrc) || defined(pam)))
  real :: amldfrow
  real :: amldfrol
  real :: reamfrow
  real :: reamfrol
  real :: veamfrow
  real :: veamfrol
  real :: fr1frow
  real :: fr1frol
  real :: fr2frow
  real :: fr2frol
  real :: ssldfrow
  real :: ssldfrol
  real :: ressfrow
  real :: ressfrol
  real :: vessfrow
  real :: vessfrol
  real :: dsldfrow
  real :: dsldfrol
  real :: redsfrow
  real :: redsfrol
  real :: vedsfrow
  real :: vedsfrol
  real :: bcldfrow
  real :: bcldfrol
  real :: rebcfrow
  real :: rebcfrol
  real :: vebcfrow
  real :: vebcfrol
  real :: ocldfrow
  real :: ocldfrol
  real :: reocfrow
  real :: reocfrol
  real :: veocfrow
  real :: veocfrol
  real :: zcdnfrow
  real :: zcdnfrol
  real :: bcicfrow
  real :: bcicfrol
  real :: bcdpfrow
  real :: bcdpfrol
!
      common /row/  amldfrow(ilg,ilev)
      common /row/  amldfrol(ilg,ilev)
      common /row/  reamfrow(ilg,ilev)
      common /row/  reamfrol(ilg,ilev)
      common /row/  veamfrow(ilg,ilev)
      common /row/  veamfrol(ilg,ilev)
      common /row/  fr1frow(ilg,ilev)
      common /row/  fr1frol(ilg,ilev)
      common /row/  fr2frow(ilg,ilev)
      common /row/  fr2frol(ilg,ilev)
      common /row/  ssldfrow(ilg,ilev)
      common /row/  ssldfrol(ilg,ilev)
      common /row/  ressfrow(ilg,ilev)
      common /row/  ressfrol(ilg,ilev)
      common /row/  vessfrow(ilg,ilev)
      common /row/  vessfrol(ilg,ilev)
      common /row/  dsldfrow(ilg,ilev)
      common /row/  dsldfrol(ilg,ilev)
      common /row/  redsfrow(ilg,ilev)
      common /row/  redsfrol(ilg,ilev)
      common /row/  vedsfrow(ilg,ilev)
      common /row/  vedsfrol(ilg,ilev)
      common /row/  bcldfrow(ilg,ilev)
      common /row/  bcldfrol(ilg,ilev)
      common /row/  rebcfrow(ilg,ilev)
      common /row/  rebcfrol(ilg,ilev)
      common /row/  vebcfrow(ilg,ilev)
      common /row/  vebcfrol(ilg,ilev)
      common /row/  ocldfrow(ilg,ilev)
      common /row/  ocldfrol(ilg,ilev)
      common /row/  reocfrow(ilg,ilev)
      common /row/  reocfrol(ilg,ilev)
      common /row/  veocfrow(ilg,ilev)
      common /row/  veocfrol(ilg,ilev)
      common /row/  zcdnfrow(ilg,ilev)
      common /row/  zcdnfrol(ilg,ilev)
      common /row/  bcicfrow(ilg,ilev)
      common /row/  bcicfrol(ilg,ilev)
      common /row/  bcdpfrow(ilg)
      common /row/  bcdpfrol(ilg)
#endif
!
!     * general tracer arrays.
!
common /row/ xfsrow  (ilg,ntrac)
common /row/ xsfxrol (ilg,ntrac)
common /row/ xsfxrow (ilg,ntrac)
common /row/ xsrfrol (ilg,ntrac)
common /row/ xsrfrow (ilg,ntrac)
common /row/ xtpfrom (ilg,ntrac)
common /row/ xtphrom (ilg,ntrac)
common /row/ xtptrom (ilg,ntrac)
common /row/ xtviros (ilg,ntrac)
common /row/ xtvirow (ilg,ntrac)
common /row/ xtvirom (ilg,ntrac)
common /row/ xwf0rol (ilg,ilev,ntrac)
common /row/ xwfmrol (ilg,ilev,ntrac)
!
!     * land surface scheme arrays.
!
common /row/ alicrot(ilg,ntld,ican+1)
common /row/ alvcrot(ilg,ntld,ican+1)
common /row/ clayrot(ilg,ntld,ignd)
common /row/ cmasrot(ilg,ntld,ican)
common /row/ dzgrow (ilg,ignd)
common /row/ fcanrot(ilg,ntld,ican+1)
common /row/ gflxrow(ilg,ignd)
common /row/ hfcgrow(ilg,ignd)
common /row/ hmfgrow(ilg,ignd)
common /row/ lamnrot(ilg,ntld,ican)
common /row/ lamxrot(ilg,ntld,ican)
common /row/ lnz0rot(ilg,ntld,ican+1)
common /row/ orgmrot(ilg,ntld,ignd)
common /row/ porgrow(ilg,ignd)
common /row/ fcaprow(ilg,ignd)
common /row/ dlzwrot(ilg,ntld,ignd)
common /row/ porgrot(ilg,ntld,ignd)
common /row/ thfcrot(ilg,ntld,ignd)
common /row/ thlwrot(ilg,ntld,ignd)
common /row/ thrrot (ilg,ntld,ignd)
common /row/ thmrot (ilg,ntld,ignd)
common /row/ birot  (ilg,ntld,ignd)
common /row/ psisrot(ilg,ntld,ignd)
common /row/ grksrot(ilg,ntld,ignd)
common /row/ thrarot(ilg,ntld,ignd)
common /row/ hcpsrot(ilg,ntld,ignd)
common /row/ tcsrot (ilg,ntld,ignd)
common /row/ psiwrot(ilg,ntld,ignd)
common /row/ zbtwrot(ilg,ntld,ignd)
common /row/ isndrot(ilg,ntld,ignd)
common /row/ qfvgrow(ilg,ignd)
common /row/ rootrot(ilg,ntld,ican)
common /row/ sandrot(ilg,ntld,ignd)
common /row/ tgrow  (ilg,ignd)
common /row/ tgrot  (ilg,ntld,ignd)
common /row/ thicrot(ilg,ntld,ignd)
common /row/ thlqrot(ilg,ntld,ignd)
common /row/ wgfrow (ilg,ignd)
common /row/ wglrow (ilg,ignd)
!
common /row/ anrow  (ilg)
common /row/ anrot  (ilg,im)
common /row/ dpthrot(ilg,ntld)
common /row/ algdvrot(ilg,ntld)
common /row/ algdnrot(ilg,ntld)
common /row/ algwvrot(ilg,ntld)
common /row/ algwnrot(ilg,ntld)
common /row/ igdrrot(ilg,ntld)
common /row/ dpthrow(ilg)
common /row/ drnrot (ilg,ntld)
common /row/ drrow  (ilg)
common /row/ flggrow(ilg)
common /row/ flgnrow(ilg)
common /row/ flgvrow(ilg)
common /row/ fnlarow(ilg)
common /row/ fnrow  (ilg)
common /row/ fnrot  (ilg,im)
common /row/ fsggrow(ilg)
common /row/ fsgnrow(ilg)
common /row/ fsgvrow(ilg)
common /row/ fvgrow (ilg)
common /row/ fvnrow (ilg)
common /row/ garow  (ilg)
common /row/ gsnorow(ilg)
common /row/ hblrow (ilg)
common /row/ hfcnrow(ilg)
common /row/ hfcvrow(ilg)
common /row/ hflgrow(ilg)
common /row/ hflnrow(ilg)
common /row/ hflvrow(ilg)
common /row/ hfsgrow(ilg)
common /row/ hfsnrow(ilg)
common /row/ hfsvrow(ilg)
common /row/ hmfnrow(ilg)
common /row/ hmfvrow(ilg)
common /row/ ilmorow(ilg)
common /row/ mvrot  (ilg,ntld)
common /row/ mvrow  (ilg)
common /row/ pcpnrow(ilg)
common /row/ petrow (ilg)
common /row/ pigrow (ilg)
common /row/ pinrow (ilg)
common /row/ pivfrow(ilg)
common /row/ pivlrow(ilg)
common /row/ qfgrow (ilg)
common /row/ qfnrow (ilg)
common /row/ qfvfrow(ilg)
common /row/ qfvlrow(ilg)
common /row/ rhonrow(ilg)
common /row/ rhonrot(ilg,im)
common /row/ rofbrow(ilg)
common /row/ refrot (ilg,im)
common /row/ refrow(ilg)
common /row/ bcsnrot(ilg,im)
common /row/ bcsnrow(ilg)
common /row/ rofnrow(ilg)
common /row/ roforol(ilg)
common /row/ roforow(ilg)
common /row/ rofrol (ilg)
common /row/ rofrow (ilg)
common /row/ rofsrow(ilg)
common /row/ rofvrow(ilg)
common /row/ rovgrow(ilg)
common /row/ skygrow(ilg)
common /row/ skynrow(ilg)
common /row/ smltrow(ilg)
common /row/ socirot (ilg,ntld)
common /row/ tbasrow(ilg)
common /row/ tbasrot(ilg,ntld)
common /row/ tnrow  (ilg)
common /row/ tnrot  (ilg,im)
common /row/ ttrow  (ilg)
common /row/ ttrot  (ilg,ntld)
common /row/ tvrow  (ilg)
common /row/ tvrot  (ilg,ntld)
common /row/ uerow  (ilg)
common /row/ wtabrow(ilg)
common /row/ wtrgrow(ilg)
common /row/ wtrnrow(ilg)
common /row/ wtrvrow(ilg)
common /row/ wvfrow (ilg)
common /row/ wvfrot (ilg,ntld)
common /row/ wvlrow (ilg)
common /row/ wvlrot (ilg,ntld)
common /row/ znrow  (ilg)
common /row/ depbrow(ilg)
common /row/ tpndrot(ilg,ntld)
common /row/ zpndrow(ilg)
common /row/ zpndrot(ilg,ntld)
common /row/ tavrot (ilg,ntld)
common /row/ qavrot (ilg,ntld)
common /row/ wsnorow(ilg)
common /row/ wsnorot(ilg,im)

common /row/ farerot(ilg,im)
common /row/ tsfsrot(ilg,ntld,4)
#if defined (agcm_ctem)
!
!     * ctem related variables
!
      common /row/ cfcanrot  (ilg,ntld,icanp1)
      common /row/ calvcrot  (ilg,ntld,icanp1)
      common /row/ calicrot  (ilg,ntld,icanp1)

      common /row/ zolncrot  (ilg,ntld,ican)
      common /row/ cmascrot  (ilg,ntld,ican)
      common /row/ rmatcrot  (ilg,ntld,ican,  ignd)
      common /row/ rtctmrot  (ilg,ntld,ictem, ignd)

      common /row/ ailcrot   (ilg,ntld,ican)
      common /row/ paicrot   (ilg,ntld,ican)
      common /row/ slaicrot  (ilg,ntld,ican)

      common /row/ fcancrot  (ilg,ntld,ictem)
      common /row/ todfcrot  (ilg,ntld,ictem)
      common /row/ ailcgrot  (ilg,ntld,ictem)
      common /row/ slairot   (ilg,ntld,ictem)
      common /row/ co2cg1rot (ilg,ntld,ictem)
      common /row/ co2cg2rot (ilg,ntld,ictem)
      common /row/ co2cs1rot (ilg,ntld,ictem)
      common /row/ co2cs2rot (ilg,ntld,ictem)
      common /row/ ancgrtl   (ilg,ntld,ictem)
      common /row/ ancsrtl   (ilg,ntld,ictem)
      common /row/ rmlcgrtl  (ilg,ntld,ictem)
      common /row/ rmlcsrtl  (ilg,ntld,ictem)

      common /row/ fsnowrtl  (ilg,ntld)
      common /row/ tcanortl  (ilg,ntld)
      common /row/ tcansrtl  (ilg,ntld)
      common /row/ tartl     (ilg,ntld)
      common /row/ cfluxcsrot(ilg,ntld)
      common /row/ cfluxcgrot(ilg,ntld)

      common /row/ tbarrtl   (ilg,ntld,ignd)
      common /row/ tbarcrtl  (ilg,ntld,ignd)
      common /row/ tbarcsrtl (ilg,ntld,ignd)
      common /row/ tbargrtl  (ilg,ntld,ignd)
      common /row/ tbargsrtl (ilg,ntld,ignd)
      common /row/ thliqcrtl (ilg,ntld,ignd)
      common /row/ thliqgrtl (ilg,ntld,ignd)
      common /row/ thicecrtl (ilg,ntld,ignd)
!
real :: lghtrow !<
real :: litrcrot !<
real :: leafsrot !<
real :: lastrrot !<
real :: lastsrot !<
real :: newfrot !<
!
!     * invariant.
!
      common /row/ wetfrow  (ilg)
      common /row/ wetsrow  (ilg)
      common /row/ lghtrow  (ilg)
!
!     * time varying.
!
      common /row/ soilcrot (ilg,ntld,ictemp1)
      common /row/ litrcrot (ilg,ntld,ictemp1)
      common /row/ rootcrot (ilg,ntld,ictem)
      common /row/ stemcrot (ilg,ntld,ictem)
      common /row/ gleafcrot(ilg,ntld,ictem)
      common /row/ bleafcrot(ilg,ntld,ictem)
      common /row/ fallhrot (ilg,ntld,ictem)
      common /row/ posphrot (ilg,ntld,ictem)
      common /row/ leafsrot (ilg,ntld,ictem)
      common /row/ growtrot (ilg,ntld,ictem)
      common /row/ lastrrot (ilg,ntld,ictem)
      common /row/ lastsrot (ilg,ntld,ictem)
      common /row/ thisylrot(ilg,ntld,ictem)
      common /row/ stemhrot (ilg,ntld,ictem)
      common /row/ roothrot (ilg,ntld,ictem)
      common /row/ tempcrot (ilg,ntld,2)
      common /row/ ailcbrot (ilg,ntld,ictem)
      common /row/ bmasvrot (ilg,ntld,ictem)
      common /row/ veghrot  (ilg,ntld,ictem)
      common /row/ rootdrot (ilg,ntld,ictem)
!
      common /row/ prefrot  (ilg,ntld,ictem)
      common /row/ newfrot  (ilg,ntld,ictem)
!
      common /row/ cvegrot  (ilg,ntld)
      common /row/ cdebrot  (ilg,ntld)
      common /row/ chumrot  (ilg,ntld)
      common /row/ fcolrot  (ilg,ntld)
!
!     * ctem diagnostic output fields.
!
      common /row/ cvegrow(ilg)
      common /row/ cdebrow(ilg)
      common /row/ chumrow(ilg)
      common /row/ clairow(ilg)
      common /row/ cfnprow(ilg)
      common /row/ cfnerow(ilg)
      common /row/ cfrvrow(ilg)
      common /row/ cfgprow(ilg)
      common /row/ cfnbrow(ilg)
      common /row/ cffvrow(ilg)
      common /row/ cffdrow(ilg)
      common /row/ cflvrow(ilg)
      common /row/ cfldrow(ilg)
      common /row/ cflhrow(ilg)
      common /row/ cbrnrow(ilg)
      common /row/ cfrhrow(ilg)
      common /row/ cfhtrow(ilg)
      common /row/ cflfrow(ilg)
      common /row/ cfrdrow(ilg)
      common /row/ cfrgrow(ilg)
      common /row/ cfrmrow(ilg)
      common /row/ cvglrow(ilg)
      common /row/ cvgsrow(ilg)
      common /row/ cvgrrow(ilg)
      common /row/ cfnlrow(ilg)
      common /row/ cfnsrow(ilg)
      common /row/ cfnrrow(ilg)
      common /row/ ch4hrow(ilg)
      common /row/ ch4nrow(ilg)
      common /row/ wfrarow(ilg)
      common /row/ cw1drow(ilg)
      common /row/ cw2drow(ilg)
      common /row/ fcolrow(ilg)
      common /row/ curfrow(ilg,ictem)

!     additional ctem related cmip6 output

      common /row/ brfrrow(ilg)   ! baresoilfrac
      common /row/ c3crrow(ilg)   ! cropfracc3
      common /row/ c4crrow(ilg)   ! cropfracc4
      common /row/ crpfrow(ilg)   ! cropfrac
      common /row/ c3grrow(ilg)   ! grassfracc3
      common /row/ c4grrow(ilg)   ! grassfracc4
      common /row/ grsfrow(ilg)   ! grassfrac
      common /row/ bdtfrow(ilg)   ! treefracbdldcd
      common /row/ betfrow(ilg)   ! treefracbdlevg
      common /row/ ndtfrow(ilg)   ! treefracndldcd
      common /row/ netfrow(ilg)   ! treefracndlevg
      common /row/ treerow(ilg)   ! treefrac
      common /row/ vegfrow(ilg)   ! vegfrac
      common /row/ c3pfrow(ilg)   ! c3pftfrac
      common /row/ c4pfrow(ilg)   ! c4pftfrac
      common /row/ clndrow(ilg)   ! cland

!
! instantaneous output (used for subdaily output)
!
      common /row/ cfgprol (ilg) ! gpp
      common /row/ cfrvrol (ilg) ! ra
      common /row/ cfrhrol (ilg) ! rh
      common /row/ cfrdrol (ilg) ! rh

#endif
!
#if defined explvol
      common /row/ vtaurow(ilg)
      common /row/ vtaurol(ilg)
      common /row/ sw_ext_sa_row(ilg,levsa,nbs)
      common /row/ sw_ext_sa_rol(ilg,levsa,nbs)
      common /row/ sw_ssa_sa_row(ilg,levsa,nbs)
      common /row/ sw_ssa_sa_rol(ilg,levsa,nbs)
      common /row/ sw_g_sa_row(ilg,levsa,nbs)
      common /row/ sw_g_sa_rol(ilg,levsa,nbs)
      common /row/ lw_ext_sa_row(ilg,levsa,nbl)
      common /row/ lw_ext_sa_rol(ilg,levsa,nbl)
      common /row/ lw_ssa_sa_row(ilg,levsa,nbl)
      common /row/ lw_ssa_sa_rol(ilg,levsa,nbl)
      common /row/ lw_g_sa_row(ilg,levsa,nbl)
      common /row/ lw_g_sa_rol(ilg,levsa,nbl)
      common /row/ w055_ext_sa_row(ilg,levsa)
      common /row/ w055_ext_sa_rol(ilg,levsa)
      common /row/ w055_vtau_sa_row(ilg)
      common /row/ w055_vtau_sa_rol(ilg)
      common /row/ w055_ext_gcm_sa_row(ilg,ilev)
      common /row/ w055_ext_gcm_sa_rol(ilg,ilev)
      common /row/ w110_ext_sa_row(ilg,levsa)
      common /row/ w110_ext_sa_rol(ilg,levsa)
      common /row/ w110_vtau_sa_row(ilg)
      common /row/ w110_vtau_sa_rol(ilg)
      common /row/ w110_ext_gcm_sa_row(ilg,ilev)
      common /row/ w110_ext_gcm_sa_rol(ilg,ilev)
      common /row/ pressure_sa_row(ilg,levsa)
      common /row/ pressure_sa_rol(ilg,levsa)
      common /row/ troprow(ilg)
      common /row/ troprol(ilg)
#endif
common /row/ spotrow(ilg)
common /row/ st01row(ilg)
common /row/ st02row(ilg)
common /row/ st03row(ilg)
common /row/ st04row(ilg)
common /row/ st06row(ilg)
common /row/ st13row(ilg)
common /row/ st14row(ilg)
common /row/ st15row(ilg)
common /row/ st16row(ilg)
common /row/ st17row(ilg)
common /row/ suz0row(ilg)
common /row/ suz0rol(ilg)
common /row/ pdsfrow(ilg)
common /row/ pdsfrol(ilg)

#ifdef rad_flux_profs
! note that for vertical profiles
! level 1 is the top of atmosphere
! level 2 is the top of the model
! level ilev+2 is the surface
  real :: fsaurow
  real :: fsadrow
  real :: flaurow
  real :: fladrow
  real :: fscurow
  real :: fscdrow
  real :: flcurow
  real :: flcdrow

      common /row/ fsaurow (ilg,ilev+2) ! upward all-sky shortwave flux profile
      common /row/ fsadrow (ilg,ilev+2) ! downward all-sky shortwave flux profile
      common /row/ flaurow (ilg,ilev+2) ! upward all-sky longwave flux profile
      common /row/ fladrow (ilg,ilev+2) ! downward all-sky longwave flux profile
      common /row/ fscurow (ilg,ilev+2) ! upward clear-sky shortwave flux profile
      common /row/ fscdrow (ilg,ilev+2) ! downward clear-sky shortwave flux profile
      common /row/ flcurow (ilg,ilev+2) ! upward clear-sky longwave flux profile
      common /row/ flcdrow (ilg,ilev+2) ! downward clear-sky longwave flux profile
#endif
common /row/ fsaurol (ilg,ilev+2) ! upward all-sky shortwave flux profile
common /row/ fsadrol (ilg,ilev+2) ! downward all-sky shortwave flux profile
common /row/ flaurol (ilg,ilev+2) ! upward all-sky longwave flux profile
common /row/ fladrol (ilg,ilev+2) ! downward all-sky longwave flux profile
common /row/ fscurol (ilg,ilev+2) ! upward clear-sky shortwave flux profile
common /row/ fscdrol (ilg,ilev+2) ! downward clear-sky shortwave flux profile
common /row/ flcurol (ilg,ilev+2) ! upward clear-sky longwave flux profile
common /row/ flcdrol (ilg,ilev+2) ! downward clear-sky longwave flux profile

#if defined radforce

      common /row/ fsaurow_r (ilg,ilev+2,nrfp) ! upward all-sky shortwave flux profile
      common /row/ fsadrow_r (ilg,ilev+2,nrfp) ! downward all-sky shortwave flux profile
      common /row/ flaurow_r (ilg,ilev+2,nrfp) ! upward all-sky longwave flux profile
      common /row/ fladrow_r (ilg,ilev+2,nrfp) ! downward all-sky longwave flux profile
      common /row/ fscurow_r (ilg,ilev+2,nrfp) ! upward clear-sky shortwave flux profile
      common /row/ fscdrow_r (ilg,ilev+2,nrfp) ! downward clear-sky shortwave flux profile
      common /row/ flcurow_r (ilg,ilev+2,nrfp) ! upward clear-sky longwave flux profile
      common /row/ flcdrow_r (ilg,ilev+2,nrfp) ! downward clear-sky longwave flux profile
      common /row/ fsaurol_r (ilg,ilev+2,nrfp) ! upward all-sky shortwave flux profile
      common /row/ fsadrol_r (ilg,ilev+2,nrfp) ! downward all-sky shortwave flux profile
      common /row/ flaurol_r (ilg,ilev+2,nrfp) ! upward all-sky longwave flux profile
      common /row/ fladrol_r (ilg,ilev+2,nrfp) ! downward all-sky longwave flux profile
      common /row/ fscurol_r (ilg,ilev+2,nrfp) ! upward clear-sky shortwave flux profile
      common /row/ fscdrol_r (ilg,ilev+2,nrfp) ! downward clear-sky shortwave flux profile
      common /row/ flcurol_r (ilg,ilev+2,nrfp) ! upward clear-sky longwave flux profile
      common /row/ flcdrol_r (ilg,ilev+2,nrfp) ! downward clear-sky longwave flux profile
!
      common /row/ rdtrow_r  (ilg,ilev,nrfp)
      common /row/ rdtrol_r  (ilg,ilev,nrfp)
      common /row/ rdtrom_r  (ilg,ilev,nrfp)
      common /row/ hrsrow_r  (ilg,ilev,nrfp)
      common /row/ hrlrow_r  (ilg,ilev,nrfp)
      common /row/ hrscrow_r (ilg,ilev,nrfp)
      common /row/ hrlcrow_r (ilg,ilev,nrfp)

      common /row/ fsgrol_r  (ilg,nrfp)
      common /row/ csbrol_r  (ilg,nrfp)
      common /row/ fssrol_r  (ilg,nrfp)
      common /row/ fsscrol_r (ilg,nrfp)
      common /row/ clbrol_r  (ilg,nrfp)
      common /row/ flgrol_r  (ilg,nrfp)
      common /row/ fdlrol_r  (ilg,nrfp)
      common /row/ fdlcrol_r (ilg,nrfp)
      common /row/ fsrrol_r  (ilg,nrfp)
      common /row/ fsrcrol_r (ilg,nrfp)
      common /row/ fsorol_r  (ilg,nrfp)
      common /row/ olrrol_r  (ilg,nrfp)
      common /row/ olrcrol_r (ilg,nrfp)
      common /row/ fslorol_r (ilg,nrfp)

      common /row/ csbrot_r  (ilg,im,nrfp)
      common /row/ clbrot_r  (ilg,im,nrfp)
      common /row/ fsgrot_r  (ilg,im,nrfp)
      common /row/ flgrot_r  (ilg,im,nrfp)
      common /row/ kthrol    (ilg)
#endif
#if defined tprhs
      common /row/ ttprow (ilg,ilev)
#endif
#if defined qprhs
      common /row/ qtprow (ilg,ilev)
#endif
#if defined uprhs
      common /row/ utprow (ilg,ilev)
#endif
#if defined vprhs
      common /row/ vtprow (ilg,ilev)
#endif
#if defined qconsav
      common /row/ qaddrow(ilg,2)
      common /row/ qtphrow(ilg,2)
      common /row/ qtpfrow(ilg,ilev)
#endif
#if defined tprhsc
      common /row/ ttpcrow(ilg,ilev)
      common /row/ ttpkrow(ilg,ilev)
      common /row/ ttpmrow(ilg,ilev)
      common /row/ ttpnrow(ilg,ilev)
      common /row/ ttpprow(ilg,ilev)
      common /row/ ttpvrow(ilg,ilev)
#endif
#if (defined (tprhsc) || defined (radforce))
      common /row/ ttplrow(ilg,ilev)
      common /row/ ttpsrow(ilg,ilev)
      common /row/ ttscrow(ilg,ilev)
      common /row/ ttlcrow(ilg,ilev)
#endif
#if defined qprhsc
      common /row/ qtpcrow(ilg,ilev)
      common /row/ qtpmrow(ilg,ilev)
      common /row/ qtpprow(ilg,ilev)
      common /row/ qtpvrow(ilg,ilev)
#endif
#if defined uprhsc
      common /row/ utpcrow(ilg,ilev)
      common /row/ utpgrow(ilg,ilev)
      common /row/ utpnrow(ilg,ilev)
      common /row/ utpsrow(ilg,ilev)
      common /row/ utpvrow(ilg,ilev)
#endif
#if defined vprhsc
      common /row/ vtpcrow(ilg,ilev)
      common /row/ vtpgrow(ilg,ilev)
      common /row/ vtpnrow(ilg,ilev)
      common /row/ vtpsrow(ilg,ilev)
      common /row/ vtpvrow(ilg,ilev)
#endif
#if defined xconsav
      common /row/ xaddrow(ilg,2,ntrac)
      common /row/ xtphrow(ilg,2,ntrac)
      common /row/ xtpfrow(ilg,ilev,ntrac)
#endif
#if defined xprhs
      common /row/ xtprow (ilg,ilev,ntrac)
#endif
#if defined xprhsc
      common /row/ xtpcrow(ilg,ilev,ntrac)
      common /row/ xtpmrow(ilg,ilev,ntrac)
      common /row/ xtpprow(ilg,ilev,ntrac)
      common /row/ xtpvrow(ilg,ilev,ntrac)
#endif
#if defined xtraconv
      common /row/ bcdrow (ilg)
      common /row/ bcsrow (ilg)
      common /row/ caperow(ilg)
      common /row/ cdcbrow(ilg)
      common /row/ cinhrow(ilg)
      common /row/ cscbrow(ilg)
      common /row/ dmcrow (ilg,ilev)
      common /row/ dmcdrow(ilg,ilev)
      common /row/ dmcurow(ilg,ilev)
      common /row/ smcrow (ilg,ilev)
      common /row/ tcdrow (ilg)
      common /row/ tcsrow (ilg)
#endif
#if defined xtrachem
!
      common /row/ dd4row (ilg)
      common /row/ dox4row(ilg)
      common /row/ doxdrow(ilg)
      common /row/ esdrow (ilg)
      common /row/ esfsrow(ilg)
      common /row/ eaisrow(ilg)
      common /row/ estsrow(ilg)
      common /row/ efisrow(ilg)
      common /row/ esfbrow(ilg)
      common /row/ eaibrow(ilg)
      common /row/ estbrow(ilg)
      common /row/ efibrow(ilg)
      common /row/ esforow(ilg)
      common /row/ eaiorow(ilg)
      common /row/ estorow(ilg)
      common /row/ efiorow(ilg)
      common /row/ edslrow(ilg)
      common /row/ edsorow(ilg)
      common /row/ esvcrow(ilg)
      common /row/ esverow(ilg)
      common /row/ noxdrow(ilg)
      common /row/ wdd4row(ilg)
      common /row/ wdl4row(ilg)
      common /row/ wds4row(ilg)
#ifndef pla
      common /row/ dddrow (ilg)
      common /row/ ddbrow (ilg)
      common /row/ ddorow (ilg)
      common /row/ ddsrow (ilg)
      common /row/ wdldrow(ilg)
      common /row/ wdlbrow(ilg)
      common /row/ wdlorow(ilg)
      common /row/ wdlsrow(ilg)
      common /row/ wdddrow(ilg)
      common /row/ wddbrow(ilg)
      common /row/ wddorow(ilg)
      common /row/ wddsrow(ilg)
      common /row/ wdsdrow(ilg)
      common /row/ wdsbrow(ilg)
      common /row/ wdsorow(ilg)
      common /row/ wdssrow(ilg)
      common /row/ dd6row (ilg)
      common /row/ phdrow (ilg,ilev)
      common /row/ phlrow (ilg,ilev)
      common /row/ phsrow (ilg,ilev)
      common /row/ sdhprow(ilg)
      common /row/ sdo3row(ilg)
      common /row/ slhprow(ilg)
      common /row/ slo3row(ilg)
      common /row/ sshprow(ilg)
      common /row/ sso3row(ilg)
      common /row/ wdd6row(ilg)
      common /row/ wdl6row(ilg)
      common /row/ wds6row(ilg)
#endif
#endif
!
! pla and pam variables
!
#if (defined(pla) && defined(pam))
  real :: ssldrow
  real :: ressrow
  real :: vessrow
  real :: dsldrow
  real :: redsrow
  real :: vedsrow
  real :: bcldrow
  real :: rebcrow
  real :: vebcrow
  real :: ocldrow
  real :: reocrow
  real :: veocrow
  real :: amldrow
  real :: reamrow
  real :: veamrow
  real :: fr1row
  real :: fr2row
  real :: zcdnrow
  real :: bcicrow
  real :: oednrow
  real :: oercrow
  real :: oidnrow
  real :: oircrow
  real :: svvbrow
  real :: psvvrow
  real :: svmbrow
  real :: svcbrow
  real :: pnvbrow
  real :: pnmbrow
  real :: pncbrow
  real :: psvbrow
  real :: psmbrow
  real :: pscbrow
  real :: qnvbrow
  real :: qnmbrow
  real :: qncbrow
  real :: qsvbrow
  real :: qsmbrow
  real :: qscbrow
  real :: qgvbrow
  real :: qgmbrow
  real :: qgcbrow
  real :: qdvbrow
  real :: qdmbrow
  real :: qdcbrow
  real :: onvbrow
  real :: onmbrow
  real :: oncbrow
  real :: osvbrow
  real :: osmbrow
  real :: oscbrow
  real :: ogvbrow
  real :: ogmbrow
  real :: ogcbrow
  real :: devbrow
  real :: pdevrow
  real :: dembrow
  real :: decbrow
  real :: divbrow
  real :: pdivrow
  real :: dimbrow
  real :: dicbrow
  real :: revbrow
  real :: prevrow
  real :: rembrow
  real :: recbrow
  real :: rivbrow
  real :: privrow
  real :: rimbrow
  real :: ricbrow
  real :: sulirow
  real :: rsuirow
  real :: vsuirow
  real :: f1surow
  real :: f2surow
  real :: bclirow
  real :: rbcirow
  real :: vbcirow
  real :: f1bcrow
  real :: f2bcrow
  real :: oclirow
  real :: rocirow
  real :: vocirow
  real :: f1ocrow
  real :: f2ocrow

  real :: sncnrow
  real :: ssunrow
  real :: scndrow
  real :: sgsprow
  real :: ccnrow
  real :: cc02row
  real :: cc04row
  real :: cc08row
  real :: cc16row
  real :: ccnerow
  real :: acasrow
  real :: acoarow
  real :: acbcrow
  real :: acssrow
  real :: acmdrow
  real :: ntrow
  real :: n20row
  real :: n50row
  real :: n100row
  real :: n200row
  real :: wtrow
  real :: w20row
  real :: w50row
  real :: w100row
  real :: w200row
  real :: rcrirow
  real :: supsrow
  real :: henrrow
  real :: o3frrow
  real :: h2o2frrow
  real :: wparrow
  real :: pm25row
  real :: pm10row
  real :: dm25row
  real :: dm10row
  real :: vncnrow
  real :: vasnrow
  real :: vscdrow
  real :: vgsprow
  real :: voaerow
  real :: vbcerow
  real :: vaserow
  real :: vmderow
  real :: vsserow
  real :: voawrow
  real :: vbcwrow
  real :: vaswrow
  real :: vmdwrow
  real :: vsswrow
  real :: voadrow
  real :: vbcdrow
  real :: vasdrow
  real :: vmddrow
  real :: vssdrow
  real :: voagrow
  real :: vbcgrow
  real :: vasgrow
  real :: vmdgrow
  real :: vssgrow
  real :: vasirow
  real :: vas1row
  real :: vas2row
  real :: vas3row
  real :: vccnrow
  real :: vcnerow

  real :: cornrow
  real :: cormrow
  real :: rsn1row
  real :: rsm1row
  real :: rsn2row
  real :: rsm2row
  real :: rsn3row
  real :: rsm3row
  real :: sdnurow
  real :: sdmarow
  real :: sdacrow
  real :: sdcorow
  real :: sssnrow
  real :: smdnrow
  real :: sianrow
  real :: sssmrow
  real :: smdmrow
  real :: sewmrow
  real :: ssumrow
  real :: socmrow
  real :: sbcmrow
  real :: siwmrow
  real :: sdvlrow
  real :: vrn1row
  real :: vrm1row
  real :: vrn2row
  real :: vrm2row
  real :: vrn3row
  real :: vrm3row
  real :: defarow
  real :: defcrow
  real :: dmacrow
  real :: dmcorow
  real :: dnacrow
  real :: dncorow
  real :: defxrow
  real :: defnrow
  real :: tnssrow
  real :: tnmdrow
  real :: tniarow
  real :: tmssrow
  real :: tmmdrow
  real :: tmocrow
  real :: tmbcrow
  real :: tmsprow
  real :: snssrow
  real :: snmdrow
  real :: sniarow
  real :: smssrow
  real :: smmdrow
  real :: smocrow
  real :: smbcrow
  real :: smsprow
  real :: siwhrow
  real :: sewhrow

      common /row/ ssldrow (ilg,ilev)
      common /row/ ressrow (ilg,ilev)
      common /row/ vessrow (ilg,ilev)
      common /row/ dsldrow (ilg,ilev)
      common /row/ redsrow (ilg,ilev)
      common /row/ vedsrow (ilg,ilev)
      common /row/ bcldrow (ilg,ilev)
      common /row/ rebcrow (ilg,ilev)
      common /row/ vebcrow (ilg,ilev)
      common /row/ ocldrow (ilg,ilev)
      common /row/ reocrow (ilg,ilev)
      common /row/ veocrow (ilg,ilev)
      common /row/ amldrow (ilg,ilev)
      common /row/ reamrow (ilg,ilev)
      common /row/ veamrow (ilg,ilev)
      common /row/ fr1row  (ilg,ilev)
      common /row/ fr2row  (ilg,ilev)
      common /row/ zcdnrow (ilg,ilev)
      common /row/ bcicrow (ilg,ilev)
      common /row/ oednrow (ilg,ilev,kextt)
      common /row/ oercrow (ilg,ilev,kextt)
      common /row/ oidnrow (ilg,ilev)
      common /row/ oircrow (ilg,ilev)
      common /row/ svvbrow (ilg,ilev)
      common /row/ psvvrow (ilg,ilev)
      common /row/ svmbrow (ilg,ilev,nrmfld)
      common /row/ svcbrow (ilg,ilev,nrmfld)
      common /row/ pnvbrow (ilg,ilev,isaintt)
      common /row/ pnmbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ pncbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ psvbrow (ilg,ilev,isaintt,kintt)
      common /row/ psmbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ pscbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ qnvbrow (ilg,ilev,isaintt)
      common /row/ qnmbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ qncbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ qsvbrow (ilg,ilev,isaintt,kintt)
      common /row/ qsmbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ qscbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ qgvbrow (ilg,ilev)
      common /row/ qgmbrow (ilg,ilev,nrmfld)
      common /row/ qgcbrow (ilg,ilev,nrmfld)
      common /row/ qdvbrow (ilg,ilev)
      common /row/ qdmbrow (ilg,ilev,nrmfld)
      common /row/ qdcbrow (ilg,ilev,nrmfld)
      common /row/ onvbrow (ilg,ilev,isaintt)
      common /row/ onmbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ oncbrow (ilg,ilev,isaintt,nrmfld)
      common /row/ osvbrow (ilg,ilev,isaintt,kintt)
      common /row/ osmbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ oscbrow (ilg,ilev,isaintt,kintt,nrmfld)
      common /row/ ogvbrow (ilg,ilev)
      common /row/ ogmbrow (ilg,ilev,nrmfld)
      common /row/ ogcbrow (ilg,ilev,nrmfld)
      common /row/ devbrow (ilg,ilev,kextt)
      common /row/ pdevrow (ilg,ilev,kextt)
      common /row/ dembrow (ilg,ilev,kextt,nrmfld)
      common /row/ decbrow (ilg,ilev,kextt,nrmfld)
      common /row/ divbrow (ilg,ilev)
      common /row/ pdivrow (ilg,ilev)
      common /row/ dimbrow (ilg,ilev,nrmfld)
      common /row/ dicbrow (ilg,ilev,nrmfld)
      common /row/ revbrow (ilg,ilev,kextt)
      common /row/ prevrow (ilg,ilev,kextt)
      common /row/ rembrow (ilg,ilev,kextt,nrmfld)
      common /row/ recbrow (ilg,ilev,kextt,nrmfld)
      common /row/ rivbrow (ilg,ilev)
      common /row/ privrow (ilg,ilev)
      common /row/ rimbrow (ilg,ilev,nrmfld)
      common /row/ ricbrow (ilg,ilev,nrmfld)
      common /row/ sulirow (ilg,ilev)
      common /row/ rsuirow (ilg,ilev)
      common /row/ vsuirow (ilg,ilev)
      common /row/ f1surow (ilg,ilev)
      common /row/ f2surow (ilg,ilev)
      common /row/ bclirow (ilg,ilev)
      common /row/ rbcirow (ilg,ilev)
      common /row/ vbcirow (ilg,ilev)
      common /row/ f1bcrow (ilg,ilev)
      common /row/ f2bcrow (ilg,ilev)
      common /row/ oclirow (ilg,ilev)
      common /row/ rocirow (ilg,ilev)
      common /row/ vocirow (ilg,ilev)
      common /row/ f1ocrow (ilg,ilev)
      common /row/ f2ocrow (ilg,ilev)

      common /row/ sncnrow (ilg,ilev)
      common /row/ ssunrow (ilg,ilev)
      common /row/ scndrow (ilg,ilev)
      common /row/ sgsprow (ilg,ilev)
      common /row/ ccnrow  (ilg,ilev)
      common /row/ cc02row (ilg,ilev)
      common /row/ cc04row (ilg,ilev)
      common /row/ cc08row (ilg,ilev)
      common /row/ cc16row (ilg,ilev)
      common /row/ ccnerow (ilg,ilev)
      common /row/ acasrow (ilg,ilev)
      common /row/ acoarow (ilg,ilev)
      common /row/ acbcrow (ilg,ilev)
      common /row/ acssrow (ilg,ilev)
      common /row/ acmdrow (ilg,ilev)
      common /row/ ntrow   (ilg,ilev)
      common /row/ n20row  (ilg,ilev)
      common /row/ n50row  (ilg,ilev)
      common /row/ n100row (ilg,ilev)
      common /row/ n200row (ilg,ilev)
      common /row/ wtrow   (ilg,ilev)
      common /row/ w20row  (ilg,ilev)
      common /row/ w50row  (ilg,ilev)
      common /row/ w100row (ilg,ilev)
      common /row/ w200row (ilg,ilev)
      common /row/ rcrirow (ilg,ilev)
      common /row/ supsrow (ilg,ilev)
      common /row/ henrrow (ilg,ilev)
      common /row/ o3frrow (ilg,ilev)
      common /row/ h2o2frrow(ilg,ilev)
      common /row/ wparrow (ilg,ilev)
      common /row/ pm25row (ilg,ilev)
      common /row/ pm10row (ilg,ilev)
      common /row/ dm25row (ilg,ilev)
      common /row/ dm10row (ilg,ilev)
      common /row/ vncnrow (ilg)
      common /row/ vasnrow (ilg)
      common /row/ vscdrow (ilg)
      common /row/ vgsprow (ilg)
      common /row/ voaerow (ilg)
      common /row/ vbcerow (ilg)
      common /row/ vaserow (ilg)
      common /row/ vmderow (ilg)
      common /row/ vsserow (ilg)
      common /row/ voawrow (ilg)
      common /row/ vbcwrow (ilg)
      common /row/ vaswrow (ilg)
      common /row/ vmdwrow (ilg)
      common /row/ vsswrow (ilg)
      common /row/ voadrow (ilg)
      common /row/ vbcdrow (ilg)
      common /row/ vasdrow (ilg)
      common /row/ vmddrow (ilg)
      common /row/ vssdrow (ilg)
      common /row/ voagrow (ilg)
      common /row/ vbcgrow (ilg)
      common /row/ vasgrow (ilg)
      common /row/ vmdgrow (ilg)
      common /row/ vssgrow (ilg)
      common /row/ vasirow (ilg)
      common /row/ vas1row (ilg)
      common /row/ vas2row (ilg)
      common /row/ vas3row (ilg)
      common /row/ vccnrow (ilg)
      common /row/ vcnerow (ilg)

      common /row/ cornrow (ilg,ilev)
      common /row/ cormrow (ilg,ilev)
      common /row/ rsn1row (ilg,ilev)
      common /row/ rsm1row (ilg,ilev)
      common /row/ rsn2row (ilg,ilev)
      common /row/ rsm2row (ilg,ilev)
      common /row/ rsn3row (ilg,ilev)
      common /row/ rsm3row (ilg,ilev)
      common /row/ sdnurow (ilg,ilev,isdnum)
      common /row/ sdmarow (ilg,ilev,isdnum)
      common /row/ sdacrow (ilg,ilev,isdnum)
      common /row/ sdcorow (ilg,ilev,isdnum)
      common /row/ sssnrow (ilg,ilev,isdnum)
      common /row/ smdnrow (ilg,ilev,isdnum)
      common /row/ sianrow (ilg,ilev,isdnum)
      common /row/ sssmrow (ilg,ilev,isdnum)
      common /row/ smdmrow (ilg,ilev,isdnum)
      common /row/ sewmrow (ilg,ilev,isdnum)
      common /row/ ssumrow (ilg,ilev,isdnum)
      common /row/ socmrow (ilg,ilev,isdnum)
      common /row/ sbcmrow (ilg,ilev,isdnum)
      common /row/ siwmrow (ilg,ilev,isdnum)
      common /row/ sdvlrow (ilg,isdiag)
      common /row/ vrn1row (ilg)
      common /row/ vrm1row (ilg)
      common /row/ vrn2row (ilg)
      common /row/ vrm2row (ilg)
      common /row/ vrn3row (ilg)
      common /row/ vrm3row (ilg)
      common /row/ defarow(ilg)
      common /row/ defcrow(ilg)
      common /row/ dmacrow(ilg,isdust)
      common /row/ dmcorow(ilg,isdust)
      common /row/ dnacrow(ilg,isdust)
      common /row/ dncorow(ilg,isdust)
      common /row/ defxrow(ilg,isdiag)
      common /row/ defnrow(ilg,isdiag)
      common /row/ tnssrow(ilg)           ! total number for sea salt
      common /row/ tnmdrow(ilg)           ! total number for mineral dust
      common /row/ tniarow(ilg)           ! total number for internally mixed aerosol
      common /row/ tmssrow(ilg)           ! total number for sea salt
      common /row/ tmmdrow(ilg)           ! total mass for mineral dust
      common /row/ tmocrow(ilg)           ! total mass for organic carbon
      common /row/ tmbcrow(ilg)           ! total mass for black carbon
      common /row/ tmsprow(ilg)           ! total mass for annonia sulphate
      common /row/ snssrow(ilg,isdnum)    ! number size distribution for sea salt
      common /row/ snmdrow(ilg,isdnum)    ! number size distribution for mineral
      common /row/ sniarow(ilg,isdnum)    ! number size distribution for internally mixed aerosol
      common /row/ smssrow(ilg,isdnum)    ! mass size distribution for sea salt
      common /row/ smmdrow(ilg,isdnum)    ! mass size distribution for mineral dust
      common /row/ smocrow(ilg,isdnum)    ! mass size distribution for organic carbon
      common /row/ smbcrow(ilg,isdnum)    ! mass size distribution for black carbon
      common /row/ smsprow(ilg,isdnum)    ! mass size distribution for annonia sulphate
      common /row/ siwhrow(ilg,isdnum)    ! water for internally mixed aerosols
      common /row/ sewhrow(ilg,isdnum)    ! water for externally mixed aerosols

#if defined (xtrapla1)
  real :: voacrow
  real :: vbccrow
  real :: vascrow
  real :: vmdcrow
  real :: vsscrow

      common /row/ voacrow (ilg)
      common /row/ vbccrow (ilg)
      common /row/ vascrow (ilg)
      common /row/ vmdcrow (ilg)
      common /row/ vsscrow (ilg)
#endif

#endif
#if defined xtrals
  real :: aggrow
  real :: autrow
  real :: cndrow
  real :: deprow
  real :: evprow
  real :: frhrow
  real :: frkrow
  real :: frsrow
  real :: mltirow
  real :: mltsrow
  real :: raclrow
  real :: rainrow
  real :: sacirow
  real :: saclrow
  real :: snowrow
  real :: subrow
  real :: sedirow
  real :: rliqrow
  real :: ricerow
  real :: rlncrow
  real :: cliqrow
  real :: cicerow
  real :: rliqrol
  real :: ricerol
  real :: rlncrol
  real :: cliqrol
  real :: cicerol
!
  real :: vaggrow
  real :: vautrow
  real :: vcndrow
  real :: vdeprow
  real :: vevprow
  real :: vfrhrow
  real :: vfrkrow
  real :: vfrsrow
  real :: vmlirow
  real :: vmlsrow
  real :: vrclrow
  real :: vscirow
  real :: vsclrow
  real :: vsubrow
  real :: vsedirow
  real :: reliqrow
  real :: reicerow
  real :: cldliqrow
  real :: cldicerow
  real :: ctlncrow
  real :: cllncrow
  real :: reliqrol
  real :: reicerol
  real :: cldliqrol
  real :: cldicerol
  real :: ctlncrol
  real :: cllncrol

      common /row/ aggrow (ilg,ilev)
      common /row/ autrow (ilg,ilev)
      common /row/ cndrow (ilg,ilev)
      common /row/ deprow (ilg,ilev)
      common /row/ evprow (ilg,ilev)
      common /row/ frhrow (ilg,ilev)
      common /row/ frkrow (ilg,ilev)
      common /row/ frsrow (ilg,ilev)
      common /row/ mltirow(ilg,ilev)
      common /row/ mltsrow(ilg,ilev)
      common /row/ raclrow(ilg,ilev)
      common /row/ rainrow(ilg,ilev)
      common /row/ sacirow(ilg,ilev)
      common /row/ saclrow(ilg,ilev)
      common /row/ snowrow(ilg,ilev)
      common /row/ subrow (ilg,ilev)
      common /row/ sedirow (ilg,ilev)
      common /row/ rliqrow(ilg,ilev)
      common /row/ ricerow(ilg,ilev)
      common /row/ rlncrow(ilg,ilev)
      common /row/ cliqrow(ilg,ilev)
      common /row/ cicerow(ilg,ilev)
      common /row/ rliqrol(ilg,ilev)
      common /row/ ricerol(ilg,ilev)
      common /row/ rlncrol(ilg,ilev)
      common /row/ cliqrol(ilg,ilev)
      common /row/ cicerol(ilg,ilev)
!
      common /row/ vaggrow(ilg)
      common /row/ vautrow(ilg)
      common /row/ vcndrow(ilg)
      common /row/ vdeprow(ilg)
      common /row/ vevprow(ilg)
      common /row/ vfrhrow(ilg)
      common /row/ vfrkrow(ilg)
      common /row/ vfrsrow(ilg)
      common /row/ vmlirow(ilg)
      common /row/ vmlsrow(ilg)
      common /row/ vrclrow(ilg)
      common /row/ vscirow(ilg)
      common /row/ vsclrow(ilg)
      common /row/ vsubrow(ilg)
      common /row/ vsedirow(ilg)
      common /row/ reliqrow(ilg)
      common /row/ reicerow(ilg)
      common /row/ cldliqrow(ilg)
      common /row/ cldicerow(ilg)
      common /row/ ctlncrow(ilg)
      common /row/ cllncrow(ilg)
      common /row/ reliqrol(ilg)
      common /row/ reicerol(ilg)
      common /row/ cldliqrol(ilg)
      common /row/ cldicerol(ilg)
      common /row/ ctlncrol(ilg)
      common /row/ cllncrol(ilg)

#endif
#if defined (aodpth)
!
      common /row/ exb1row(ilg)
      common /row/ exb2row(ilg)
      common /row/ exb3row(ilg)
      common /row/ exb4row(ilg)
      common /row/ exb5row(ilg)
      common /row/ exbtrow(ilg)
      common /row/ odb1row(ilg)
      common /row/ odb2row(ilg)
      common /row/ odb3row(ilg)
      common /row/ odb4row(ilg)
      common /row/ odb5row(ilg)
      common /row/ odbtrow(ilg)
      common /row/ odbvrow(ilg)
      common /row/ ofb1row(ilg)
      common /row/ ofb2row(ilg)
      common /row/ ofb3row(ilg)
      common /row/ ofb4row(ilg)
      common /row/ ofb5row(ilg)
      common /row/ ofbtrow(ilg)
      common /row/ abb1row(ilg)
      common /row/ abb2row(ilg)
      common /row/ abb3row(ilg)
      common /row/ abb4row(ilg)
      common /row/ abb5row(ilg)
      common /row/ abbtrow(ilg)
!
      common /row/ exb1rol(ilg)
      common /row/ exb2rol(ilg)
      common /row/ exb3rol(ilg)
      common /row/ exb4rol(ilg)
      common /row/ exb5rol(ilg)
      common /row/ exbtrol(ilg)
      common /row/ odb1rol(ilg)
      common /row/ odb2rol(ilg)
      common /row/ odb3rol(ilg)
      common /row/ odb4rol(ilg)
      common /row/ odb5rol(ilg)
      common /row/ odbtrol(ilg)
      common /row/ odbvrol(ilg)
      common /row/ ofb1rol(ilg)
      common /row/ ofb2rol(ilg)
      common /row/ ofb3rol(ilg)
      common /row/ ofb4rol(ilg)
      common /row/ ofb5rol(ilg)
      common /row/ ofbtrol(ilg)
      common /row/ abb1rol(ilg)
      common /row/ abb2rol(ilg)
      common /row/ abb3rol(ilg)
      common /row/ abb4rol(ilg)
      common /row/ abb5rol(ilg)
      common /row/ abbtrol(ilg)
!
      common /row/ exs1row(ilg)
      common /row/ exs2row(ilg)
      common /row/ exs3row(ilg)
      common /row/ exs4row(ilg)
      common /row/ exs5row(ilg)
      common /row/ exstrow(ilg)
      common /row/ ods1row(ilg)
      common /row/ ods2row(ilg)
      common /row/ ods3row(ilg)
      common /row/ ods4row(ilg)
      common /row/ ods5row(ilg)
      common /row/ odstrow(ilg)
      common /row/ odsvrow(ilg)
      common /row/ ofs1row(ilg)
      common /row/ ofs2row(ilg)
      common /row/ ofs3row(ilg)
      common /row/ ofs4row(ilg)
      common /row/ ofs5row(ilg)
      common /row/ ofstrow(ilg)
      common /row/ abs1row(ilg)
      common /row/ abs2row(ilg)
      common /row/ abs3row(ilg)
      common /row/ abs4row(ilg)
      common /row/ abs5row(ilg)
      common /row/ abstrow(ilg)
!
      common /row/ exs1rol(ilg)
      common /row/ exs2rol(ilg)
      common /row/ exs3rol(ilg)
      common /row/ exs4rol(ilg)
      common /row/ exs5rol(ilg)
      common /row/ exstrol(ilg)
      common /row/ ods1rol(ilg)
      common /row/ ods2rol(ilg)
      common /row/ ods3rol(ilg)
      common /row/ ods4rol(ilg)
      common /row/ ods5rol(ilg)
      common /row/ odstrol(ilg)
      common /row/ odsvrol(ilg)
      common /row/ ofs1rol(ilg)
      common /row/ ofs2rol(ilg)
      common /row/ ofs3rol(ilg)
      common /row/ ofs4rol(ilg)
      common /row/ ofs5rol(ilg)
      common /row/ ofstrol(ilg)
      common /row/ abs1rol(ilg)
      common /row/ abs2rol(ilg)
      common /row/ abs3rol(ilg)
      common /row/ abs4rol(ilg)
      common /row/ abs5rol(ilg)
      common /row/ abstrol(ilg)
!
#endif
#if defined use_cosp
! cosp input
  real :: rmixrow
  real :: smixrow
  real :: rrefrow
  real :: srefrow

      common /row/ rmixrow(ilg,ilev) ! rain mixing ratio
      common /row/ smixrow(ilg,ilev) ! snow mixing ratio
      common /row/ rrefrow(ilg,ilev) ! rain effective particle radius
      common /row/ srefrow(ilg,ilev) ! snow effective particle radius
#endif
#if defined (xtradust)
      common /row/ duwdrow(ilg)
      common /row/ dustrow(ilg)
      common /row/ duthrow(ilg)
      common /row/ usmkrow(ilg)
      common /row/ fallrow(ilg)
      common /row/ fa10row(ilg)
      common /row/ fa2row(ilg)
      common /row/ fa1row(ilg)
      common /row/ gustrow(ilg)
      common /row/ zspdrow(ilg)
      common /row/ vgfrrow(ilg)
      common /row/ smfrrow(ilg)
#endif
#if defined x01
  real :: x01row
      common /row/ x01row (ilg,ilev,ntrac)
#endif
#if defined x02
  real :: x02row
      common /row/ x02row (ilg,ilev,ntrac)
#endif
#if defined x03
  real :: x03row
      common /row/ x03row (ilg,ilev,ntrac)
#endif
#if defined x04
  real :: x04row
      common /row/ x04row (ilg,ilev,ntrac)
#endif
#if defined x05
  real :: x05row
      common /row/ x05row (ilg,ilev,ntrac)
#endif
#if defined x06
  real :: x06row
      common /row/ x06row (ilg,ilev,ntrac)
#endif
#if defined x07
  real :: x07row
      common /row/ x07row (ilg,ilev,ntrac)
#endif
#if defined x08
  real :: x08row
      common /row/ x08row (ilg,ilev,ntrac)
#endif
#if defined x09
  real :: x09row
      common /row/ x09row (ilg,ilev,ntrac)
#endif
!$omp threadprivate (/row/)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
