!>\file
!>\brief Read in the information from the configuration file.
!!
!! @author Jason Cole
!

MODULE read_config_file_mod

  implicit none
  
  type cfg_values    
    integer :: isvaod          !< Output diagnostic aerosol optical thickness, no=0, yes=1 [1]
    integer :: imam            !< Perform mam radiation calculations, no=0, yes=1 [1]
    integer :: iexplvol        !< Prescribed stratospheric aerosols in calculations, no=0, yes=1 [1]
    integer :: ivtau           !< Type of stratospheric aerosols optics, sato=1, cmip6=2 [1]
    integer :: max_sam         !< Maximum number of samples for the mcica calculations [1]
    integer :: iseedrow        !< Seed for random number generator [1] ! set single value her use for all columns
    integer :: il1             !< Start column for calculations [1]
    integer :: il2             !< End column for calculations [1]
    integer :: ilg             !< Total number of columns [1]
    integer :: nlat            !< Number of latitudes [1]
    integer :: ilev            !< Number of vertical layers [1]
    integer :: lev             !< Number of vertical levels (layer interfaces) [1]
    integer :: nxloc           !< Number of subcolumns for mcica [1]
    integer :: im              !< Number of tiles [1]
    integer :: ivers           !< Sampling method for mcica, 1=basic, 2=cloudy, 3=spectral [1]
    integer :: mcica           !< Indicate using mcica method (1) or deterministic method (0) [1]
    integer :: iradforce       !< Is the calculation a radiative forcing calculation [1] ! never is for offline
    integer :: irad_flux_profs !< Output the radiative flux profiles (1) or not (0) [1]
    integer :: iactive_rt      !< Is call to raddriv for active (1) or diagnostic (0) calculation [1] ! always active in offline mode
    integer :: itilerad        !< Is the surface tiled (1) or not (0) [1]
    integer :: ipam            !< Using bulk aerosols (0) or pam aerosols (1) [1]
    integer :: idist           !< Distribution to use for pdf of horizontal variability of cloud condensate, beta=1, gamma=2
    integer :: ioverlap        !< Vertical overlap for clouds in radiation, cccma max-ran=0, geleyn max-ran=1, decorrelation lengths=2
    integer :: ipph            !< Switch for horizontal variability of cloud condensate, homogeneous=0, inhomogeneous=1
    integer :: ipph_re         !< Switch for homogeneous effective cloud particle size (0) or inhomogeneous (1)
    integer :: input_type      !< Type of input, rfmip=1
    integer :: output_type     !< Type of input, rfmip=1
    integer :: num_profile     !< Number of profiles
    integer :: num_experiment  !< Number of experiments to perform for each profile. default is 1 but rfmip has 18 per profile.
    
    ! These are configuration parameters that are derived from those read in
    integer :: idimr           !< Set the number of aerosols passed into radiation, depends on bulk (ipam=0) or pam (ipam=1)

    ! Control if certain things are done in the simulation
    logical :: compute_subgrid_clouds !< If we should simulate subgrid-scale clouds (.true.) or use directly fields read in (.false.)
  end type cfg_values
    
CONTAINS
  
  subroutine read_config_file(cfg_filename,    & ! Input
                              cfg_settings     ) ! Output

    ! It seems that reading a namelist into a derived data type is not robust
    ! or even possible for older fortran so I will read the namelist into a 
    ! locally defined variable and then copy it into the derived type.
    
    use parameters_mod, only : maxlen_char, &
                               exit_code_config

    implicit none
    
    ! input 
    character(len=maxlen_char), intent(in) :: cfg_filename !< Name of the configuration file
    
    ! output
    type(cfg_values), intent(out) :: cfg_settings !< Type holding all configuration settings
    
    ! local
    integer :: isvaod          !< Output diagnostic aerosol optical thickness, no=0, yes=1 [1]
    integer :: imam            !< Perform mam radiation calculations, no=0, yes=1 [1]
    integer :: iexplvol        !< Prescribed stratospheric aerosols in calculations, no=0, yes=1 [1]
    integer :: ivtau           !< Type of stratospheric aerosols optics, sato=1, cmip6=2 [1]
    integer :: max_sam         !< Maximum number of samples for the McICA calculations [1]
    integer :: iseedrow        !< Seed for random number generator [1] ! set single value here use for all columns
    integer :: il1             !< Start column for calculations [1]
    integer :: il2             !< End column for calculations [1]
    integer :: ilg             !< Total number of columns [1]
    integer :: nlat            !< Number of latitudes [1]    
    integer :: ilev            !< Number of vertical layers [1]
    integer :: lev             !< Number of vertical levels (layer interfaces) [1]
    integer :: nxloc           !< Number of subcolumns for mcica [1]
    integer :: im              !< Number of tiles [1]
    integer :: ivers           !< Sampling method for mcica, 1=clds, 2=spec, 3=ref [1]
    integer :: mcica           !< Indicate using mcica method (1) or deterministic method (0) [1]
    integer :: iradforce       !< Is the calculation a radiative forcing calculation [1] ! never is for offline
    integer :: irad_flux_profs !< Output the radiative flux profiles (1) or not (0) [1]
    integer :: iactive_rt      !< Is call to raddriv for active (1) or diagnostic (0) calculation [1] ! always active in offline mode
    integer :: itilerad        !< Is the surface tiled (1) or not (0) [1]
    integer :: ipam            !< Using bulk aerosols (0) or pam aerosols (1) [1]
    integer :: idist           !< Distribution to use for pdf of horizontal variability of cloud condensate, beta=1, gamma=2
    integer :: ioverlap        !< Vertical overlap for clouds in radiation, cccma max-ran=0, geleyn max-ran=1, decorrelation lengths=2
    integer :: ipph            !< Switch for horizontal variability of cloud condensate, homogeneous=0, inhomogeneous=1
    integer :: ipph_re         !< Switch for homogeneous effective cloud particle size (0) or inhomogeneous (1)
    integer :: input_type      !< Type of input, rfmip=1
    integer :: output_type     !< Type of input, rfmip=1
    integer :: num_profile     !< Number of profiles
    integer :: num_experiment  !< Number of experiments to perform for each profile. default is 1 but rfmip has 18 per profile.

    ! These are configuration parameters that are derived from those read in
    integer :: idimr           !< Set the number of aerosols passed into radiation, depends on bulk (ipam=0) or pam (ipam=1)

    ! Control if certain things are done in the simulation
    logical :: compute_subgrid_clouds !< If we should simulate subgrid-scale clouds (.true.) or use directly fields read in (.false.)

    integer :: iu
    logical :: ok
    
    ! Define the configuration namelist
    namelist /configuration/ isvaod,          &
                             imam,            &
                             iexplvol,        &
                             ivtau,           &
                             max_sam,         &
                             iseedrow,        &
                             il1,             &
                             il2,             &
                             ilg,             &
                             nlat,            &
                             ilev,            &
                             lev,             &
                             nxloc,           &
                             im,              &
                             ivers,           &
                             mcica,           &
                             iradforce,       &
                             irad_flux_profs, &
                             iactive_rt,      &
                             itilerad,        &
                             ipam,            &
                             idist,           &
                             ioverlap,        &
                             ipph,            &
                             ipph_re,         &
                             input_type,      &
                             output_type,     &
                             num_profile,     &
                             num_experiment,  &
                             compute_subgrid_clouds

    ! Some defaults, just in case values are not defined in the namelist
    compute_subgrid_clouds = .true.
                             
    ! Read in the configuration file namelist.
    inquire(file=trim(cfg_filename),exist=ok)
    
    if (ok) then
      iu = 16
      open(iu,file=trim(cfg_filename),form='formatted')
      read(iu,nml=configuration)
      close(iu)
    else
      write(6,*) 'Error opening configuration file ',trim(cfg_filename)
      stop exit_code_config
    endif ! ok
        
    ! Copy the information into cfg_settings

    cfg_settings%isvaod          = isvaod
    cfg_settings%imam            = imam
    cfg_settings%iexplvol        = iexplvol
    cfg_settings%ivtau           = ivtau
    cfg_settings%max_sam         = max_sam
    cfg_settings%iseedrow        = iseedrow
    cfg_settings%il1             = il1
    cfg_settings%il2             = il2
    cfg_settings%ilg             = ilg
    cfg_settings%nlat            = nlat
    cfg_settings%ilev            = ilev
    cfg_settings%lev             = lev
    cfg_settings%nxloc           = nxloc
    cfg_settings%im              = im
    cfg_settings%ivers           = ivers
    cfg_settings%mcica           = mcica
    cfg_settings%iradforce       = iradforce
    cfg_settings%irad_flux_profs = irad_flux_profs
    cfg_settings%iactive_rt      = iactive_rt
    cfg_settings%itilerad        = itilerad
    cfg_settings%ipam            = ipam
    cfg_settings%idist           = idist
    cfg_settings%ioverlap        = ioverlap
    cfg_settings%ipph            = ipph
    cfg_settings%ipph_re         = ipph_re
    cfg_settings%input_type      = input_type
    cfg_settings%output_type     = output_type
    cfg_settings%num_profile     = num_profile
    cfg_settings%num_experiment  = num_experiment
    cfg_settings%compute_subgrid_clouds = compute_subgrid_clouds
    
    ! Based on the value of ipam set idimr (follow approach in p_sizes)
    if (ipam == 0) then ! bulk
      cfg_settings%idimr = 9
    else ! pam
      cfg_settings%idimr = 17
    end if ! ipam
        
    ! Should call a routine here to check that the configuration parameters
    ! are consistent and make sense.
    
    return
    
  end subroutine read_config_file
  
  
end module read_config_file_mod

!> \file
!> Read in the information from the configuration file.  The information is 
!! provided in a namelist.
