!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine legbd2(spec,four1,four2,four3, &
                        alp,dalp,delalp, &
                        wl,wocsl,ilev,levs,nlat, &
                        itrac,ntrac, &
                        m,ntot)
  !
  !     * sep 11/06 - m. lazare. new version for gcm15f:
  !     *                        - calls new vfastx2 to support 32-bit driver.
  !     * oct 29/03 - m. lazare. previous version legbd from ibm conversion:
  !     * oct 29/03 - m. lazare. new routine to do exclusively the backward
  !     *                        legendre transform for dynamics, based on
  !     *                        old code in mhanl_ and using dgemm.
  !     *
  !     * transform fourier coefficients tendencies to spectral tendencies
  !     * for this (possibly chained) gaussian latitude.
  !     * tracer variable transforms done only if itrac/=0.
  !
  !     * this is called for a given zonal wavenumber index "M", ie inside
  !     * a loop over m.
  !     *
  !     *    alp = legendre polynomials *gaussian weight
  !     *   dalp = n-s derivative of leg.poly. *gauss.weight/sin(lat)
  !     * delalp = laplacian of leg.poly. *(-.5*gauss.weight/sin(lat))
  !     *
  !     * input fields
  !     * ------------
  !     *
  !     * this routine is called within a loop over zonal index "M" and
  !     * thus accesses the input for that particular zonal index from
  !     * the calling array.
  !     *
  !     * ttgd,estgd,tractgd,vtgd,utgd,pstgd,pressgd --> four1(2,nlev1,nlat)
  !     * tvtg,svtg,xvtg,utmp,vtmp                   --> four2(2,nlev2,nlat)
  !     * tutg,sutg,xutg,eg                          --> four3(2,nlev3,nlat)
  !     *
  !     * output fields
  !     * -------------
  !     *
  !     * the output data fields are the spectral tendencies work field,
  !     * accumulated into the following data structure:
  !     *
  !     *           spec(2,nlev1,ntot)
  !     *
  !
  implicit none
  real :: beta
  real :: bi
  real :: f1tmp
  real :: fms
  real :: fsq
  integer :: i
  integer :: ief
  integer :: iestf
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ipressf
  integer :: ipstf
  integer :: isutf
  integer :: isvtf
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer :: itractf
  integer :: ittf
  integer :: itutf
  integer :: itvtf
  integer :: iutf
  integer :: iutmp
  integer :: ivtf
  integer :: ivtmp
  integer :: ixutf
  integer :: ixvtf
  integer :: lat
  integer :: lev
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: m
  integer :: n
  integer :: nilev
  integer, intent(in) :: nlat
  integer :: nlev1
  integer :: nlev2
  integer :: nlev3
  integer, intent(in) :: ntot
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  !     * input fourier coefficients.
  !
  real, intent(inout) :: four1(2,3*ilev+levs+itrac*ntrac*ilev+2,nlat) !< Variable description\f$[units]\f$
  real, intent(inout) :: four2(2,3*ilev+levs+itrac*ntrac*ilev,  nlat) !< Variable description\f$[units]\f$
  real, intent(in) :: four3(2,2*ilev+levs+itrac*ntrac*ilev,  nlat) !< Variable description\f$[units]\f$
  !
  !     * output spectral tendencies.
  !
  real, intent(inout), target :: spec(2,3*ilev+levs+itrac*ntrac*ilev+2,ntot) !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real*8, intent(in) :: alp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) :: dalp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) :: delalp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wl !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat) :: wocsl !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * work arrays.
  !
  real, allocatable, dimension(:,:,:) :: four4 !<
  real, allocatable, dimension(:,:,:) :: spec0 !<
  real, allocatable, dimension(:,:,:) :: spec2 !<
  !-----------------------------------------------------------------------
  fms = real(m-1)
  !
  !     * calculate various sizes.
  !
  nilev = 0
  if (itrac /= 0) nilev=ilev*ntrac
  nlev1 = 3*ilev + levs + nilev + 2
  nlev2 = 3*ilev + levs + nilev
  nlev3 = 2*ilev + levs + nilev
  !
  !     * initialize pointers to variables in "FOUR" arrays.
  !     * these constants are equal to the number of levels that
  !     * must be skipped to access the data for a particular variable.
  !
  ittf      = 0
  iestf     = ittf  + ilev
  itractf   = iestf + levs
  ivtf      = itractf
  if (itrac /= 0) then
    ivtf    = ivtf  + nilev
  end if
  iutf      = ivtf  + ilev
  ipstf     = iutf  + ilev
  ipressf   = ipstf + 1

  itvtf     = 0
  isvtf     = itvtf + ilev
  ixvtf     = isvtf + levs
  iutmp     = ixvtf
  if (itrac /= 0) then
    iutmp   = iutmp + nilev
  end if
  ivtmp     = iutmp + ilev

  itutf     = 0
  isutf     = itutf + ilev
  ixutf     = isutf + levs
  ief       = ixutf
  if (itrac /= 0) then
    ief     = ief   + nilev
  end if
  !
  !     * copy the fourier * "VTF" and "UTF" fields from "FOUR1" into "FOUR2".
  !     * note that the ordering of "UTMP" and "VTMP" in "FOUR2" is reversed
  !     * relative to the ordering of "VTF" and "UTF" in "FOUR1".
  !     * this is the ordering required for calculating the cross-product
  !     * contributions to the "PT" and "CT" tendencies.
  !     * also negate the tvtg term.
  !
  do lat=1,nlat
    do i=1,2*ilev
      four2(i,iutmp+1,lat) =  four1(i,iutf +1,lat)
      four2(i,ivtmp+1,lat) =  four1(i,ivtf +1,lat)
      four2(i,itvtf+1,lat) = -four2(i,itvtf+1,lat)
    end do
  end do ! loop 100
  !
  !     * east - west derivative of fourier components.
  !     * special note:  note that "PUTF" and "PVTF" map to "VTG" and "UTG"
  !     * respectively in the call to "MHANL8".  this mapping is expressed
  !     * using the "iutf" and "ivtf" pointers below.
  !
  do lat=1,nlat
    fsq   = wocsl(lat)/wl(lat)
    bi    = fsq*fms
    do lev=1,ilev
      four1(1,ittf+lev,lat) =    four1(1,ittf +lev,lat) &
                                  - bi*four3(2,itutf+lev,lat)
      four1(2,ittf+lev,lat) =    four1(2,ittf +lev,lat) &
                                  + bi*four3(1,itutf+lev,lat)
    end do ! loop 320
    !
    do lev=ivtf+1,ivtf+ilev
      f1tmp                =      four1(1, lev, lat)
      four1(1, lev, lat)   =   bi*four1(2, lev, lat)
      four1(2, lev, lat)   =  -bi*f1tmp
    end do ! loop 324
    !
    do lev=iutf+1,iutf+ilev
      f1tmp                =      four1(1, lev, lat)
      four1(1, lev, lat)   =  -bi*four1(2, lev, lat)
      four1(2, lev, lat)   =   bi*f1tmp
    end do ! loop 326
  end do ! loop 330
  !
  if (itrac /= 0) then
    do lat=1,nlat
      fsq   = wocsl(lat)/wl(lat)
      bi    = fsq*fms
      do lev=1,itrac*ntrac*ilev
        four1(1,itractf+lev,lat) =    four1(1,itractf+lev,lat) &
                                     + bi*four3(2,ixutf  +lev,lat)
        four1(2,itractf+lev,lat) =    four1(2,itractf+lev,lat) &
                                     - bi*four3(1,ixutf  +lev,lat)
      end do ! loop 335
    end do ! loop 340
  end if
  !
  do lat=1,nlat
    fsq   = wocsl(lat)/wl(lat)
    bi    = fsq*fms
    do lev=1,levs
      four1(1,iestf+lev,lat) =    four1(1,iestf+lev,lat) &
                                   + bi*four3(2,isutf+lev,lat)
      four1(2,iestf+lev,lat) =    four1(2,iestf+lev,lat) &
                                   - bi*four3(1,isutf+lev,lat)
    end do ! loop 360
  end do ! loop 380
  !
  !     * do the direct legendre transforms.
  !     * this presumes that the grid slices and spectral arrays
  !     * are lined up in memory,  and that pstg-pst are the
  !     * first of the series.
  !     * number = ilev*3 (for putf,pvtf,peetf),
  !     *         +levs*1 (for estf),
  !     *         +  1 *2 (for pstf,pressf),
  !     *         +ilev*ntrac (for tractf, if itrac/=0)
  !
  beta=0.
  call vfastx2(four1,spec,alp, &
                   beta,nlev1,nlat,ntot)
  !
  !     * north - south derivatives using dalp.
  !
  allocate(spec0(2,nlev2,ntot))
  beta=0.
  call vfastx2(four2,spec0,dalp, &
                   beta,nlev2,nlat,ntot)
  !
  !     * accumulate contribution for spec from array "SPEC0".
  !
  do n=1,ntot
    do i=1,2*nlev2
      spec(i,1,n) = spec(i,1,n) + spec0(i,1,n)
    end do
  end do ! loop 400
  deallocate(spec0)
  !
  !     * calculate the laplacian contribution to the ct tendency.
  !     * first, copy ef (fourier transform of eg) into array "FOUR4".
  !
  allocate(four4(2,ilev,nlat))
  allocate(spec2(2,ilev,ntot))

  do lat=1,nlat
    do i=1,2*ilev
      four4(i,1,lat) = four3(i,ief+1,lat)
    end do
  end do ! loop 480
  !
  beta=0.
  call vfastx2(four4,spec2,delalp, &
                   beta,ilev,nlat,ntot)
  !
  deallocate(four4)
  !
  !     * accumulate contribution for ct from array "SPEC2".
  !     * the "IUTF" pointer corresponds to the same range of levels
  !     * as for the "CT" spectral tendency.
  !
  do n=1,ntot
    do i=1,2*ilev
      spec(i,iutf+1,n) = spec(i,iutf+1,n) + spec2(i,1,n)
    end do
  end do ! loop 500
  deallocate(spec2)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
