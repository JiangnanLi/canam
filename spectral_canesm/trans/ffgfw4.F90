!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ffgfw4 (gd, ilg,fc,ilh,ir,lon,wrk,ilev,ifax,trigs)
  !
  !     * aug 01/03 - m.lazare. - generalized using "MAXLOT" passed in
  !     *                         common block, instead of hard-coded
  !     *                         value of 256 only valid for the nec.
  !     *                         this is consistent with what is done in vfft3.
  !     *                       - calls new vfft3 instead of vfft2.
  !     * nov 01/92 - j.stacey. previous version ffgfw3.
  !
  !     * driving routine for the fourier transform,
  !     * coefficients to grid.
  !     * ====================
  !     * fc    = fourier coefficients,
  !     * ilh   = first dimension of complex :: fc,
  !     * gd    = grid data,
  !     * ilg   = first dimension of real :: gd, note that it is
  !     *         assumed that gd and fc are equivalenced in main,
  !     *         so obviously ilg must equal 2*ilh,
  !     *         actually fc is declared real :: for convenience.
  !     * ir    = maximum east-west wave number (m=0,ir),
  !     * lon   = number of distinct longitudes,
  !     * wrk   = work space,
  !     * ilev  = number of levels.
  !
  implicit none
  integer :: i
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilh
  integer :: inc
  integer, intent(in) :: ir
  integer :: ir121
  integer :: isign
  integer :: l
  integer :: length
  integer, intent(in) :: lon
  integer :: maxlot
  integer :: n
  integer :: nrest
  integer :: nstart
  integer :: ntimes

  real, intent(inout), dimension(ilg,ilev) :: fc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: gd !< Variable description\f$[units]\f$
  real, intent(in) :: wrk(ilg*maxlot) !< Variable description\f$[units]\f$
  real, intent(in), dimension(lon) :: trigs !< Variable description\f$[units]\f$
  integer, intent(in) :: ifax(*) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  common /itrans/ maxlot
  !-----------------------------------------------------------------
  isign = +1
  inc   =  1
  ir121 =  (ir+1)*2 +1
  if (ilg<lon+2) call xit('FFGFW4',-1)
  !
  !     * abort if workspace size of "WRK" insufficient.
  !
  if (maxlot>ilev) call xit('FFGFW4',-2)
  !
  !     * set to zero fourier coefficients beyond truncation.
  !
  do l=1,ilev
    do i=ir121,lon+2
      fc(i,l) =0.
    end do
  end do ! loop 100
  !
  !     * as many as maxlot are done at once for vectorization.
  !
  nstart=1
  ntimes=ilev/maxlot
  nrest =ilev-ntimes*maxlot
  !
  if (nrest/=0) then
    length=nrest
    ntimes=ntimes+1
  else
    length=maxlot
  end if
  !
  !     * do the fourier transforms.
  !
  do n=1,ntimes
    call vfft3(fc(1,nstart),wrk,trigs,ifax,inc,ilg,lon,length,isign)
    nstart=nstart+length
    length=maxlot
  end do ! loop 300
  !
  return
  !--------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
