!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getgg14(nf,tbarpat,thlqpat,thicpat, &
                   sicnpak,sicpak,gtpak,snopak, &
                   luinpak,luimpak,lrinpak,lrimpak, &
                   envpak,gampak,psipak,alphpak,deltpak,sigxpak, &
                   phispak, &
                   sandpat,claypat,orgmpat, &
                   dpthpat,drnpat,socipat, &
                   fcanpat,alvcpat,alicpat,lnz0pat, &
                   lamxpat,lamnpat,cmaspat,rootpat, &
                   hlakpak,llakpak,blakpak, &
                   flkupak,flkrpak,gicnpak,flndpak, &
                   lc,lg,lct,lgt,ican,icanp1,ignd,ntld,ijpak, &
                   nlon,nlat,iday,gg)
  !
  !     * aug 14/18 - m.lazare. remove maskpak.
  !     * nov 29/16 - m.lazare. new version for gcm19+:
  !     *                       - add "LLAK" and "BLAK" for diana.
  !     *                       - "FLAK" -> "FLKU" and add "FLKR".
  !     *                       - {"LICN","LIC"} -> {"LUIN","LUIM","LRIN","LRIM"}.
  !     *                       - add "HLAK" field.
  !     *                       - "LUIN" -> "GICN".
  !     *                       - add {"LGT","LUIN","LIC"} in conjunction
  !     *                         with changes to new initialization
  !     *                         program initg14.
  !     * aug 08/14 - m.lazare. new version for gcm18:
  !     *                       - add {flak,licn,flnd)pak and socipat in
  !     *                         conjunction with changes to initialization.
  !     *                       - tbar,thlq,thic are also "PAT" arrays now.
  !     * feb 14/13 - m.lazare. previous version getgg13 for gcm17 using
  !     *                       class_v3.6 and tiles.
  !     * mar 30/09 - m.lazare. previous version getgg12 for gcm15i:
  !     *                       - phispak added.
  !     * jan 17/08 - m.lazare. previous version getgg11 for gcm15g/h.
  !     *                       - evol,hvol,eso2 removed (not
  !     *                         used since new aerocom).
  !     * dec 15/03 - m.lazare. previous version getgg10x for gcm15b->f.
  !     *                       - "EVOL","HVOL" and "ESO2" added.
  !     * may 30/03 - m.lazare. previous version getgg9x for gcm13b.
  !
  !     * gets gaussian grids (nlon,nlat) from file nf for the gcm.
  !     * iday is the day of the year.
  !     * input grids can be in any order on the file.
  !     * row length in the model is nlon-1.
  !
  implicit none
  integer, intent(in) :: ican
  integer, intent(in) :: icanp1
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ignd
  integer, intent(in) :: ijpak
  integer :: l
  integer :: m
  integer :: ml
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: ntld
  !
  !     * fields read-in for initial iday:
  !
  real, intent(in), dimension(ijpak,ntld,ignd) :: tbarpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ignd) :: thlqpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ignd) :: thicpat !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ijpak) :: sicnpak !<Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sicpak  !<Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: gtpak   !<Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: snopak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: luinpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: luimpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: lrinpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: lrimpak !< Variable description\f$[units]\f$
  !
  !     * invariant fields:
  !
  real, intent(in), dimension(ijpak,ntld,ignd) :: sandpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ignd) :: claypat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ignd) :: orgmpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,icanp1) :: fcanpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,icanp1) :: alvcpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,icanp1) :: alicpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,icanp1) :: lnz0pat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ican) :: lamxpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ican) :: lamnpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ican) :: cmaspat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld,ican) :: rootpat !< Variable description\f$[units]\f$

  real, intent(in), dimension(ijpak,ntld) :: dpthpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld) :: drnpat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak,ntld) :: socipat !< Variable description\f$[units]\f$

  real, intent(in), dimension(ijpak) :: envpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: gampak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: psipak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: alphpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: deltpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: sigxpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: gicnpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: flndpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: hlakpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: llakpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: blakpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: flkupak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: flkrpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(ijpak) :: phispak !< Variable description\f$[units]\f$
  !
  !     * work field:
  !
  real, intent(in), dimension(1) :: gg !< Variable description\f$[units]\f$
  !
  !     * level index input arrays:
  !
  integer, intent(in), dimension(icanp1) :: lc !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ignd) :: lg !< Variable description\f$[units]\f$
  integer, intent(in) :: lct(ntld * icanp1) !< Variable description\f$[units]\f$
  integer, intent(in) :: lgt(ntld * ignd) !< Variable description\f$[units]\f$
  integer :: nc4to8
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !     * grids read from file nf.

  rewind nf

  call getggbx(sicnpak,nc4to8("SICN"),nf,nlon,nlat,iday,1,gg)
  call getggbx(sicpak ,nc4to8(" SIC"),nf,nlon,nlat,iday,1,gg)
  call getggbx(luinpak,nc4to8("LUIN"),nf,nlon,nlat,iday,1,gg)
  call getggbx(luimpak,nc4to8("LUIM"),nf,nlon,nlat,iday,1,gg)
  call getggbx(lrinpak,nc4to8("LRIN"),nf,nlon,nlat,iday,1,gg)
  call getggbx(lrimpak,nc4to8("LRIM"),nf,nlon,nlat,iday,1,gg)
  call getggbx(gtpak  ,nc4to8("  GT"),nf,nlon,nlat,iday,1,gg)
  call getggbx(snopak ,nc4to8(" SNO"),nf,nlon,nlat,iday,1,gg)
  !
  ml = 0
  do l = 1,ignd
    do m = 1,ntld
      ml = ml + 1
      call getggbx(tbarpat(1,m,l),nc4to8("TBAR"),nf,nlon,nlat,iday, &
                   lgt(ml),gg)
      call getggbx(thlqpat(1,m,l),nc4to8("THLQ"),nf,nlon,nlat,iday, &
                   lgt(ml),gg)
      call getggbx(thicpat(1,m,l),nc4to8("THIC"),nf,nlon,nlat,iday, &
                   lgt(ml),gg)
    end do
  end do ! loop 10
  !
  call getggbx(envpak,nc4to8("  SD"),nf,nlon,nlat,0,1,gg)
  call getggbx(gampak,nc4to8(" GAM"),nf,nlon,nlat,0,1,gg)
  call getggbx(psipak,nc4to8(" PSI"),nf,nlon,nlat,0,1,gg)
  call getggbx(alphpak,nc4to8("ALPH"),nf,nlon,nlat,0,1,gg)
  call getggbx(deltpak,nc4to8("DELT"),nf,nlon,nlat,0,1,gg)
  call getggbx(sigxpak,nc4to8("SIGX"),nf,nlon,nlat,0,1,gg)
  !
  do m = 1,ntld
    call getggbx(dpthpat(1,m),nc4to8("DPTH"),nf,nlon,nlat,0,m,gg)
    call getggbx(drnpat(1,m),nc4to8(" DRN"),nf,nlon,nlat,0,m,gg)
    call getggbx(socipat(1,m),nc4to8("SOCI"),nf,nlon,nlat,0,m,gg)
  end do
  !
  do l = 1,ignd
    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(sandpat(1,m,l),nc4to8("SAND"),nf,nlon,nlat,0, &
                   lgt(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(claypat(1,m,l),nc4to8("CLAY"),nf,nlon,nlat,0, &
                   lgt(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(orgmpat(1,m,l),nc4to8("ORGM"),nf,nlon,nlat,0, &
                   lgt(ml),gg)
    end do
  end do ! loop 20
  !
  do l = 1,ican
    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(fcanpat(1,m,l),nc4to8("FCAN"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(alvcpat(1,m,l),nc4to8("ALVC"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(alicpat(1,m,l),nc4to8("ALIC"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(lnz0pat(1,m,l),nc4to8("LNZ0"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(lamxpat(1,m,l),nc4to8("LAMX"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(lamnpat(1,m,l),nc4to8("LAMN"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(cmaspat(1,m,l),nc4to8("CMAS"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do

    do m = 1,ntld
      ml = (l - 1) * ntld + m
      call getggbx(rootpat(1,m,l),nc4to8("ROOT"),nf,nlon,nlat,0, &
                   lct(ml),gg)
    end do
  end do ! loop 30
  !
  l = icanp1
  do m = 1,ntld
    ml = (l - 1) * ntld + m
    call getggbx(fcanpat(1,m,l),nc4to8("FCAN"),nf,nlon,nlat,0, &
                 lct(ml),gg)
  end do

  do m = 1,ntld
    ml = (l - 1) * ntld + m
    call getggbx(alvcpat(1,m,l),nc4to8("ALVC"),nf,nlon,nlat,0, &
                 lct(ml),gg)
  end do

  do m = 1,ntld
    ml = (l - 1) * ntld + m
    call getggbx(alicpat(1,m,l),nc4to8("ALIC"),nf,nlon,nlat,0, &
                 lct(ml),gg)
  end do

  do m = 1,ntld
    ml = (l - 1) * ntld + m
    call getggbx(lnz0pat(1,m,l),nc4to8("LNZ0"),nf,nlon,nlat,0, &
                 lct(ml),gg)
  end do
  !
  call getggbx(gicnpak,nc4to8("GICN"),nf,nlon,nlat,0,1,gg)
  call getggbx(flndpak,nc4to8("FLND"),nf,nlon,nlat,0,1,gg)
  call getggbx(flkupak,nc4to8("FLKU"),nf,nlon,nlat,0,1,gg)
  call getggbx(hlakpak,nc4to8("LDEP"),nf,nlon,nlat,0,1,gg)
  call getggbx(llakpak,nc4to8("LLAK"),nf,nlon,nlat,0,1,gg)
  call getggbx(blakpak,nc4to8("BLAK"),nf,nlon,nlat,0,1,gg)
  call getggbx(flkrpak,nc4to8("FLKR"),nf,nlon,nlat,0,1,gg)
  !
  call getggbx(phispak,nc4to8("PHIS"),nf,nlon,nlat,0,1,gg)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


