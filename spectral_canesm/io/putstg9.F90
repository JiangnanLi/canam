!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putstg9(nf ,ps,p,c,t,es,phis, &
                   kount,la,lrlmt,ilev,levs,ls,lh, &
                   lsrtotal,latotal,lmtotal,gll)

  !     * nov 04/03 - m.lazare. new version to support mpi, which uses
  !     *                       new routine "PUTSPN" and work array "GLL".
  !     * may 29/95 - m.lazare. previous version putstg8.
  !
  !     * saves global spectral forecast on sequential file nf.
  !     * all fields are written unpacked.
  !     * ls,lh = output label values for full,half levels.
  !
  implicit none
  integer :: ibuf2
  integer :: idiv
  integer :: ies
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ilnsp
  integer :: imdh
  integer :: iphis
  integer :: ipio
  integer :: isavdts
  integer :: itmp
  integer :: ivort
  integer :: iyear
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: lmtotal
  integer, intent(in) :: lrlmt
  integer :: max
  integer :: myrssti
  integer :: n
  integer, intent(in) :: nf
  integer :: nc4to8

  complex, intent(inout) :: ps(la)                    !< Variable description\f$[units]\f$
  complex, intent(inout) :: phis(la)                  !< Variable description\f$[units]\f$
  complex, intent(inout) :: t(la,ilev)                !< Variable description\f$[units]\f$
  complex, intent(inout) :: p(la,ilev)                !< Variable description\f$[units]\f$
  complex, intent(inout) :: c(la,ilev)                !< Variable description\f$[units]\f$
  complex, intent(inout) :: es(la,levs)               !< Variable description\f$[units]\f$
  complex, intent(inout) :: gll(latotal)              !< Variable description\f$[units]\f$

  integer, intent(in), dimension(2,lmtotal + 1) :: lsrtotal !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: ls !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  integer, dimension(8) :: ibuf

  logical :: ok !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  common /ipario/  ipio
  common /keeptim/ iyear,imdh,myrssti,isavdts
  !--------------------------------------------------------------------
  max = 2 * latotal
  !
  iphis = nc4to8("PHIS")
  ilnsp = nc4to8("LNSP")
  itmp = nc4to8("TEMP")
  ivort = nc4to8("VORT")
  idiv = nc4to8(" DIV")
  ies = nc4to8("  ES")

  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts /= 0) then
    !        * in 32-bit, this only works until iyear=2147 !
    ibuf2 = 1000000 * iyear + imdh
  else
    ibuf2 = kount
  end if
  !
  call setlab(ibuf,nc4to8("SPEC"),ibuf2, - 1,1, - 1,1,lrlmt,0)
  !
  !     * surface geopotential (mountains) are saved every time.
  !
  ibuf(3) = iphis
  call putspn (nf,phis,la, &
               lsrtotal,latotal,lmtotal,gll, &
               ibuf,max,ipio,ok)
  !
  !     * save ln(ps) in pascals.
  !
  ibuf(3) = ilnsp
  call putspn (nf,ps,la, &
               lsrtotal,latotal,lmtotal,gll, &
               ibuf,max,ipio,ok)
  !
  !     * save temperature for ilev levels.
  !
  ibuf(3) = itmp
  do l = 1,ilev
    ibuf(4) = lh(l)
    call putspn (nf,t(1,l),la, &
                 lsrtotal,latotal,lmtotal,gll, &
                 ibuf,max,ipio,ok)
  end do ! loop 310
  !
  !     * save vorticity and divergence in pairs for each level.
  !
  do l = 1,ilev
    ibuf(4) = ls(l)

    ibuf(3) = ivort
    call putspn (nf,p(1,l),la, &
                 lsrtotal,latotal,lmtotal,gll, &
                 ibuf,max,ipio,ok)

    ibuf(3) = idiv
    call putspn (nf,c(1,l),la, &
                 lsrtotal,latotal,lmtotal,gll, &
                 ibuf,max,ipio,ok)
  end do ! loop 410
  !
  !     * moisture variable saved for levs levels.
  !
  if (levs == 0) return
  ibuf(3) = ies
  do n = 1,levs
    l = (ilev - levs) + n
    ibuf(4) = lh(l)
    call putspn (nf,es(1,n),la, &
                 lsrtotal,latotal,lmtotal,gll, &
                 ibuf,max,ipio,ok)
  end do ! loop 510
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
