!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine get_strat_aerosol_optics(sw_ext_sa_pak, & ! output
                                    sw_ext_sa_pal, &
                                    sw_ssa_sa_pak, &
                                    sw_ssa_sa_pal, &
                                    sw_g_sa_pak, &
                                    sw_g_sa_pal, &
                                    lw_ext_sa_pak, &
                                    lw_ext_sa_pal, &
                                    lw_ssa_sa_pak, &
                                    lw_ssa_sa_pal, &
                                    lw_g_sa_pak, &
                                    lw_g_sa_pal, &
                                    w055_ext_sa_pak, &
                                    w055_ext_sa_pal, &
                                    w110_ext_sa_pak, &
                                    w110_ext_sa_pal, &
                                    pressure_sa_pak, &
                                    pressure_sa_pal, &
                                    incd, & ! input
                                    kount, &
                                    irefyr, &
                                    irefmn, &
                                    ioffyr, &
                                    iday, &
                                    ilat_in, &
                                    nuan, &
                                    ijpakz, &
                                    nlat, &
                                    nlonz, &
                                    levsa, &
                                    nbs, &
                                    nbl)
  !
  !     * dec 20, 2016 - j cole      new routine to read in volcanic aerosol
  !                                  dataset from ethz which are height and
  !                                  latitude resolved optical properties.

  implicit none

  ! parameters
  integer, parameter :: mon_per_yr = 12
  integer, parameter :: day_per_yr = 365
  integer, parameter :: n_opt_var = 3
  integer, parameter :: n_ext_diag = 3

  !--- a list containing mid month days of the year
  integer, parameter, dimension(mon_per_yr) :: mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

  !--- a list containing mid month days for each month
  integer, parameter, dimension(mon_per_yr) :: mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)

  character(len = 3), parameter :: sw_opt_vars(n_opt_var) = (/"SWE","SWS","SWG"/)
  character(len = 3), parameter :: lw_opt_vars(n_opt_var) = (/"LWE","LWS","LWG"/)

  character(len = 4), parameter :: diag_opt_vars(n_ext_diag) = (/"E055","E110","PRES"/)


  ! output data

  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_ext_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_ext_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_ssa_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_ssa_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_g_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbs) :: sw_g_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_ext_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_ext_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_ssa_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_ssa_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_g_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa,nbl) :: lw_g_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: w055_ext_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: w055_ext_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: w110_ext_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: w110_ext_sa_pal !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: pressure_sa_pak !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ijpakz,levsa) :: pressure_sa_pal !< Variable description\f$[units]\f$

  ! input data

  integer, intent(in) :: incd !< Variable description\f$[units]\f$
  integer, intent(in) :: kount   !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: irefyr !< Variable description\f$[units]\f$
  integer, intent(in) :: irefmn !< Variable description\f$[units]\f$
  integer, intent(in) :: ioffyr !< Variable description\f$[units]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ilat_in !< Variable description\f$[units]\f$
  integer, intent(in) :: nuan !< Variable description\f$[units]\f$
  integer, intent(in) :: ijpakz !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat !< Variable description\f$[units]\f$
  integer, intent(in) :: nlonz !< Variable description\f$[units]\f$
  integer, intent(in) :: levsa !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer, intent(in) :: nbl   !< Number of wavelength intervals for thermal radiative transfer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  ! local data

  integer :: year_prev
  integer :: year_next
  integer :: mon_prev
  integer :: mon_next
  integer :: ib2p
  integer :: ib2n
  integer :: curr_year
  integer :: i
  integer :: ij
  integer :: iz
  integer :: ib
  integer :: iyear
  integer :: ivar
  integer :: imdh
  integer :: rssti
  integer :: isavdts
  integer :: ijpak_loc

  integer*4 :: mynode

  real :: day1
  real :: day2
  real :: day3
  real :: w1
  real :: w2
  real :: e
  real :: e1
  real :: e2
  real :: s
  real :: s1
  real :: s2
  real :: g
  real :: g1
  real :: g2

  real, dimension(ijpakz,levsa) :: idata_pak
  real, dimension(ijpakz,levsa) :: idata_pal

  character(len = 4) :: name

  ! common data
  !--- mynode used to control i/o to stdout
  common /mpinfo/ mynode

  !--- keeptime is required for iyear
  common /keeptim/ iyear,imdh,rssti,isavdts

  !----------------------------------------------------------------------
  !
  ! this program reads in the stratospheric/volcanic aerosol datasets to be used
  ! by the gcm.
  !
  ! it basically reads in datasets provided by ethz for cmip6, which are
  ! height resolved optical properties for the radiative transfer wavelength
  ! intervals.
  !
  ! the data can be read in three different ways,
  !
  ! 1. fully transient mode - time varying data is interpolated for each month
  !                           in a time-series.
  !
  ! 2. repeated annual cycle - the data is time varying but repeats a particular
  !                            year from a time-series.
  !
  ! 3. single month - data from a single month in the time-series is used.  this
  !                   seems odd but has been requested for some studies (geomip).
  !
  !----------------------------------------------------------------------

  if (mynode == 0) then
    write(6, * )'GET_STRAT_AEROSOL_OPTICS: NEW STRATOSPHERIC ', &
             'AEROSOL REQUIRED'
  end if

  !
  ! figure out the year and month to use for the interpolation in time.
  !

  !.....determine the year for which data is required

  if (irefyr > 0) then
    !--- when irefyr > 0 read emissions from the data file for
    !--- a repeated annual cycle of year irefyr
    curr_year = irefyr
  else
    !--- otherwise read time dependent emissions from the data
    !--- file using iyear to determine the current year
    curr_year = iyear + ioffyr
  end if

  !.....check on iday

  if (iday < 1 .or. iday > day_per_yr) then
    if (mynode == 0) then
      write(6, * )'GET_STRAT_AEROSOL_OPTICS: IDAY is out of range.', &
                 'IDAY = ',iday
    end if
    call xit("GET_STRAT_AEROSOL_OPTICS", - 1)
  end if

  !.....determine previous and next year/month, relative to iday

  mon_prev = 0
  if (irefmn < 0) then
    !--- the month is transient.
    if (iday >= 1 .and. iday < mm_doy(1)) then
      year_prev = curr_year - 1
      year_next = curr_year
      mon_prev = 12
      mon_next = 1
    else if (iday >= mm_doy(mon_per_yr) &
                     .and. iday <= day_per_yr) then
      year_prev = curr_year
      year_next = curr_year + 1
      mon_prev = mon_per_yr
      mon_next = 1
    else
      do i = 2,mon_per_yr
        if (iday >= mm_doy(i - 1) .and. &
            iday < mm_doy(i)) then
          year_prev = curr_year
          year_next = curr_year
          mon_prev = i - 1
          mon_next = i
          exit
        end if
      end do
    end if
  else
    !--- the month is not changing.
    mon_prev = irefmn
    mon_next = irefmn
  end if

  if (mon_prev == 0) then
    if (mynode == 0) then
      write(6, * )'GET_STRAT_AEROSOL_OPTICS: Problem setting year', &
                 '/month on IDAY = ',iday
    end if
    call xit("GET_STRAT_AEROSOL_OPTICS", - 2)
  end if

  !.... determine time stamps (ibuf2 values) to be used to read
  !.... appropriate data from the file

  ib2p = year_prev * 100 + mon_prev
  ib2n = year_next * 100 + mon_next

  if (mynode == 0) then
    write(6, * )'GET_STRAT_AEROSOL_OPTICS: year_prev,mon_prev,', &
             'year_next,mon_next: ', &
              year_prev,mon_prev,year_next,mon_next
    write(6, * )'GET_STRAT_AEROSOL_OPTICS: ib2p,ib2n: ',ib2p,ib2n
  end if

  !
  !     * get new boundary fields for next mid-month day.
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      !         * data for each band is saved in a separate variable but
      !         * are read into a single variable here.

      do ib = 1, nbs
        do ivar = 1, n_opt_var
          write(name,101) sw_opt_vars(ivar),ib
          call get_zonal(idata_pak, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
              do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ext_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
              end do ! iz
            case (2)
              do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ssa_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
              end do ! iz
            case (3)
              do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_g_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
              end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 3)
          end select
        end do ! ivar
      end do ! ib

      do ib = 1, nbl
        do ivar = 1, n_opt_var
          write(name,101) lw_opt_vars(ivar),ib
          call get_zonal(idata_pak, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   lw_ext_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   lw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case (2)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   lw_ssa_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   lw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case (3)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   lw_g_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   lw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 4)
          end select

        end do ! ivar
      end do ! ib

      do ivar = 1, n_ext_diag
        name = diag_opt_vars(ivar)

        call get_zonal(idata_pak, &
                       nlat, &
                       nlonz, &
                       ilat_in, &
                       ijpakz, &
                       ib2n, &
                       nuan, &
                       levsa, &
                       name)

        call get_zonal(idata_pal, &
                       nlat, &
                       nlonz, &
                       ilat_in, &
                       ijpakz, &
                       ib2n, &
                       nuan, &
                       levsa, &
                       name)

        select case (ivar)
          case (1)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w055_ext_sa_pak(ij,iz) = idata_pak(ij,iz)
                  w055_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (2)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w110_ext_sa_pak(ij,iz) = idata_pak(ij,iz)
                  w110_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (3)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  pressure_sa_pak(ij,iz) = idata_pak(ij,iz)
                  pressure_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case default
            call xit("GET_STRAT_AEROSOL_OPTICS", - 3)
        end select

      end do ! ivar

    end if

  else

    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !         * start-up time. get fields for previous and target mid-month days.
      !
      do ib = 1, nbs
        do ivar = 1, n_opt_var

          write(name,101) sw_opt_vars(ivar),ib
          call get_zonal(idata_pak, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2p, &
                         nuan, &
                         levsa, &
                         name)

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ext_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case (2)
              do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ssa_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
              end do ! iz
            case (3)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_g_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                   sw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 5)
          end select
        end do ! ivar
      end do ! ib

      do ib = 1, nbl
        do ivar = 1, n_opt_var
          write(name,101) lw_opt_vars(ivar),ib
          call get_zonal(idata_pak, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2p, &
                         nuan, &
                         levsa, &
                         name)

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_ext_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                    lw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case (2)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_ssa_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                    lw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case (3)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_g_sa_pak(ij,iz,ib) = idata_pak(ij,iz)
                    lw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 6)
          end select

        end do ! ivar
      end do ! ib

      do ivar = 1, n_ext_diag
        name = diag_opt_vars(ivar)

        call get_zonal(idata_pak, &
                       nlat, &
                       nlonz, &
                       ilat_in, &
                       ijpakz, &
                       ib2n, &
                       nuan, &
                       levsa, &
                       name)

        call get_zonal(idata_pal, &
                       nlat, &
                       nlonz, &
                       ilat_in, &
                       ijpakz, &
                       ib2n, &
                       nuan, &
                       levsa, &
                       name)

        select case (ivar)
          case (1)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w055_ext_sa_pak(ij,iz) = idata_pak(ij,iz)
                  w055_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (2)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w110_ext_sa_pak(ij,iz) = idata_pak(ij,iz)
                  w110_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (3)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  pressure_sa_pak(ij,iz) = idata_pak(ij,iz)
                  pressure_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case default
            call xit("GET_STRAT_AEROSOL_OPTICS", - 6)
        end select

      end do ! ivar

      !--- interpolate to current day
      day1 = real(mm_doy(mon_prev))
      day2 = real(iday)
      day3 = real(mm_doy(mon_next))

      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.

      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)

      if (mynode == 0) then
        write(6, * ) &
         'GET_STRAT_AEROSOL_OPTICS: Interpolating at ',curr_year, &
         'day',iday,' between ',year_prev,' day',mm_doy(mon_prev), &
         ' and ',year_next,' day',mm_doy(mon_next), &
         ' using weights ',w1,w2
      end if

      ijpak_loc = ijpakz - 1

      do ib = 1, nbs
        do iz = 1,levsa
          do i = 1,ijpak_loc
            e1 = sw_ext_sa_pal(i,iz,ib)
            e2 = sw_ext_sa_pak(i,iz,ib)
            s1 = sw_ssa_sa_pal(i,iz,ib)
            s2 = sw_ssa_sa_pak(i,iz,ib)
            g1 = sw_g_sa_pal(i,iz,ib)
            g2 = sw_g_sa_pak(i,iz,ib)

            g = w1 * e1 * s1 * g1 + w2 * e2 * s2 * g2
            s = w1 * e1 * s1 + w2 * e2 * s2
            e = w1 * e1 + w2 * e2

            sw_g_sa_pak(i,iz,ib)   = g/s
            sw_ssa_sa_pak(i,iz,ib) = s/e
            sw_ext_sa_pak(i,iz,ib) = e

          end do ! i
        end do ! iz
      end do ! ib

      do ib = 1, nbl
        do iz = 1,levsa
          do i = 1,ijpak_loc

            e1 = lw_ext_sa_pal(i,iz,ib)
            e2 = lw_ext_sa_pak(i,iz,ib)
            s1 = lw_ssa_sa_pal(i,iz,ib)
            s2 = lw_ssa_sa_pak(i,iz,ib)
            g1 = lw_g_sa_pal(i,iz,ib)
            g2 = lw_g_sa_pak(i,iz,ib)

            g = w1 * e1 * s1 * g1 + w2 * e2 * s2 * g2
            s = w1 * e1 * s1 + w2 * e2 * s2
            e = w1 * e1 + w2 * e2

            lw_g_sa_pak(i,iz,ib)   = g/s
            lw_ssa_sa_pak(i,iz,ib) = s/e
            lw_ext_sa_pak(i,iz,ib) = e

          end do ! i
        end do ! iz
      end do ! ib

      do iz = 1,levsa
        do i = 1,ijpak_loc

          w055_ext_sa_pak(i,iz) = w1 * w055_ext_sa_pal(i,iz) &
                                  + w2 * w055_ext_sa_pak(i,iz)

          w110_ext_sa_pak(i,iz) = w1 * w110_ext_sa_pal(i,iz) &
                                  + w2 * w110_ext_sa_pak(i,iz)

          pressure_sa_pak(i,iz) = w1 * pressure_sa_pal(i,iz) &
                                  + w2 * pressure_sa_pak(i,iz)
        end do ! i
      end do ! iz

    else
      !
      !         * this is in the middle of a run.
      !

      do ib = 1, nbs
        do ivar = 1, n_opt_var
          write(name,101) sw_opt_vars(ivar),ib

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case (2)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case (3)
               do iz = 1, levsa
                 do ij = 1, ijpakz
                   sw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                 end do ! ij
               end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 7)
          end select

        end do ! ivar
      end do ! ib

      do ib = 1, nbl
        do ivar = 1, n_opt_var
          write(name,101) lw_opt_vars(ivar),ib

          call get_zonal(idata_pal, &
                         nlat, &
                         nlonz, &
                         ilat_in, &
                         ijpakz, &
                         ib2n, &
                         nuan, &
                         levsa, &
                         name)

          select case (ivar)
            case (1)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_ext_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case (2)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_ssa_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case (3)
               do iz = 1, levsa
                  do ij = 1, ijpakz
                    lw_g_sa_pal(ij,iz,ib) = idata_pal(ij,iz)
                  end do ! ij
               end do ! iz
            case default
              call xit("GET_STRAT_AEROSOL_OPTICS", - 8)
          end select

        end do ! ivar
      end do ! ib

      do ivar = 1, n_ext_diag
        name = diag_opt_vars(ivar)

        call get_zonal(idata_pal, &
                       nlat, &
                       nlonz, &
                       ilat_in, &
                       ijpakz, &
                       ib2n, &
                       nuan, &
                       levsa, &
                       name)

        select case (ivar)
          case (1)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w055_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (2)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  w110_ext_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case (3)
             do iz = 1, levsa
                do ij = 1, ijpakz
                  pressure_sa_pal(ij,iz) = idata_pal(ij,iz)
                end do ! ij
             end do ! iz
          case default
            call xit("GET_STRAT_AEROSOL_OPTICS", - 8)
        end select

      end do ! ivar

    end if
  end if

101 format(a3,i1)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
