!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getxfxx(nf,xsfxpak,xsfxpal,nlon,nlat, &
                   incd,iday,mday,kount,gg,namts)
  !
  !     * may 28, 2003 - m. lazare. new version which removes "ROW"
  !     *                           fields and internal packing densities.
  !     *                           also, uses new routine "GETAGBX" instead
  !     *                           of "GETAGB".
  !     *                           note that no interpolation is done at
  !     *                           first step (removes call to intxfx).
  !     * may  7/98 - r.harvey - previous version getxfx.
  !
  !     * read in tracer surface flux field(s).
  !     * time label is the julian date of the first of the month
  !     * (nfdm) for which the sst are the monthly average.
  !     * when the model is moving through the year (incd/=0),
  !     * then new fields are read on every mid month day (mmd),
  !     * and interpolation will be done between the two adjacent
  !     * mid month days. xsfxpak is the precedent, xsfxpal the following.
  !
  implicit none
  real, intent(in), dimension(1) :: xsfxpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(1) :: xsfxpal !< Variable description\f$[units]\f$
  real, intent(in), dimension(1) :: gg !< Variable description\f$[units]\f$
  integer, dimension(12) :: nfdm
  integer, dimension(12) :: mmd

  integer, intent(in) :: nf !< Variable description\f$[units]\f$
  integer, intent(in) :: nlon !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat !< Variable description\f$[units]\f$
  integer, intent(in) :: incd !< Variable description\f$[units]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(inout) :: mday   !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: kount   !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: namts !< Variable description\f$[units]\f$

  integer :: imo
  integer :: imot
  integer :: iyrmot
  integer :: jour1
  integer :: jour
  integer :: n
  integer :: mbuf2
  integer :: l1
  integer :: mday1
  integer :: m
  integer :: l

  integer :: iyear
  integer :: iymdh
  integer :: imdh
  integer :: myrssti
  integer :: isavdts
  common /keeptim/ iyear,imdh,myrssti,isavdts
  !
  data nfdm/  1, 32, 60, 91,121,152,182,213,244,274,305,335/
  data  mmd/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
  !----------------------------------------------------------------------
  !     * value to search for in ibuf(2) is dependant on whether run
  !     * is using standard boundary forcing or multi-year conditions.
  !
  !     * in 32-bit, this only works until iyear=2147 !
  iymdh = 1000000 * iyear + imdh
  imo = (iymdh - (1000000 * iyear))/10000
  if (imo /= 12) then
    imot = imo + 1
    iyrmot = 1000000 * iyear + 10000 * imot + 100
  else
    imot = 1
    iyrmot = (iyear + 1) * 1000000 + 10000 * imot + 100
  end if
  !
  if (incd == 0) then
    !       *
    !       * model is stationary.
    !       * abort if running amip-like experiment.
    !
    if (myrssti /= 0)           call xit('GETXFXX', - 1)
    if (kount == 0) then
      !         *
      !         * start-up time. read-in xsfx for average of month.
      !         *
      !          mday=iday
      call getagbx(xsfxpak,namts,nf,nlon,nlat,iday,gg)
      return
    else
      !         *
      !         * not a new integration. no new field required.
      !         *
      return
    end if
  else
    !       *
    !       * the model is moving.
    !       *
    if (kount == 0) then
      !         *
      !         * start-up time. get xsfx for previous and following mid month days.
      !         * note that for amip-like runs, no "PREVIOUS" conditions
      !         * exist so that start conditions held constant through first
      !         * half-month.
      !         *
      l = 0
      do n = 1,12
        if (iday >= mmd(n)) l = n + 1
      end do ! loop 150

      if (l == 0 .or. l == 13) l = 1
      mday = mmd(l)
      write(6,6020) mday
      call getagbx(xsfxpak,namts,nf,nlon,nlat,iymdh,gg)
      call getagbx(xsfxpal,namts,nf,nlon,nlat,iymdh,gg)

      return
    else
      !         *
      !         * this is in the middle of a run.
      !         *
      print * ,'INSIDE GETXFX - - IDAY, MDAY = ',iday,' ',mday

      print * ,'TIME TO RESET MDAYT.'
      write(6,6010)
      !           *
      !           * time to get new tracer surface flux fields for next
      !           * mid-month day.
      !           *
      m = 0
      do n = 1,12
        if (iday == mmd(n)) m = n + 1
      end do ! loop 250

      if (m == 13) m = 1
      if (m == 0)                         call xit('GETXFXX', - 2)
      mday = mmd(m)
      jour = nfdm(m)
      write(6,6020) mday
      if (myrssti == 0) then
        mbuf2 = jour
      else
        mbuf2 = iyrmot
      end if
      print * ,'BEFORE GETAGBX - - IDAY, MDAY = ',iday,' ',mday
      call getagbx(xsfxpal,namts,nf,nlon,nlat,mbuf2,gg)
      print * ,'AFTER GETAGBX - - IDAY, MDAY = ',iday,' ',mday
      return

    end if
  end if
  !---------------------------------------------------------------------
6010 format('0    NEW TRACER SURFACE FLUX FIELDS REQUIRED')
6020 format('0    MDAYT RESET TO',i6)
end
