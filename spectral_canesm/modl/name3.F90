!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine name3(nam,ic,integ)

  !     * apr 17/15 - m.lazare/   new version for gcm18+:
  !     *             j.cole/     - rename "INT" as "INTEG"
  !     *             f.majaess.    to avoid conflict with intrinsic
  !     *                           of same name. this is a purely
  !     *                           cosmetic modification so the same
  !     *                           name is kept.
  !     * dec 30/09 - k.vonsalzen - new, based on "NAME2", similar to "NOM".
  !
  !     * subroutine qui definie le nom des champs associe aux differents
  !     * traceurs en fonction du nombre de traceurs.

  implicit none
  integer, intent(inout) :: integ
  integer :: n1
  integer :: n2
  integer :: n3
  integer, intent(inout) :: nam
  integer :: nc4to8
  integer :: nn

  character*4 :: cnam !<
  character*1, intent(in) :: ic(1) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !----------------------------------------------------------------------
  n3=integ/100
  nn=integ-n3*100
  n2=nn/10
  n1=nn-n2*10
  if (integ>999) call                          xit('NAME3',-1)
  write(cnam,1000) ic,n3,n2,n1
  nam=nc4to8(cnam)

  1000 format(1a1,3i1)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
