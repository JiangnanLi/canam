!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine monsav(ismon,iday,delt)
  !
  !     * apr 01/2005 - m.lazare. new routine to calculate number of
  !     *                         timesteps in a month.
  !
  !     * daysinmon = number of days in the current month.
  !     * delt      = model timestep length (seconds).
  !     * iday      = day of the year
  !     * ismon     = number of timesteps in a month.
  !     * month     = current month (3-character name).
  !
  implicit none

  integer, intent(inout) :: ismon   !< Timestep interval to save monthly output \f$[unitless]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer :: mdum !<
  integer :: daysinmon !<
  real, intent(in) :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  character*3 :: month !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * get current month. mdum is a dummy returned argument not used.
  !     * ABORT IF MONTH=='XXX'.
  !
  call idatec(month,mdum,iday)
  if (month=='XXX')   call xit('MONSAV',-1)
  !
  !     * determine the number of days in the month.
  !
  if (month=='JAN' .or. month=='MAR' .or. month=='MAY' .or. &
      month=='JUL' .or. month=='AUG' .or. month=='OCT' .or. &
      month=='DEC') then
    daysinmon=31
  else if (month=='APR' .or. month=='JUN' .or. &
         month=='SEP' .or. month=='NOV') then
    daysinmon=30
  else if (month=='FEB') then
    daysinmon=28
  else
    call xit('MONSAV',-2)
  end if
  !
  !     * finally, determine number of timesteps in month, for normaliztion
  !     * factor of monthly accumulated output fields.
  !
  ismon=daysinmon*nint(86400./delt)

  return
  !--------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

