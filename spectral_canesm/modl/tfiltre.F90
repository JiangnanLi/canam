!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tfiltre(p,c,t,q,ps, pm,cm,tm,qm,psm, &
                         fp,fc,ft,fq,fps, ilev,levs,la, &
                         ltf2)
  !
  !     * feb 26/96 - m.lazare. bracket calls for moisture so not
  !     *                       called if levs passed in as zero (to
  !     *                       support semilagrangian code).
  !     * oct 26/87 - r.laprise.
  !
  !     * apply a robert time filter on model variables in two steps as...
  !     * TF1: X' (K) = (1-2F) X (K) + F X''(K-1),
  !     * TF2: X''(K-1) =        X'(K-1) + F X  (K).
  !     *
  !     * tf2 should be activated 2 steps following a forward timestep,
  !     * tf1 should be activated 1 step  after     a forward timestep.
  !
  implicit none
  real, intent(inout) :: fc
  real, intent(inout) :: fp
  real, intent(inout) :: fps
  real, intent(inout) :: fq
  real, intent(inout) :: ft
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: la
  integer, intent(inout) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$

  logical, intent(inout) :: ltf2 !< Variable description\f$[units]\f$
  complex, intent(inout) :: p (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: c (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: t (la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: q (la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: ps (la) !< Variable description\f$[units]\f$
  complex, intent(inout) :: pm(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: cm(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: tm(la,ilev) !< Variable description\f$[units]\f$
  complex, intent(inout) :: qm(la,levs) !< Variable description\f$[units]\f$
  complex, intent(inout) :: psm(la) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  if (ltf2) then
    call tf2 (p,  pm,  fp, la*ilev)
    call tf2 (c,  cm,  fc, la*ilev)
    call tf2 (t,  tm,  ft, la*ilev)
    if (levs>0) then
      call tf2 (q,  qm,  fq, la*levs)
    end if
    call tf2 (ps, psm, fps, la)
  end if
  !
  call tf1 (p,  pm,  fp, la*ilev)
  call tf1 (c,  cm,  fc, la*ilev)
  call tf1 (t,  tm,  ft, la*ilev)
  if (levs>0) then
    call tf1 (q,  qm,  fq, la*levs)
  end if
  call tf1 (ps, psm, fps, la)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
