!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine calcns3(rns2la,lsr,mindex,lm,la)
  !
  !     * nov 03/03 - m.lazare. new version which uses "MINDEX" (local
  !     *                       to each node), to determine the local node
  !     *                       values for "RNS2LA".
  !     * jul 14/92 - e.chan.   previous version calcns2.
  !
  !     * calculates array (length la) of n*n+1 values for subsequent
  !     * use in model. since this array is invariant during the course
  !     * of a model simulation, it is pre-computed only once at the
  !     * beginning of a job.
  !
  implicit none
  integer :: k
  integer :: kl
  integer :: kr
  integer, intent(inout) :: la
  integer, intent(inout) :: lm
  integer :: m
  integer :: ms
  integer :: ns
  !
  integer, intent(in) :: lsr(2,lm+1) !< Variable description\f$[units]\f$
  integer, intent(in), dimension(lm) :: mindex !< Variable description\f$[units]\f$
  real, intent(inout), dimension(la) :: rns2la !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-------------------------------------------------------------------
  do m=1,lm
    ms=mindex(m)-1
    kl=lsr(1,m)
    kr=lsr(1,m+1)-1
    do k=kl,kr
      ns=ms+(k-kl)
      rns2la(k)=real(ns*(ns+1))
    end do ! loop 290
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
