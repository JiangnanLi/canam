!>\file
!>\brief Set up namelists to hold various tracers' definitions, propeties, and parameters
!!
!! @author Slava Kharin (30 Jul 2019), Mike Lazare (Sep 16 2009), Deji Akingunola (Spring, 2021)
!

module tracers_defs_mod
   use psizes_19, only: ilev

   !--- Maximum number of user-defined tracers' attributes
   integer, parameter :: max_ntrac_nml = 512

   !--- Strings of user-defined tracers' attributes
! Each tracer attribute line is specified as a string of a comma-separated list of key-value pair,
! with the following key names;

   ! --- Name     ! 4-Character Tracer short name
   ! --- ADV      ! Integer flag to indicate if tracer is advected (1/0)
   ! --- PHS      ! Integer flag to indicate if physics is applied to the tracer (1/0)
   ! --- SRF      ! Integer flag to indicate availability of surface fluxes; chem tracers have special flags -1 and -2
   ! --- WET      ! Integer flag to indicate if tracer undergoes wet deposition
   ! --- DRY      ! Integer flag to indicate if tracer undergoes dry deposition
   ! --- CCH      ! Integer flag for convective chemistry
   ! --- ICFLAG   ! Integer flag to indicate if initial conditions are available for the tracer
   ! --- XREF     ! Real-valued ...
   ! --- XPOW     ! Real-valued ...
   ! --- MW       ! Real-valued tracer's molecular weight
   character(len=128), dimension(max_ntrac_nml) :: tracers_inp_list
!
   character(len=4) :: itrvarc  ! FUNCTIONAL FORM OF TRACER (I.E. '   Q' FOR MIXING RATIO).
   integer :: itrvar            ! Set from itrvarc

!  !--- Tracers top and bottom levels (For use in the AGCM restarts)
   integer :: itrlvf, &    ! Define non-advective tracers's bottom level
              itrlvs       ! Define non-advective tracers's topmost level

!  !--- Hybrid information for moisture.
   real    :: sref, &      ! REFERENCE MOISTURE VALUE FOR QHYB
              spow, &      ! REFERENCE POWER    VALUE FOR QHYB.
   ! -- QMIN and SMIN, set from SREF and SPOW
              qmin, &
              smin         ! MINIMUM   MOISTURE VALUE FOR QHYB.
   character(len=4) :: moistc   ! FUNCTIONAL FORM OF MOISTURE VARIABLE ('QHYB','   Q', 'SL3D','SLQB).
   integer :: moist             ! Set from moistc

   !--- List of tracer correction parameters used in the AGCM.
   integer :: reset_tracer_at_kount, &
              reset_tracer_corr,     &
              reset_xsfx_corr

   save

   !-------------------------------------------------------------
   !--- Set up namelists to hold tracers list and correction parameters
   !-------------------------------------------------------------
   namelist /tracer_config/ tracers_inp_list, &
                            itrlvs, itrlvf, itrvarc, &
                            sref, spow, moistc,      &
                            reset_tracer_at_kount, &
                            reset_tracer_corr, &
                            reset_xsfx_corr

 contains
   integer function tracers_nml_input(iu, mynode, machine)
      implicit none

      integer, intent(in) :: iu
      integer, intent(in) :: mynode
      integer, intent(in) :: machine
!
      integer :: nml_read_status
      real    :: pinv
      character(len=256) :: line
      integer, external :: nc4to8

      !-- Default tracers' attributes user-defined list
      tracers_inp_list = ''

      itrvarc = 'QHYB'

      !-- Default tracers' top/bottom levels
      itrlvf = ilev
      itrlvs = 1

      !--- Set the tracer correction parameter defaults
      reset_tracer_at_kount = 0 ! kount value at which tracer corrections are reset
                                ! (typically the kount value in the 1st restart file of the run)
      reset_tracer_corr = 0     ! reset all tracer corrections computed in ENERSVB if set to 1
      reset_xsfx_corr = 0       ! reset XSFX tracer corrections XSFXCORR in GCM18 if set to 1

      !--  Default values hybrid moisture variables
      sref  = 0.0
      spow  = 1.0
      moistc = 'QHYB'

      !  Read the tracer_config input namelist
      rewind(iu)
      read(iu, nml = tracer_config, iostat = nml_read_status)
      if (nml_read_status > 0) then
         if (mynode == 0) then
            write(6, *) ' Error in reading tracer_config namelist in tracers_defs_mod.F90'
            write(6, *) ' Read status = ', nml_read_status
            backspace(iu)
            read(iu, '(A)') line
            write(6, *) 'Invalid line in tracer_config namelist is: ', trim(line)
         end if
         tracers_nml_input = -1
         return
      else if (nml_read_status < 0) then
         if (mynode == 0) write(6, *) ' No tracer_config namelist found in file --> ABORT !!!!'
         tracers_nml_input = -1
         return
      end if

      ! * Set itrvar and moist
      itrvar = nc4to8(itrvarc)
      moist  = nc4to8(moistc)

      ! * Set QMIN value.
      qmin = sref * (10.0**(-16.0 / real(machine)))

      !* Calculate lower bound for hole-filling for hybrid moisture variable
      pinv = 1.0 / spow
      if (sref == 0.0) then
         smin = qmin
      else
         smin = sref / ((1.0 + spow * log(sref / qmin))**pinv)
      end if
!
      tracers_nml_input = 1
      return
   end function tracers_nml_input

end module tracers_defs_mod
!> \file
