#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * mar 23/2019 - j. cole     - save site data for cfmip/cmip6
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 20/2018 - m.lazare.   - ensure eco2pak and eco2pal are defined for all cases.
!     * nov 07/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     *                           - now save all tiled fields, sic and sicn
!     *                             at (lsgg.or.lsbeg) frequency.
!     * aug 14/2018 - m.lazare.   - remove gtpal.
!     * aug 14/2018 - m.lazare.   - remove {gtm,sicp,sicm,sicnp,sicm}.
!     * jul 30/2018 - m.lazare.   - unused {fmi,gc} removed.
!     * mar 09/2018 - m.lazare.   - beg changed from pak to pal
!     *                             and bwg removed.
!     * mar 04/2018 - m.lazare.   remove unused iocean=0 section.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * apr 19/2018 - m.lazare.    - add fcoo and eco2.
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}.
!     * aug 10/2017 - m.lazare.    add lakes saving.
!     * aug 07/2017 - m.lazare.    initial git verison.
!     * aug 10/2017 - m.lazare.    - add saving of {fnpat,tnpat,rhonpat,bcsnpat,refpat}
!     *                              under cpp control of saving tiled
!     *                              fields.
!     *                            - {phlpak,phspak,phdpak) are not
!     *                              accumulated for now, so zeroing
!     *                              removed.
!     *                            - add lakes fields.
!     * aug 07/2017 - m.lazare.    - initial git version.
!     * mar 14/2015 - m.lazare/    new version for gcm18:
!     *               k.vonsalzen/ - add (for nemo/cice support):
!     *               j.cole.        hflipal,begipal,begopal,bwgopal,
!     *                              hseapal,obegpal,obwgpal,rainspal,snowspal.
!     *                            - bugfixes for isccp fields and additions
!     *                              for calipso and modis.
!     * jul 10/2013 - m.lazare/    previous version saveacc5 for gcm17:
!     *               k.vonsalzen/
!     *               j.cole.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - added: gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     * may 07/2012 - m.lazare/    previous version saveacc4 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     * may 04/2010 - m.lazare.    previous version saveacc3i for gcm15i:
!     *                            - add/remove fields (see other
!     *                              common decks for details).
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 20/2009 - k.vonsalzen/ previous version saveacc3h for gcm15h:
!     *               m.lazare.    - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud field
!     *                              (using optical depth cutoff): cldo.
!     * apr 21/2008 - l.solheim/   previous version saveacc3g for gcm15g:
!     *               m.lazare/    - add new radiative forcing arrays
!     *               x.ma/          (under control of "%DF RADFORCE").
!     *               k.vonsalzen. - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem.
!     * jan 11/2006 - j.cole/      previous version saveacc3f for gcm15f:
!     *               m.lazare.    add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    two extra new fields ("DMC" and "SMC")
!     *                            under control of "%IF DEF,XTRACONV".
!     * sep 11/2006 - m.lazare.    - calls "NAME2" instead of "NAME".
!     *                            - saved name of cscbpak now consistent
!     *                              (ie "CSCB") instead of "SDCB".
!     * dec 15/2005 - m.lazare/    previous version for gcm15d/e:
!     *               k.vonsalzen. - add monthly accumulation fields (ie
!     *                              pam/rom) for pwat,qtph,qtpf,xtvi,xtph,
!     *                              xtpf, for new moisture/tracer
!     *                              conservation diagnostics.
!     *                            - bugfixes for xaddpak and qtphpak savings.
!
#if defined isavdts
!     * if at a new day boundary (nsecs=0), redefine "IMDH" to be
!     * in format "MMDD24"(where "DD" is the previous day), using
!     * information from previous steps "IMDH" (STORED IN "IMDHO"
!     * defined just before call to gcmtym2). this will be applied
!     * in the ensuing routines through the "KEEPTIM" common block
!     * and "IMDH" will be reset back at the end of this common deck.
!
      if (nsecs==0) then
         imdhn=imdh
         imdo=imdho/100
         imdh=imdo*100 + 24
      end if
#endif
#if defined (agcm_ctem)
!
!     * define time label for ctem restart and output files.
!     * save ctem output.
!
      iymd = iyearo*10000+imdho/100
      if (lsday) then
#include "save_ctem.h"
      end if
#endif

!     * save screen level fields on file nupr every israd steps.

if (lsrad) then
  k=kount
  call putgg(srhnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SRHN") &
                     , 1,gll,wrks)
  call putgg(srhxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SRHX") &
                     , 1,gll,wrks)
  call putgg(stmxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("STMX") &
                     ,1,gll,wrks)
  call putgg(stmnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("STMN") &
                     ,1,gll,wrks)
  call putgg(swxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" SWX") &
                     , 1,gll,wrks)
  call putgg(swxupak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SWXU") &
                     , 1,gll,wrks)
  call putgg(swxvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SWXV") &
                     , 1,gll,wrks)
  do ij=1,ijpak
     srhnpak(ij)=+1.0e+39
     srhxpak(ij)=-1.0e+39
     stmxpak(ij)=-1.0e+39
     stmnpak(ij)=+1.0e+39
     swxpak(ij)=-1.0e+39
  end do
end if

!     * save selected fields on file nu3hr every is3hr (3-hour) steps.

if (ls3hr) then
  k=kount
  call putgg(cldt3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CLDT"),1,gll,wrks)
  call putgg(hfl3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" HFL"),1,gll,wrks)
  call putgg(hfs3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" HFS"),1,gll,wrks)
  call putgg(rof3hpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" ROF"),1,gll,wrks)
  call putgg(wgl3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" WGL"),1,gll,wrks)
  call putgg(wgf3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" WGF"),1,gll,wrks)
  call putgg(pcp3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" PCP"),1,gll,wrks)
  call putgg(pcpc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCPC"),1,gll,wrks)
  call putgg(pcpl3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCPL"),1,gll,wrks)
  call putgg(pcps3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCPS"),1,gll,wrks)
  call putgg(pcpn3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCPN"),1,gll,wrks)
  call putgg(fdl3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FDL"),1,gll,wrks)
  call putgg(fdlc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FDLC"),1,gll,wrks)
  call putgg(flg3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FLG"),1,gll,wrks)
  call putgg(fss3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FSS"),1,gll,wrks)
  call putgg(fssc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSSC"),1,gll,wrks)
  call putgg(fsd3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FSD"),1,gll,wrks)
  call putgg(fsg3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FSG"),1,gll,wrks)
  call putgg(fsgc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSGC"),1,gll,wrks)
  call putgg(sq3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("  SQ"),1,gll,wrks)
  call putgg(st3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" STA"),1,gll,wrks)
  call putgg(su3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" SUA"),1,gll,wrks)
  call putgg(sv3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" SVA"),1,gll,wrks)
  call putgg(olr3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" OLR"),1,gll,wrks)
  call putgg(fsr3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FSR"),1,gll,wrks)
  call putgg(olrc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("OLRC"),1,gll,wrks)
  call putgg(fsrc3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSRC"),1,gll,wrks)
  call putgg(fso3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" FSO"),1,gll,wrks)
  call putgg(pwat3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PWAT"),1,gll,wrks)
  call putgg(clwt3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CLWT"),1,gll,wrks)
  call putgg(cict3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CICT"),1,gll,wrks)
  call putgg(pmsl3hpak,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PMSL"),1,gll,wrks)

  do m=1,im
    call putgg(gtpat  (1,m),lon1,ilat,khem,npgg,k,nu3hr, &
                      nc4to8(" GTT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(farepat(1,m),lon1,ilat,khem,npgg,k,nu3hr, &
                      nc4to8("FARE"),m,gll,wrks)
  end do
  !
  call pkzeros2(cldt3hpak,ijpak,   1)
  call pkzeros2(hfl3hpak,ijpak,   1)
  call pkzeros2(hfs3hpak,ijpak,   1)
  call pkzeros2(rof3hpal,ijpak,   1)
  call pkzeros2(pcp3hpak,ijpak,   1)
  call pkzeros2(pcpc3hpak,ijpak,   1)
  call pkzeros2(pcpl3hpak,ijpak,   1)
  call pkzeros2(pcps3hpak,ijpak,   1)
  call pkzeros2(pcpn3hpak,ijpak,   1)
  call pkzeros2(fdl3hpak,ijpak,   1)
  call pkzeros2(fdlc3hpak,ijpak,   1)
  call pkzeros2(flg3hpak,ijpak,   1)
  call pkzeros2(fss3hpak,ijpak,   1)
  call pkzeros2(fssc3hpak,ijpak,   1)
  call pkzeros2(fsd3hpak,ijpak,   1)
  call pkzeros2(fsg3hpak,ijpak,   1)
  call pkzeros2(fsgc3hpak,ijpak,   1)

  call pkzeros2(olr3hpak,ijpak,    1)
  call pkzeros2(fsr3hpak,ijpak,    1)
  call pkzeros2(olrc3hpak,ijpak,   1)
  call pkzeros2(fsrc3hpak,ijpak,   1)
  call pkzeros2(fso3hpak,ijpak,    1)
  call pkzeros2(pwat3hpak,ijpak,   1)
  call pkzeros2(clwt3hpak,ijpak,   1)
  call pkzeros2(cict3hpak,ijpak,   1)
  call pkzeros2(pmsl3hpak,ijpak,   1)
  call pkzeros2(st3hpak,ijpak,     1)
  call pkzeros2(su3hpak,ijpak,     1)
  call pkzeros2(sv3hpak,ijpak,     1)

  !
  ! sampled fields
  !
  call putgg(stpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("  ST"),1,gll,wrks)
  call putgg(supal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("  SU"),1,gll,wrks)
  call putgg(svpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("  SV"),1,gll,wrks)
  call putgg(swpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" SWI"),1,gll,wrks)
  call putgg(srhpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("SRHI"),1,gll,wrks)
  call putgg(pcppal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCPI"),1,gll,wrks)
  call putgg(pcpnpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCNI"),1,gll,wrks)
  call putgg(pcpcpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PCCI"),1,gll,wrks)
  call putgg(qfsipal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("QFSI"),1,gll,wrks)
  call putgg(qfnpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("QFNI"),1,gll,wrks)
  call putgg(qfvfpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("QFVI"),1,gll,wrks)
  call putgg(ufsinstpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("UFSI"),1,gll,wrks)
  call putgg(vfsinstpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("VFSI"),1,gll,wrks)
  call putgg(hflipal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("HFLI"),1,gll,wrks)
  call putgg(hfsipal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("HFSI"),1,gll,wrks)
  call putgg(fdlpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FDLI"),1,gll,wrks)
  call putgg(flgpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FLGI"),1,gll,wrks)
  call putgg(fsspal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSSI"),1,gll,wrks)
  call putgg(fsgpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSGI"),1,gll,wrks)
  call putgg(fsscpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSCI"),1,gll,wrks)
  call putgg(fsgcpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FGCI"),1,gll,wrks)
  call putgg(fdlcpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FLCI"),1,gll,wrks)
  call putgg(fsopal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSOI"),1,gll,wrks)
  call putgg(fsrpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FSRI"),1,gll,wrks)
  call putgg(olrpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("OLRI"),1,gll,wrks)
  call putgg(olrcpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("OLCI"),1,gll,wrks)
  call putgg(fsrcpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("FRCI"),1,gll,wrks)
  call putgg(pwatipal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PWTI"),1,gll,wrks)
  call putgg(cldtpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CLTI"),1,gll,wrks)
  call putgg(clwtpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CLWI"),1,gll,wrks)
  call putgg(cictpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CICI"),1,gll,wrks)
  call putgg(baltpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("BLTI"),1,gll,wrks)
  call putgg(cdcbpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CDCI"),1,gll,wrks)
  call putgg(cscbpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("CSCI"),1,gll,wrks)
  call putgg(pmslipal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8("PSMI"),1,gll,wrks)
  call putgg(gtpal,lon1,ilat,khem,npgg,k,nu3hr, &
                    nc4to8(" GTI"),1,gll,wrks)

#if defined (agcm_ctem)
         call putgg(cfgppal,lon1,ilat,khem,npgg,k,nu3hr, &
                 nc4to8("CFGP"),1,gll,wrks)
         call putgg(cfrvpal,lon1,ilat,khem,npgg,k,nu3hr, &
                 nc4to8("CFRV"),1,gll,wrks)
         call putgg(cfrhpal,lon1,ilat,khem,npgg,k,nu3hr, &
                 nc4to8("CFRH"),1,gll,wrks)
         call putgg(cfrdpal,lon1,ilat,khem,npgg,k,nu3hr, &
                 nc4to8("CFRD"),1,gll,wrks)
#endif

end if ! ls3hr
!
if (lsbeg) then
  !
  !        * save i/o grids on file nupr every isbeg steps, if requested.
  !
  k=kount
  call putgg(eco2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ECO2") &
                    ,1,gll,wrks)
  call putgg(fcoopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FCOO") &
                    ,1,gll,wrks)
  call putgg(qfslpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("QFSL") &
                    ,1,gll,wrks)
  call putgg(qfsopal,lon1,ilat,khem,npgg,k,nupr,nc4to8("QFSO") &
                    ,1,gll,wrks)
  call putgg(begkpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("BEGK") &
                    ,1,gll,wrks)
  call putgg(beglpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("BEGL") &
                    ,1,gll,wrks)
  call putgg(bwgkpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("BWGK") &
                    ,1,gll,wrks)
  call putgg(bwglpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("BWGL") &
                    ,1,gll,wrks)
  call pkzeros2(qfslpal,ijpak,   1)
  !        call pkzeros2(qfsopal,ijpak,   1)    ! zeroed out after normalizing bwgo
  call pkzeros2(begkpal,ijpak,   1)
  call pkzeros2(beglpal,ijpak,   1)
  !        call pkzeros2(bwgkpal,ijpak,   1)    ! zeroed out after normalizing bwgo
  !        call pkzeros2(bwglpal,ijpak,   1)    ! zeroed out after normalizing bwgo
  !
  do n=1,ntrac
    if (modl_tracers(n)%srf/=0) then
      call name2(nam,'XS',n)
      call putgg(xsfxpak(1,n),lon1,ilat,khem,npgg,k,nupr,nam, &
                        1,gll,wrks)
    end if
  end do
end if
if (lsbeg.or.lsgg) then
  !
  !        * save sea-ice and tiled fields on file nupr every (isbeg.or.isgg) steps.
  !
  k = kount
  call putgg(sicnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SICN") &
                    ,1,gll,wrks)
  call putgg(sicpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" SIC") &
                    ,1,gll,wrks)
  !
  do m=1,im
    call putgg(farepat(1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("FARE"),m,gll,wrks)
  end do

  do m=1,im
    call putgg(gtpat  (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" GTT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(snopat (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SNOT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(anpat  (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" ANT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(fnpat  (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" FNT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(tnpat  (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" TNT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(rhonpat(1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RHNT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(bcsnpat(1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("BCNT"),m,gll,wrks)
  end do
  do m=1,im
    call putgg(refpat (1,m),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("REFT"),m,gll,wrks)
  end do
end if
if (lsgg) then
  !
  !        * save i/o grids on file nupr every isgg steps, if requested.
  !
  k=kount
  call putgg(tbaspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("TBAS") &
                    ,1,gll,wrks)
  call putgg(gtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  GT") &
                    ,1,gll,wrks)
  call putgg(gtapak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" GTA") &
                    ,1,gll,wrks)
  call putgg(snopak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" SNO") &
                    ,1,gll,wrks)
  call putgg(wsnopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WSNO") &
                    ,1,gll,wrks)
  call putgg(zpndpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ZPND") &
                    ,1,gll,wrks)
  call putgg(refpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" REF") &
                    ,1,gll,wrks)
  call putgg(bcsnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("BCSN") &
                    ,1,gll,wrks)
  call putgg(depbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DEPB") &
                    ,1,gll,wrks)
  call putgg(anpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  AN") &
                    ,1,gll,wrks)
  call putgg(rhonpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("RHON") &
                    ,1,gll,wrks)
  call putgg(tvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  TV") &
                    ,1,gll,wrks)
  call putgg(tnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  TN") &
                    ,1,gll,wrks)
  call putgg(ttpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  TT") &
                    ,1,gll,wrks)
  call putgg(wvlpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" WVL") &
                    ,1,gll,wrks)
  call putgg(wvfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" WVF") &
                    ,1,gll,wrks)
  call putgg(mvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  MV") &
                    ,1,gll,wrks)
  call putgg(tfxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" TFX") &
                    ,1,gll,wrks)
  call putgg(qfxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFX") &
                    ,1,gll,wrks)
  call putgg(chfxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CHFX") &
                    ,1,gll,wrks)
  call putgg(cqfxpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CQFX") &
                    ,1,gll,wrks)
  call putgg(cbmfpal,lon1,ilat,khem,npgg,k,nupr,nc4to8("CBML") &
                    ,1,gll,wrks)
  call putgg(pbltpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PBLT") &
                    ,1,gll,wrks)
  call putgg(tcvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" TCV") &
                    ,1,gll,wrks)
  call putgg(pblhpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PBLH") &
                    ,1,gll,wrks)
#if (defined(pla) && defined(pam))
#if defined (xtrapla2)
          if (isdiag > 1) then
           do isf=1,isdiag
            call name2(nam,'SI',isf)
            call putgg(sdvlpak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
           end do
          end if
#endif
#endif
#if defined (aodpth)
!         call putgg(odstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODST")
!     1                       ,1,gll,wrks)
!         call putgg(abstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABST")
!     1                       ,1,gll,wrks)
!         call putgg(odsvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODSV")
!     1                       ,1,gll,wrks)
!         call putgg(ofstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFST")
!     1                       ,1,gll,wrks)
#endif
  !        do l=1,nbs
  !           call putgg(salbpal(1,l),lon1,ilat,khem,npgg,k,nupr,
  !    1                                nc4to8("SALB"),l,gll,wrks)
  !        end do

  do l=1,ignd
    call putgg(tgpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8("  TG"),lg(l),gll,wrks)
    call putgg(wglpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" WGL"),lg(l),gll,wrks)
    call putgg(wgfpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                       nc4to8(" WGF"),lg(l),gll,wrks)
  end do
  !
#if defined xtrals
         do l=1,ilev
            call putgg(rainpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("RAIN"),lh(l),gll,wrks)
            call putgg(snowpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("SNOW"),lh(l),gll,wrks)
         end do
#endif
  !
#if (defined(pla) && defined(pam))
#if defined (xtraplafrc)
          do l=1,ilev
             call putgg(amldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("AMLD"),lh(l),gll,wrks)
             call putgg(reampak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("REAM"),lh(l),gll,wrks)
             call putgg(veampak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("VEAM"),lh(l),gll,wrks)
             call putgg(fr1pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8(" FR1"),lh(l),gll,wrks)
             call putgg(fr2pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8(" FR2"),lh(l),gll,wrks)
             call putgg(ssldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("SSLD"),lh(l),gll,wrks)
             call putgg(resspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("RESS"),lh(l),gll,wrks)
             call putgg(vesspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("VESS"),lh(l),gll,wrks)
             call putgg(dsldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("DSLD"),lh(l),gll,wrks)
             call putgg(redspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("REDS"),lh(l),gll,wrks)
             call putgg(vedspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("VEDS"),lh(l),gll,wrks)
             call putgg(bcldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("BCLD"),lh(l),gll,wrks)
             call putgg(rebcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("REBC"),lh(l),gll,wrks)
             call putgg(vebcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("VEBC"),lh(l),gll,wrks)
             call putgg(ocldpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("OCLD"),lh(l),gll,wrks)
             call putgg(reocpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("REOC"),lh(l),gll,wrks)
             call putgg(veocpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("VEOC"),lh(l),gll,wrks)
             call putgg(bcicpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("BCIC"),lh(l),gll,wrks)
          end do
#endif
#if (defined(xtraplafrc) || defined(xtrapla1))
          do l=1,ilev
             call putgg(zcdnpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ZCDN"),lh(l),gll,wrks)
          end do
#endif
#endif
  !
  !     * save sampled burdens
  !
  do n=1,ntrac
    call name2(nam,'VS',n)
    call putgg(xtvipas(1,n),lon1,ilat,khem,npgg,k,nupr, &
                       nam,1,gll,wrks)
  end do
end if

!     * save high-frequency fields on file nupr every ishf steps.

if (lshf) then
  k=kount
  call putgg(pchfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PCHF") &
                     ,1,gll,wrks)
  call putgg(plhfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PLHF") &
                     ,1,gll,wrks)
  call putgg(pshfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PSHF") &
                     ,1,gll,wrks)
  call pkzeros2(pchfpak,ijpak,   1)
  call pkzeros2(plhfpak,ijpak,   1)
  call pkzeros2(pshfpak,ijpak,   1)
end if

!     * save monthly-saved fields on file nupr every ismon steps.

if (iday==lday .and. nsecs==0) then
  k=kount
  call putgg(pwatpam,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PWM") &
                     ,1,gll,wrks)
  call putgg(qtphpam,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QHM") &
                     ,1,gll,wrks)
  call putgg(qtpfpam,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFM") &
                     ,1,gll,wrks)
  call putgg(qtptpam,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QTM") &
                     ,1,gll,wrks)
  call pkzeros2(pwatpam,ijpak,   1)
  call pkzeros2(qtphpam,ijpak,   1)
  call pkzeros2(qtpfpam,ijpak,   1)
  call pkzeros2(qtptpam,ijpak,   1)
  !
  do n=1,ntrac
    call name2(nam,'VM',n)
    call putgg(xtvipam(1,n),lon1,ilat,khem,npgg,k,nupr, &
                       nam,1,gll,wrks)
    call name2(nam,'VH',n)
    call putgg(xtphpam(1,n),lon1,ilat,khem,npgg,k,nupr, &
                       nam,1,gll,wrks)
    call name2(nam,'VF',n)
    call putgg(xtpfpam(1,n),lon1,ilat,khem,npgg,k,nupr, &
                       nam,1,gll,wrks)
    call name2(nam,'VT',n)
    call putgg(xtptpam(1,n),lon1,ilat,khem,npgg,k,nupr, &
                       nam,1,gll,wrks)
    call pkzeros2(xtvipam(1,n),ijpak,   1)
    call pkzeros2(xtphpam(1,n),ijpak,   1)
    call pkzeros2(xtpfpam(1,n),ijpak,   1)
    call pkzeros2(xtptpam(1,n),ijpak,   1)
  end do
end if
!
!     * save accumulated fields on file nupr every israd steps, if
!     * requested.
!
if (lsrad) then
  k = kount
  call putgg(drpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  DR") &
                     ,1,gll,wrks)
  call putgg(fsopak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSO") &
                     ,1,gll,wrks)
  call putgg(fsgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSG") &
                     ,1,gll,wrks)
  call putgg(fsdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSD") &
                     ,1,gll,wrks)
  call putgg(fsvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSV") &
                     ,1,gll,wrks)
  call putgg(fsspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSS") &
                     ,1,gll,wrks)
  call putgg(fsdcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSDC") &
                     ,1,gll,wrks)
  call putgg(fsscpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSSC") &
                     ,1,gll,wrks)
  call putgg(fdlcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FDLC") &
                     ,1,gll,wrks)
  call putgg(olrpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" OLR") &
                     ,1,gll,wrks)
  call putgg(olrcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OLRC") &
                     ,1,gll,wrks)
  call putgg(flampak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLAM") &
                     ,1,gll,wrks)
  call putgg(flanpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLAN") &
                     ,1,gll,wrks)
  call putgg(fdlpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FDL") &
                     ,1,gll,wrks)
  call putgg(flgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FLG") &
                     ,1,gll,wrks)
  call putgg(fslopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSLO") &
                     ,1,gll,wrks)
  call putgg(cldtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CLDT") &
                     ,1,gll,wrks)
  call putgg(cldopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CLDO") &
                     ,1,gll,wrks)
  call putgg(cldapak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CLDA") &
                     ,1,gll,wrks)
  call putgg(fsrpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FSR") &
                     ,1,gll,wrks)
  call putgg(fsrcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSRC") &
                     ,1,gll,wrks)
  call putgg(fsampak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSAM") &
                     ,1,gll,wrks)
  call putgg(fsanpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSAN") &
                     ,1,gll,wrks)
  call putgg(fsgcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSGC") &
                     ,1,gll,wrks)
  call putgg(flgcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLGC") &
                     ,1,gll,wrks)
  call putgg(pwatpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PWAT") &
                     ,1,gll,wrks)
  call putgg(fnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  FN") &
                     ,1,gll,wrks)
  call putgg(fnlapak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FNLA") &
                     ,1,gll,wrks)
  call putgg(gsnopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("GSNO") &
                     ,1,gll,wrks)
  call putgg(smltpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SMLT") &
                     ,1,gll,wrks)
  call putgg(clwtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CLWT") &
                     ,1,gll,wrks)
  call putgg(cictpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CICT") &
                     ,1,gll,wrks)
  call putgg(hflpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" HFL") &
                     ,1,gll,wrks)
  call putgg(hfspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" HFS") &
                     ,1,gll,wrks)
  call putgg(pcppak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PCP") &
                     ,1,gll,wrks)
  call putgg(pcpcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PCPC") &
                     , 1,gll,wrks)
  call putgg(pcpspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PCPS") &
                     ,1,gll,wrks)
  call putgg(pivfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PIVF") &
                     , 1,gll,wrks)
  call putgg(pcpnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PCPN") &
                     ,1,gll,wrks)
  call putgg(pivlpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("PIVL") &
                     , 1,gll,wrks)
  call putgg(pinpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PIN") &
                     , 1,gll,wrks)
  call putgg(pigpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PIG") &
                     , 1,gll,wrks)
  call putgg(qfspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFS") &
                     ,1,gll,wrks)
  call putgg(begpal,lon1,ilat,khem,npgg,k,nupr,nc4to8(" BEG") &
                     ,1,gll,wrks)
  call putgg(ufspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" UFS") &
                     ,1,gll,wrks)
  call putgg(vfspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" VFS") &
                     ,1,gll,wrks)
  call putgg(stpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  ST") &
                     , 1,gll,wrks)
  call putgg(sqpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  SQ") &
                     , 1,gll,wrks)
  call putgg(supak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  SU") &
                     , 1,gll,wrks)
  call putgg(svpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  SV") &
                     , 1,gll,wrks)
  call putgg(srhpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" SRH") &
                     , 1,gll,wrks)
  call putgg(swapak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" SWA") &
                     , 1,gll,wrks)
  call putgg(fsgvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSGV") &
                     , 1,gll,wrks)
  call putgg(fsggpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSGG") &
                     , 1,gll,wrks)
  call putgg(flgvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLGV") &
                     , 1,gll,wrks)
  call putgg(flggpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLGG") &
                     , 1,gll,wrks)
  call putgg(hfsvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFSV") &
                     , 1,gll,wrks)
  call putgg(hfsgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFSG") &
                     , 1,gll,wrks)
  call putgg(hfsnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFSN") &
                     , 1,gll,wrks)
  call putgg(hmfvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HMFV") &
                     , 1,gll,wrks)
  call putgg(hmfnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HMFN") &
                     , 1,gll,wrks)
  call putgg(hfcvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFCV") &
                     , 1,gll,wrks)
  call putgg(hfcnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFCN") &
                     , 1,gll,wrks)
  call putgg(qfgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFG") &
                     , 1,gll,wrks)
  call putgg(qfnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFN") &
                     , 1,gll,wrks)
  call putgg(qfvlpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("QFVL") &
                     , 1,gll,wrks)
  call putgg(qfvfpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("QFVF") &
                     , 1,gll,wrks)
  call putgg(fsgnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSGN") &
                     , 1,gll,wrks)
  call putgg(flgnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLGN") &
                     , 1,gll,wrks)
  call putgg(rofpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" ROF") &
                     , 1,gll,wrks)
  call putgg(rofvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROFV") &
                     , 1,gll,wrks)
  call putgg(rofnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROFN") &
                     , 1,gll,wrks)
  call putgg(rofopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROFO") &
                     , 1,gll,wrks)
  call putgg(rovgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROVG") &
                     , 1,gll,wrks)
  call putgg(wtrvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WTRV") &
                     , 1,gll,wrks)
  call putgg(wtrnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WTRN") &
                     , 1,gll,wrks)
  call putgg(wtrgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WTRG") &
                     , 1,gll,wrks)
  call putgg(hflvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFLV") &
                     , 1,gll,wrks)
  call putgg(hflnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFLN") &
                     , 1,gll,wrks)
  call putgg(hflgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFLG") &
                     , 1,gll,wrks)
  call putgg(znpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  ZN") &
                     , 1,gll,wrks)
  call putgg(psapak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PSA") &
                     , 1,gll,wrks)
  call putgg(fvnpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FVN") &
                     , 1,gll,wrks)
  call putgg(fvgpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FVG") &
                     , 1,gll,wrks)
  call putgg(skygpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SKYG") &
                     , 1,gll,wrks)
  call putgg(skynpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SKYN") &
                     , 1,gll,wrks)
  call putgg(gapak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  GA") &
                    ,1,gll,wrks)
  call putgg(hblpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" HBL") &
                    ,1,gll,wrks)
  call putgg(ilmopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ILMO") &
                    ,1,gll,wrks)
  call putgg(petpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" PET") &
                    ,1,gll,wrks)
  call putgg(rofbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROFB") &
                    ,1,gll,wrks)
  call putgg(rofspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ROFS") &
                    ,1,gll,wrks)
  call putgg(uepak,lon1,ilat,khem,npgg,k,nupr,nc4to8("  UE") &
                    ,1,gll,wrks)
  call putgg(wtabpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WTAB") &
                    ,1,gll,wrks)

  do l=1,ignd
    call putgg(hfcgpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                         nc4to8("HFCG"),lg(l),gll,wrks)
    call putgg(gflxpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                         nc4to8("GFLX"),lg(l),gll,wrks)
    call putgg(qfvgpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                         nc4to8("QFVG"),lg(l),gll,wrks)
    call putgg(hmfgpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                         nc4to8("HMFG"),lg(l),gll,wrks)
  end do ! loop 530
  !
  call pkzeros2(drpak,ijpak,   1)
  call pkzeros2(fsopak,ijpak,   1)
  call pkzeros2(fsgpak,ijpak,   1)
  call pkzeros2(fsdpak,ijpak,   1)
  call pkzeros2(fsvpak,ijpak,   1)
  call pkzeros2(fsspak,ijpak,   1)
  call pkzeros2(fsdcpak,ijpak,   1)
  call pkzeros2(fsscpak,ijpak,   1)
  call pkzeros2(fdlcpak,ijpak,   1)
  call pkzeros2(olrpak,ijpak,   1)
  call pkzeros2(olrcpak,ijpak,   1)
  call pkzeros2(flampak,ijpak,   1)
  call pkzeros2(flanpak,ijpak,   1)
  call pkzeros2(fdlpak,ijpak,   1)
  call pkzeros2(flgpak,ijpak,   1)
  call pkzeros2(fslopak,ijpak,   1)
  call pkzeros2(cldtpak,ijpak,   1)
  call pkzeros2(cldopak,ijpak,   1)
  call pkzeros2(cldapak,ijpak,   1)
  call pkzeros2(fsrpak,ijpak,   1)
  call pkzeros2(fsrcpak,ijpak,   1)
  call pkzeros2(fsampak,ijpak,   1)
  call pkzeros2(fsanpak,ijpak,   1)
  call pkzeros2(fsgcpak,ijpak,   1)
  call pkzeros2(flgcpak,ijpak,   1)
  call pkzeros2(pwatpak,ijpak,   1)
  call pkzeros2(smltpak,ijpak,   1)
  call pkzeros2(fnpak,ijpak,   1)
  call pkzeros2(fnlapak,ijpak,   1)
  call pkzeros2(gsnopak,ijpak,   1)
  call pkzeros2(clwtpak,ijpak,   1)
  call pkzeros2(cictpak,ijpak,   1)
  call pkzeros2(hflpak,ijpak,   1)
  call pkzeros2(hfspak,ijpak,   1)
  call pkzeros2(qfspak,ijpak,   1)
  call pkzeros2(pcppak,ijpak,   1)
  call pkzeros2(pcpcpak,ijpak,   1)
  call pkzeros2(pcpnpak,ijpak,   1)
  call pkzeros2(pcpspak,ijpak,   1)
  call pkzeros2(pivfpak,ijpak,   1)
  call pkzeros2(pivlpak,ijpak,   1)
  call pkzeros2(pinpak,ijpak,   1)
  call pkzeros2(pigpak,ijpak,   1)
  call pkzeros2(ufspak,ijpak,   1)
  call pkzeros2(vfspak,ijpak,   1)
  call pkzeros2(begpal,ijpak,   1)
  call pkzeros2(stpak,ijpak,   1)
  call pkzeros2(sqpak,ijpak,   1)
  call pkzeros2(supak,ijpak,   1)
  call pkzeros2(svpak,ijpak,   1)
  call pkzeros2(srhpak,ijpak,   1)
  call pkzeros2(swapak,ijpak,   1)
  call pkzeros2(fsgvpak,ijpak,   1)
  call pkzeros2(fsggpak,ijpak,   1)
  call pkzeros2(flgvpak,ijpak,   1)
  call pkzeros2(flggpak,ijpak,   1)
  call pkzeros2(hfsvpak,ijpak,   1)
  call pkzeros2(hfsgpak,ijpak,   1)
  call pkzeros2(hfsnpak,ijpak,   1)
  call pkzeros2(hmfvpak,ijpak,   1)
  call pkzeros2(hmfnpak,ijpak,   1)
  call pkzeros2(hfcgpak,ijpak,ignd)
  call pkzeros2(hfcvpak,ijpak,   1)
  call pkzeros2(hfcnpak,ijpak,   1)
  call pkzeros2(qfgpak,ijpak,   1)
  call pkzeros2(qfnpak,ijpak,   1)
  call pkzeros2(qfvgpak,ijpak,ignd)
  call pkzeros2(qfvlpak,ijpak,   1)
  call pkzeros2(qfvfpak,ijpak,   1)
  call pkzeros2(fsgnpak,ijpak,   1)
  call pkzeros2(flgnpak,ijpak,   1)
  call pkzeros2(rofpak,ijpak,   1)
  call pkzeros2(rofvpak,ijpak,   1)
  call pkzeros2(rofnpak,ijpak,   1)
  call pkzeros2(rofopak,ijpak,   1)
  call pkzeros2(rovgpak,ijpak,   1)
  call pkzeros2(wtrvpak,ijpak,   1)
  call pkzeros2(wtrnpak,ijpak,   1)
  call pkzeros2(wtrgpak,ijpak,   1)
  call pkzeros2(hflvpak,ijpak,   1)
  call pkzeros2(hflnpak,ijpak,   1)
  call pkzeros2(hflgpak,ijpak,   1)
  call pkzeros2(znpak,ijpak,   1)
  call pkzeros2(psapak,ijpak,   1)
  call pkzeros2(fvnpak,ijpak,   1)
  call pkzeros2(fvgpak,ijpak,   1)
  call pkzeros2(skygpak,ijpak,   1)
  call pkzeros2(skynpak,ijpak,   1)
  call pkzeros2(rofbpak,ijpak,   1)
  call pkzeros2(rofspak,ijpak,   1)
  call pkzeros2(hmfgpak,ijpak,ignd)
  call pkzeros2(gapak,ijpak,   1)
  call pkzeros2(gflxpak,ijpak,   1)
  call pkzeros2(petpak,ijpak,   1)
  call pkzeros2(uepak,ijpak,   1)
  !
  do n=1,ntrac
    call name2(nam,'XF',n)
    call putgg(xfspak (1,n),lon1,ilat,khem,npgg,k,nupr, &
                         nam,1,gll,wrks)
    call name2(nam,'XK',n)
    call putgg(xsrfpak(1,n),lon1,ilat,khem,npgg,k,nupr, &
                         nam,1,gll,wrks)
    call name2(nam,'VI',n)
    call putgg(xtvipak(1,n),lon1,ilat,khem,npgg,k,nupr, &
                         nam,1,gll,wrks)
    !
    call pkzeros2(xfspak (1,n),ijpak,   1)
    call pkzeros2(xsrfpak(1,n),ijpak,   1)
    call pkzeros2(xtvipak(1,n),ijpak,   1)
  end do ! loop 534
  do l=1,ilev
    call putgg(altipak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8("ALTI"),lh(l),gll,wrks)
    call putgg(tbndpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8("TBND"),lh(l),gll,wrks)
    call putgg(ea55pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8("EA55"),lh(l),gll,wrks)
    call putgg(rkmpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8(" RKM"),lh(l),gll,wrks)
    call putgg(rkhpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8(" RKH"),lh(l),gll,wrks)
    call putgg(rkqpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                        nc4to8(" RKQ"),lh(l),gll,wrks)
  end do

  call pkzeros2(altipak,ijpak,ilev)
  call pkzeros2(tbndpak,ijpak,ilev)
  call pkzeros2(ea55pak,ijpak,ilev)
  call pkzeros2(rkmpak,ijpak,ilev)
  call pkzeros2(rkhpak,ijpak,ilev)
  call pkzeros2(rkqpak,ijpak,ilev)
  !
  !         * general lake fields.
  !
  call putgg(ldmxpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LDMX"),1,gll,wrks)
  call putgg(lrimpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LRIM"),1,gll,wrks)
  call putgg(lrinpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LRIN"),1,gll,wrks)
  call putgg(luimpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LUIM"),1,gll,wrks)
  call putgg(luinpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LUIN"),1,gll,wrks)
  call putgg(lzicpak,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("LZIC"),1,gll,wrks)
#if defined (cslm)
!
!         * sampled fields.
!
          call putgg(delupak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("DELU"),1,gll,wrks)
          call putgg(dtmppak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("DTMP"),1,gll,wrks)
          call putgg(expwpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("EXPW"),1,gll,wrks)
          call putgg(gredpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("GRED"),1,gll,wrks)
          call putgg(rhompak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("RHOM"),1,gll,wrks)
          call putgg(t0lkpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("T0LK"),1,gll,wrks)
          call putgg(tkelpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("TKEL"),1,gll,wrks)
          do l=1,100
           call putgg(tlakpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                   nc4to8("TLAK"),l,gll,wrks)
          end do
!
!         * accumulated fields.
!
          call putgg(flglpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FLGL") &
                         ,1,gll,wrks)
          call putgg(fnlpak, lon1,ilat,khem,npgg,k,nupr,nc4to8(" FNL") &
                         ,1,gll,wrks)
          call putgg(fsglpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FSGL") &
                         ,1,gll,wrks)
          call putgg(hfclpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFCL") &
                         ,1,gll,wrks)
          call putgg(hfllpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFLL") &
                         ,1,gll,wrks)
          call putgg(hfslpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HFSL") &
                         ,1,gll,wrks)
          call putgg(hmflpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("HMFL") &
                         ,1,gll,wrks)
          call putgg(pilpak, lon1,ilat,khem,npgg,k,nupr,nc4to8(" PIL") &
                         ,1,gll,wrks)
          call putgg(qflpak, lon1,ilat,khem,npgg,k,nupr,nc4to8(" QFL") &
                         ,1,gll,wrks)
!
          flglpak=0.
          fnlpak =0.
          fsglpak=0.
          hfclpak=0.
          hfllpak=0.
          hfslpak=0.
          hmflpak=0.
          pilpak =0.
          qflpak =0.
#endif
#if defined (flake)
!
!        * sampled fields.
!
          call putgg(lshppak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LSHP"),1,gll,wrks)
          call putgg(ltavpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LTAV"),1,gll,wrks)
          call putgg(lticpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LTIC"),1,gll,wrks)
          call putgg(ltmxpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LTMX"),1,gll,wrks)
          call putgg(ltsnpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LTSN"),1,gll,wrks)
          call putgg(ltwbpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LTWB"),1,gll,wrks)
          call putgg(lzsnpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("LZSN"),1,gll,wrks)
#endif
#if defined xtraconv
!
          call putgg(capepak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CAPE") &
                          ,1,gll,wrks)
          call putgg(cinhpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CINH") &
                          ,1,gll,wrks)
          call putgg(bcdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" BCD") &
                          ,1,gll,wrks)
          call putgg(bcspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" BCS") &
                          ,1,gll,wrks)
          call putgg(tcdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" TCD") &
                          ,1,gll,wrks)
          call putgg(tcspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" TCS") &
                          ,1,gll,wrks)
          call putgg(cdcbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CDCB") &
                          ,1,gll,wrks)
          call putgg(cscbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("CSCB") &
                          ,1,gll,wrks)
!
          do l=1,ilev
              call putgg(dmcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" DMC"),lh(l),gll,wrks)
              call putgg(smcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" SMC"),lh(l),gll,wrks)
              call putgg(dmcdpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("DMCD"),lh(l),gll,wrks)
              call putgg(dmcupak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("DMCU"),lh(l),gll,wrks)
  end do ! loop 535
!
          call pkzeros2(capepak,ijpak,   1)
          call pkzeros2(cinhpak,ijpak,   1)
          call pkzeros2(bcdpak,ijpak,   1)
          call pkzeros2(bcspak,ijpak,   1)
          call pkzeros2(tcdpak,ijpak,   1)
          call pkzeros2(tcspak,ijpak,   1)
          call pkzeros2(cdcbpak,ijpak,   1)
          call pkzeros2(cscbpak,ijpak,   1)
          call pkzeros2(dmcpak,ijpak,ilev)
          call pkzeros2(smcpak,ijpak,ilev)
          call pkzeros2(dmcdpak,ijpak,ilev)
          call pkzeros2(dmcupak,ijpak,ilev)
#endif
#if defined xtrachem
          call putgg(dd4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DD4") &
                          ,1,gll,wrks)
          call putgg(dox4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DOX4") &
                          ,1,gll,wrks)
          call putgg(doxdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DOXD") &
                          ,1,gll,wrks)
          call putgg(esdpak ,lon1,ilat,khem,npgg,k,nupr,nc4to8(" ESD") &
                          ,1,gll,wrks)
          call putgg(esfspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESFS") &
                          ,1,gll,wrks)
          call putgg(eaispak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EAIS") &
                          ,1,gll,wrks)
          call putgg(estspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESTS") &
                          ,1,gll,wrks)
          call putgg(efispak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EFIS") &
                          ,1,gll,wrks)
          call putgg(esfbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESFB") &
                          ,1,gll,wrks)
          call putgg(eaibpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EAIB") &
                          ,1,gll,wrks)
          call putgg(estbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESTB") &
                          ,1,gll,wrks)
          call putgg(efibpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EFIB") &
                          ,1,gll,wrks)
          call putgg(esfopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESFO") &
                          ,1,gll,wrks)
          call putgg(eaiopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EAIO") &
                          ,1,gll,wrks)
          call putgg(estopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESTO") &
                          ,1,gll,wrks)
          call putgg(efiopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EFIO") &
                          ,1,gll,wrks)
          call putgg(edslpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EDSL") &
                          ,1,gll,wrks)
          call putgg(edsopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EDSO") &
                          ,1,gll,wrks)
          call putgg(esvcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESVC") &
                          ,1,gll,wrks)
          call putgg(esvepak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ESVE") &
                          ,1,gll,wrks)
          call putgg(noxdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("NOXD") &
                          ,1,gll,wrks)
          call putgg(wdd4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDD4") &
                          ,1,gll,wrks)
          call putgg(wdl4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDL4") &
                          ,1,gll,wrks)
          call putgg(wds4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDS4") &
                          ,1,gll,wrks)
!
          call pkzeros2(dd4pak,ijpak,   1)
          call pkzeros2(dox4pak,ijpak,   1)
          call pkzeros2(doxdpak,ijpak,   1)
          call pkzeros2(esdpak, ijpak,   1)
          call pkzeros2(esfspak,ijpak,   1)
          call pkzeros2(eaispak,ijpak,   1)
          call pkzeros2(estspak,ijpak,   1)
          call pkzeros2(efispak,ijpak,   1)
          call pkzeros2(esfbpak,ijpak,   1)
          call pkzeros2(eaibpak,ijpak,   1)
          call pkzeros2(estbpak,ijpak,   1)
          call pkzeros2(efibpak,ijpak,   1)
          call pkzeros2(esfopak,ijpak,   1)
          call pkzeros2(eaiopak,ijpak,   1)
          call pkzeros2(estopak,ijpak,   1)
          call pkzeros2(efiopak,ijpak,   1)
          call pkzeros2(edslpak,ijpak,   1)
          call pkzeros2(edsopak,ijpak,   1)
          call pkzeros2(esvcpak,ijpak,   1)
          call pkzeros2(esvepak,ijpak,   1)
          call pkzeros2(noxdpak,ijpak,   1)
          call pkzeros2(wdd4pak,ijpak,   1)
          call pkzeros2(wdl4pak,ijpak,   1)
          call pkzeros2(wds4pak,ijpak,   1)
#ifndef pla
          call putgg(slo3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SLO3") &
                          ,1,gll,wrks)
          call putgg(slhppak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SLHP") &
                          ,1,gll,wrks)
          call putgg(sso3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SSO3") &
                          ,1,gll,wrks)
          call putgg(sshppak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SSHP") &
                          ,1,gll,wrks)
          call putgg(wds6pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDS6") &
                          ,1,gll,wrks)
          call putgg(sdo3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SDO3") &
                          ,1,gll,wrks)
          call putgg(sdhppak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SDHP") &
                          ,1,gll,wrks)
          call putgg(dd6pak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DD6") &
                          ,1,gll,wrks)
          call putgg(wdl6pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDL6") &
                          ,1,gll,wrks)
          call putgg(wdd6pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDD6") &
                          ,1,gll,wrks)
          call putgg(dddpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DDD") &
                          ,1,gll,wrks)
          call putgg(ddbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DDB") &
                          ,1,gll,wrks)
          call putgg(ddopak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DDO") &
                          ,1,gll,wrks)
          call putgg(ddspak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" DDS") &
                          ,1,gll,wrks)
          call putgg(wdldpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDLD") &
                          ,1,gll,wrks)
          call putgg(wdlbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDLB") &
                          ,1,gll,wrks)
          call putgg(wdlopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDLO") &
                          ,1,gll,wrks)
          call putgg(wdlspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDLS") &
                          ,1,gll,wrks)
          call putgg(wdddpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDDD") &
                          ,1,gll,wrks)
          call putgg(wddbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDDB") &
                          ,1,gll,wrks)
          call putgg(wddopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDDO") &
                          ,1,gll,wrks)
          call putgg(wddspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDDS") &
                          ,1,gll,wrks)
          call putgg(wdsdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDSD") &
                          ,1,gll,wrks)
          call putgg(wdsbpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDSB") &
                          ,1,gll,wrks)
          call putgg(wdsopak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDSO") &
                          ,1,gll,wrks)
          call putgg(wdsspak,lon1,ilat,khem,npgg,k,nupr,nc4to8("WDSS") &
                          ,1,gll,wrks)
!
          do l=1,ilev
              call putgg(phlpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                   nc4to8(" PHL"),lh(l),gll,wrks)
              call putgg(phspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                   nc4to8(" PHS"),lh(l),gll,wrks)
              call putgg(phdpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                   nc4to8(" PHD"),lh(l),gll,wrks)
  end do ! loop 540
!
          call pkzeros2(slo3pak,ijpak,   1)
          call pkzeros2(slhppak,ijpak,   1)
          call pkzeros2(sso3pak,ijpak,   1)
          call pkzeros2(sshppak,ijpak,   1)
          call pkzeros2(wds6pak,ijpak,   1)
          call pkzeros2(sdo3pak,ijpak,   1)
          call pkzeros2(sdhppak,ijpak,   1)
          call pkzeros2(dd6pak,ijpak,   1)
          call pkzeros2(wdl6pak,ijpak,   1)
          call pkzeros2(wdd6pak,ijpak,   1)
          call pkzeros2(dddpak ,ijpak,   1)
          call pkzeros2(ddbpak ,ijpak,   1)
          call pkzeros2(ddopak ,ijpak,   1)
          call pkzeros2(ddspak ,ijpak,   1)
          call pkzeros2(wdldpak,ijpak,   1)
          call pkzeros2(wdlbpak,ijpak,   1)
          call pkzeros2(wdlopak,ijpak,   1)
          call pkzeros2(wdlspak,ijpak,   1)
          call pkzeros2(wdddpak,ijpak,   1)
          call pkzeros2(wddbpak,ijpak,   1)
          call pkzeros2(wddopak,ijpak,   1)
          call pkzeros2(wddspak,ijpak,   1)
          call pkzeros2(wdsdpak,ijpak,   1)
          call pkzeros2(wdsbpak,ijpak,   1)
          call pkzeros2(wdsopak,ijpak,   1)
          call pkzeros2(wdsspak,ijpak,   1)
#endif
#endif
#if defined (xtradust)
          call putgg(duwdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DUWD") &
                          ,1,gll,wrks)
          call putgg(dustpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DUST") &
                          ,1,gll,wrks)
          call putgg(duthpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DUTH") &
                          ,1,gll,wrks)
          call putgg(usmkpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("USMK") &
                          ,1,gll,wrks)
          call putgg(fallpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FALL") &
                          ,1,gll,wrks)
          call putgg(fa10pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("FA10") &
                          ,1,gll,wrks)
          call putgg(fa2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FA2") &
                          ,1,gll,wrks)
          call putgg(fa1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8(" FA1") &
                          ,1,gll,wrks)
          call putgg(gustpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("GUST") &
                          ,1,gll,wrks)
          call putgg(zspdpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ZSPD") &
                          ,1,gll,wrks)
          call putgg(vgfrpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("VGFR") &
                          ,1,gll,wrks)
          call putgg(smfrpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("SMFR") &
                          ,1,gll,wrks)
!
          call pkzeros2(duwdpak,ijpak,   1)
          call pkzeros2(dustpak,ijpak,   1)
          call pkzeros2(duthpak,ijpak,   1)
          call pkzeros2(usmkpak,ijpak,   1)
          call pkzeros2(fallpak,ijpak,   1)
          call pkzeros2(fa10pak,ijpak,   1)
          call pkzeros2(fa2pak,ijpak,   1)
          call pkzeros2(fa1pak,ijpak,   1)
          call pkzeros2(gustpak,ijpak,   1)
          call pkzeros2(zspdpak,ijpak,   1)
          call pkzeros2(vgfrpak,ijpak,   1)
          call pkzeros2(smfrpak,ijpak,   1)
#endif
#if (defined(pla) && defined(pam))
#if defined (xtrapla1)
          do l=1,ilev
              call putgg(sncnpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SNCN"),lh(l),gll,wrks)
              call putgg(ssunpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SSUN"),lh(l),gll,wrks)
              call putgg(scndpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SCND"),lh(l),gll,wrks)
              call putgg(sgsppak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SGSP"),lh(l),gll,wrks)
              call putgg(ccnpak(1,l) ,lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" CCN"),lh(l),gll,wrks)
              call putgg(cc02pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CC02"),lh(l),gll,wrks)
              call putgg(cc04pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CC04"),lh(l),gll,wrks)
              call putgg(cc08pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CC08"),lh(l),gll,wrks)
              call putgg(cc16pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CC16"),lh(l),gll,wrks)
              call putgg(ccnepak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CCNE"),lh(l),gll,wrks)
              call putgg(acaspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("ACAS"),lh(l),gll,wrks)
              call putgg(acoapak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("ACOA"),lh(l),gll,wrks)
              call putgg(acbcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("ACBC"),lh(l),gll,wrks)
              call putgg(acsspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("ACSS"),lh(l),gll,wrks)
              call putgg(acmdpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("ACMD"),lh(l),gll,wrks)
              call putgg(ntpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" N00"),lh(l),gll,wrks)
              call putgg(n20pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" N20"),lh(l),gll,wrks)
              call putgg(n50pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" N50"),lh(l),gll,wrks)
              call putgg(n100pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("N100"),lh(l),gll,wrks)
              call putgg(n200pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("N200"),lh(l),gll,wrks)
              call putgg(wtpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" W00"),lh(l),gll,wrks)
              call putgg(w20pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" W20"),lh(l),gll,wrks)
              call putgg(w50pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" W50"),lh(l),gll,wrks)
              call putgg(w100pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("W100"),lh(l),gll,wrks)
              call putgg(w200pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("W200"),lh(l),gll,wrks)
              call putgg(rcripak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RCRI"),lh(l),gll,wrks)
              call putgg(supspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SUPS"),lh(l),gll,wrks)
              call putgg(henrpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("HENR"),lh(l),gll,wrks)
              call putgg(o3frpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("O3FR"),lh(l),gll,wrks)
              call putgg(h2o2frpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("HPFR"),lh(l),gll,wrks)
              call putgg(wparpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("WPAR"),lh(l),gll,wrks)
              call putgg(pm25pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("PM25"),lh(l),gll,wrks)
              call putgg(pm10pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("PM10"),lh(l),gll,wrks)
              call putgg(dm25pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("DM25"),lh(l),gll,wrks)
              call putgg(dm10pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("DM10"),lh(l),gll,wrks)
          end do
          call putgg(vncnpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VNCN"),1,gll,wrks)
          call putgg(vasnpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASN"),1,gll,wrks)
          call putgg(vscdpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSCD"),1,gll,wrks)
          call putgg(vgsppak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VGSP"),1,gll,wrks)
          call putgg(voaepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VOAE"),1,gll,wrks)
          call putgg(vbcepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VBCE"),1,gll,wrks)
          call putgg(vasepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASE"),1,gll,wrks)
          call putgg(vmdepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMDE"),1,gll,wrks)
          call putgg(vssepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSSE"),1,gll,wrks)
          call putgg(voawpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VOAW"),1,gll,wrks)
          call putgg(vbcwpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VBCW"),1,gll,wrks)
          call putgg(vaswpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASW"),1,gll,wrks)
          call putgg(vmdwpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMDW"),1,gll,wrks)
          call putgg(vsswpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSSW"),1,gll,wrks)
          call putgg(voadpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VOAD"),1,gll,wrks)
          call putgg(vbcdpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VBCD"),1,gll,wrks)
          call putgg(vasdpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASD"),1,gll,wrks)
          call putgg(vmddpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMDD"),1,gll,wrks)
          call putgg(vssdpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSSD"),1,gll,wrks)
          call putgg(voagpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VOAG"),1,gll,wrks)
          call putgg(vbcgpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VBCG"),1,gll,wrks)
          call putgg(vasgpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASG"),1,gll,wrks)
          call putgg(vmdgpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMDG"),1,gll,wrks)
          call putgg(vssgpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSSG"),1,gll,wrks)
          call putgg(voacpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VOAC"),1,gll,wrks)
          call putgg(vbccpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VBCC"),1,gll,wrks)
          call putgg(vascpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASC"),1,gll,wrks)
          call putgg(vmdcpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMDC"),1,gll,wrks)
          call putgg(vsscpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSSC"),1,gll,wrks)
          call putgg(vasipak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VASI"),1,gll,wrks)
          call putgg(vas1pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VAS1"),1,gll,wrks)
          call putgg(vas2pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VAS2"),1,gll,wrks)
          call putgg(vas3pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VAS3"),1,gll,wrks)
          call putgg(vccnpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VCCN"),1,gll,wrks)
          call putgg(vcnepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VCNE"),1,gll,wrks)
!
!          call pkzeros2(sncnpak,ijpak,ilev)
!          call pkzeros2(ssunpak,ijpak,ilev)
!          call pkzeros2(scndpak,ijpak,ilev)
!          call pkzeros2(sgsppak,ijpak,ilev)
          call pkzeros2(cc02pak,ijpak,ilev)
          call pkzeros2(cc04pak,ijpak,ilev)
          call pkzeros2(cc08pak,ijpak,ilev)
          call pkzeros2(cc16pak,ijpak,ilev)
          call pkzeros2(acaspak,ijpak,ilev)
          call pkzeros2(acoapak,ijpak,ilev)
          call pkzeros2(acbcpak,ijpak,ilev)
          call pkzeros2(acsspak,ijpak,ilev)
          call pkzeros2(acmdpak,ijpak,ilev)
          call pkzeros2(ntpak,ijpak,ilev)
          call pkzeros2(n20pak,ijpak,ilev)
          call pkzeros2(n50pak,ijpak,ilev)
          call pkzeros2(n100pak,ijpak,ilev)
          call pkzeros2(n200pak,ijpak,ilev)
          call pkzeros2(wtpak,ijpak,ilev)
          call pkzeros2(w20pak,ijpak,ilev)
          call pkzeros2(w50pak,ijpak,ilev)
          call pkzeros2(w100pak,ijpak,ilev)
          call pkzeros2(w200pak,ijpak,ilev)
          call pkzeros2(pm25pak,ijpak,ilev)
          call pkzeros2(pm10pak,ijpak,ilev)
          call pkzeros2(dm25pak,ijpak,ilev)
          call pkzeros2(dm10pak,ijpak,ilev)
          call pkzeros2(vncnpak,ijpak,   1)
          call pkzeros2(vasnpak,ijpak,   1)
          call pkzeros2(vscdpak,ijpak,   1)
          call pkzeros2(vgsppak,ijpak,   1)
          call pkzeros2(voaepak,ijpak,   1)
          call pkzeros2(vbcepak,ijpak,   1)
          call pkzeros2(vasepak,ijpak,   1)
          call pkzeros2(vmdepak,ijpak,   1)
          call pkzeros2(vssepak,ijpak,   1)
          call pkzeros2(voawpak,ijpak,   1)
          call pkzeros2(vbcwpak,ijpak,   1)
          call pkzeros2(vaswpak,ijpak,   1)
          call pkzeros2(vmdwpak,ijpak,   1)
          call pkzeros2(vsswpak,ijpak,   1)
          call pkzeros2(voadpak,ijpak,   1)
          call pkzeros2(vbcdpak,ijpak,   1)
          call pkzeros2(vasdpak,ijpak,   1)
          call pkzeros2(vmddpak,ijpak,   1)
          call pkzeros2(vssdpak,ijpak,   1)
          call pkzeros2(voagpak,ijpak,   1)
          call pkzeros2(vbcgpak,ijpak,   1)
          call pkzeros2(vasgpak,ijpak,   1)
          call pkzeros2(vmdgpak,ijpak,   1)
          call pkzeros2(vssgpak,ijpak,   1)
          call pkzeros2(voacpak,ijpak,   1)
          call pkzeros2(vbccpak,ijpak,   1)
          call pkzeros2(vascpak,ijpak,   1)
          call pkzeros2(vmdcpak,ijpak,   1)
          call pkzeros2(vsscpak,ijpak,   1)
          call pkzeros2(vasipak,ijpak,   1)
          call pkzeros2(vas1pak,ijpak,   1)
          call pkzeros2(vas2pak,ijpak,   1)
          call pkzeros2(vas3pak,ijpak,   1)
!          call pkzeros2(vccnpak,ijpak,   1)
!          call pkzeros2(vcnepak,ijpak,   1)
#endif
#if defined (xtrapla2)
          do l=1,ilev
              call putgg(cornpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CORN"),lh(l),gll,wrks)
              call putgg(cormpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CORM"),lh(l),gll,wrks)
              call putgg(rsn1pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSN1"),lh(l),gll,wrks)
              call putgg(rsm1pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSM1"),lh(l),gll,wrks)
              call putgg(rsn2pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSN2"),lh(l),gll,wrks)
              call putgg(rsm2pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSM2"),lh(l),gll,wrks)
              call putgg(rsn3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSN3"),lh(l),gll,wrks)
              call putgg(rsm3pak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RSM3"),lh(l),gll,wrks)
              if (isdnum > 1) then
               do isf=1,isdnum
                call name2(nam,'SN',isf)
                call putgg(sdnupak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'SM',isf)
                call putgg(sdmapak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'SA',isf)
                call putgg(sdacpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'SC',isf)
                call putgg(sdcopak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'N1',isf)
                call putgg(sssnpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'N2',isf)
                call putgg(smdnpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'N3',isf)
                call putgg(sianpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M1',isf)
                call putgg(sssmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M2',isf)
                call putgg(smdmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M3',isf)
                call putgg(sewmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M4',isf)
                call putgg(ssumpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M5',isf)
                call putgg(socmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M6',isf)
                call putgg(sbcmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
                call name2(nam,'M7',isf)
                call putgg(siwmpak(1,l,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,lh(l),gll,wrks)
               end do
              end if
          end do
          call putgg(vrn1pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRN1"),1,gll,wrks)
          call putgg(vrm1pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRM1"),1,gll,wrks)
          call putgg(vrn2pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRN2"),1,gll,wrks)
          call putgg(vrm2pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRM2"),1,gll,wrks)
          call putgg(vrn3pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRN3"),1,gll,wrks)
          call putgg(vrm3pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRM3"),1,gll,wrks)
          call putgg(defapak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DEFA") &
                          ,1,gll,wrks)
          call putgg(defcpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("DEFC") &
                          ,1,gll,wrks)
          do isf=1,isdust
            call name2(nam,'MA',isf)
            call putgg(dmacpak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
            call name2(nam,'MC',isf)
            call putgg(dmcopak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
            call name2(nam,'NA',isf)
            call putgg(dnacpak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
            call name2(nam,'NC',isf)
            call putgg(dncopak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
          end do
          if (isdiag > 1) then
           do isf=1,isdiag
            call name2(nam,'EF',isf)
            call putgg(defxpak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
            call name2(nam,'EN',isf)
            call putgg(defnpak(1,isf),lon1,ilat,khem,npgg,k, &
                                          nupr,nam,1,gll,wrks)
           end do
          end if
!
          call pkzeros2(cornpak,ijpak,ilev)
          call pkzeros2(cormpak,ijpak,ilev)
          call pkzeros2(rsn1pak,ijpak,ilev)
          call pkzeros2(rsm1pak,ijpak,ilev)
          call pkzeros2(rsn2pak,ijpak,ilev)
          call pkzeros2(rsm2pak,ijpak,ilev)
          call pkzeros2(rsn3pak,ijpak,ilev)
          call pkzeros2(rsm3pak,ijpak,ilev)
          do isf=1,isdnum
            call pkzeros2(sdnupak(1,1,isf),ijpak,ilev)
            call pkzeros2(sdmapak(1,1,isf),ijpak,ilev)
            call pkzeros2(sdacpak(1,1,isf),ijpak,ilev)
            call pkzeros2(sdcopak(1,1,isf),ijpak,ilev)
            call pkzeros2(sssnpak(1,1,isf),ijpak,ilev)
            call pkzeros2(smdnpak(1,1,isf),ijpak,ilev)
            call pkzeros2(sianpak(1,1,isf),ijpak,ilev)
            call pkzeros2(sssmpak(1,1,isf),ijpak,ilev)
            call pkzeros2(smdmpak(1,1,isf),ijpak,ilev)
            call pkzeros2(sewmpak(1,1,isf),ijpak,ilev)
            call pkzeros2(ssumpak(1,1,isf),ijpak,ilev)
            call pkzeros2(socmpak(1,1,isf),ijpak,ilev)
            call pkzeros2(sbcmpak(1,1,isf),ijpak,ilev)
            call pkzeros2(siwmpak(1,1,isf),ijpak,ilev)
          end do
!          do isf=1,isdiag
!            call pkzeros2(sdvlpak(1,isf),ijpak,   1)
!          end do
          call pkzeros2(vrn1pak,ijpak,   1)
          call pkzeros2(vrm1pak,ijpak,   1)
          call pkzeros2(vrn2pak,ijpak,   1)
          call pkzeros2(vrm2pak,ijpak,   1)
          call pkzeros2(vrn3pak,ijpak,   1)
          call pkzeros2(vrm3pak,ijpak,   1)
          call pkzeros2(defapak,ijpak,   1)
          call pkzeros2(defcpak,ijpak,   1)
          do isf=1,isdust
            call pkzeros2(dmacpak(1,isf),ijpak,   1)
            call pkzeros2(dmcopak(1,isf),ijpak,   1)
            call pkzeros2(dnacpak(1,isf),ijpak,   1)
            call pkzeros2(dncopak(1,isf),ijpak,   1)
          end do
          do isf=1,isdiag
            call pkzeros2(defxpak(1,isf),ijpak,   1)
            call pkzeros2(defnpak(1,isf),ijpak,   1)
          end do
#endif
#endif
#if defined xtrals
!
          do l=1,ilev
              call putgg(aggpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" AGG"),lh(l),gll,wrks)
              call putgg(autpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" AUT"),lh(l),gll,wrks)
              call putgg(cndpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" CND"),lh(l),gll,wrks)
              call putgg(deppak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" DEP"),lh(l),gll,wrks)
              call putgg(evppak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" EVP"),lh(l),gll,wrks)
              call putgg(frhpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" FRH"),lh(l),gll,wrks)
              call putgg(frkpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" FRK"),lh(l),gll,wrks)
              call putgg(frspak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" FRS"),lh(l),gll,wrks)
              call putgg(mltipak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("MLTI"),lh(l),gll,wrks)
              call putgg(mltspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("MLTS"),lh(l),gll,wrks)
              call putgg(raclpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RACL"),lh(l),gll,wrks)
              call putgg(sacipak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SACI"),lh(l),gll,wrks)
              call putgg(saclpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SACL"),lh(l),gll,wrks)
              call putgg(subpak (1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" SUB"),lh(l),gll,wrks)
              call putgg(sedipak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("SEDI"),lh(l),gll,wrks)
              call putgg(rliqpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RLIQ"),lh(l),gll,wrks)
              call putgg(ricepak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("RICE"),lh(l),gll,wrks)
              call putgg(cliqpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CLIQ"),lh(l),gll,wrks)
              call putgg(cicepak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8("CICE"),lh(l),gll,wrks)
              call putgg(rlncpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                      nc4to8(" LNC"),lh(l),gll,wrks)
          end do
!
          call putgg(vaggpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VAGG"),1,gll,wrks)
          call putgg(vautpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VAUT"),1,gll,wrks)
          call putgg(vcndpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VCND"),1,gll,wrks)
          call putgg(vdeppak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VDEP"),1,gll,wrks)
          call putgg(vevppak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VEVP"),1,gll,wrks)
          call putgg(vfrhpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VFRH"),1,gll,wrks)
          call putgg(vfrkpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VFRK"),1,gll,wrks)
          call putgg(vfrspak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VFRS"),1,gll,wrks)
          call putgg(vmlipak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMLI"),1,gll,wrks)
          call putgg(vmlspak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VMLS"),1,gll,wrks)
          call putgg(vrclpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VRCL"),1,gll,wrks)
          call putgg(vscipak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSCI"),1,gll,wrks)
          call putgg(vsclpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSCL"),1,gll,wrks)
          call putgg(vsubpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSUB"),1,gll,wrks)
          call putgg(vsedipak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("VSED"),1,gll,wrks)
          call putgg(reliqpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("RELL"),1,gll,wrks)
          call putgg(reicepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("RELI"),1,gll,wrks)
          call putgg(cldliqpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("CLDL"),1,gll,wrks)
          call putgg(cldicepak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("CLDI"),1,gll,wrks)
          call putgg(ctlncpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("CTLN"),1,gll,wrks)
          call putgg(cllncpak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("CLLN"),1,gll,wrks)
!
          call pkzeros2(aggpak, ijpak,ilev)
          call pkzeros2(autpak, ijpak,ilev)
          call pkzeros2(cndpak, ijpak,ilev)
          call pkzeros2(deppak, ijpak,ilev)
          call pkzeros2(evppak, ijpak,ilev)
          call pkzeros2(frhpak, ijpak,ilev)
          call pkzeros2(frkpak, ijpak,ilev)
          call pkzeros2(frspak, ijpak,ilev)
          call pkzeros2(mltipak,ijpak,ilev)
          call pkzeros2(mltspak,ijpak,ilev)
          call pkzeros2(raclpak,ijpak,ilev)
          call pkzeros2(sacipak,ijpak,ilev)
          call pkzeros2(saclpak,ijpak,ilev)
          call pkzeros2(subpak, ijpak,ilev)
          call pkzeros2(sedipak,ijpak,ilev)
          call pkzeros2(rliqpak,ijpak,ilev)
          call pkzeros2(ricepak,ijpak,ilev)
          call pkzeros2(cliqpak,ijpak,ilev)
          call pkzeros2(cicepak,ijpak,ilev)
          call pkzeros2(rlncpak,ijpak,ilev)
!
          call pkzeros2(vaggpak,ijpak,   1)
          call pkzeros2(vautpak,ijpak,   1)
          call pkzeros2(vcndpak,ijpak,   1)
          call pkzeros2(vdeppak,ijpak,   1)
          call pkzeros2(vevppak,ijpak,   1)
          call pkzeros2(vfrhpak,ijpak,   1)
          call pkzeros2(vfrkpak,ijpak,   1)
          call pkzeros2(vfrspak,ijpak,   1)
          call pkzeros2(vmlipak,ijpak,   1)
          call pkzeros2(vmlspak,ijpak,   1)
          call pkzeros2(vrclpak,ijpak,   1)
          call pkzeros2(vscipak,ijpak,   1)
          call pkzeros2(vsclpak,ijpak,   1)
          call pkzeros2(vsubpak,ijpak,   1)
          call pkzeros2(vsedipak,ijpak,  1)
          call pkzeros2(reliqpak,ijpak,  1)
          call pkzeros2(reicepak,ijpak,  1)
          call pkzeros2(cldliqpak,ijpak, 1)
          call pkzeros2(cldicepak,ijpak, 1)
          call pkzeros2(ctlncpak,ijpak,  1)
          call pkzeros2(cllncpak,ijpak,  1)
#endif
  !
#if defined use_cosp
! cosp input for cloudsat simulator
          if (lradar_sim) then
           do l=1,ilev
             call putgg(rmixpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("RMIX"),lh(l),gll,wrks)
             call putgg(smixpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("SMIX"),lh(l),gll,wrks)
             call putgg(rrefpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("RREF"),lh(l),gll,wrks)
             call putgg(srefpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("SREF"),lh(l),gll,wrks)
           end do
          end if
          call pkzeros2(rmixpak,ijpak,ilev)
          call pkzeros2(smixpak,ijpak,ilev)
          call pkzeros2(rrefpak,ijpak,ilev)
          call pkzeros2(srefpak,ijpak,ilev)

! cosp output
! save fields from the module cosp_defs and zero as needed

! isccp

! total cloud from isccp
          if (lalbisccp) then
             call putgg(o_albisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ICCA"),1,gll,wrks)
             call pkzeros2(o_albisccp,ijpak,1)
          end if

          if (lcltisccp) then
             call putgg(o_cltisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ICCF"),1,gll,wrks)
             call pkzeros2(o_cltisccp,ijpak,1)
          end if

          if (ltauisccp) then
             call putgg(o_tauisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ICCT"),1,gll,wrks)
             call pkzeros2(o_tauisccp,ijpak,1)
          end if

          if (lpctisccp) then
             call putgg(o_pctisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ICCP"),1,gll,wrks)
             call pkzeros2(o_pctisccp,ijpak,1)
          end if

          if (lmeantbisccp) then
             call putgg(o_meantbisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8(" ITB"),1,gll,wrks)
             call pkzeros2(o_meantbisccp,ijpak,1)
          end if

          if (lmeantbclrisccp) then
             call putgg(o_meantbclrisccp,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("ITBC"),1,gll,wrks)
             call pkzeros2(o_meantbclrisccp,ijpak,1)
          end if

          if (lisccp_sim) then
             call putgg(o_sunl,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("SUNL"),1,gll,wrks)
             call pkzeros2(o_sunl,ijpak,1)
          end if

          if (lclisccp) then
             n = 1
             do ip = 1,  7      ! nptop
                do it = 1, 7    ! ntaucld
                   call putgg(o_clisccp(1,it,ip),lon1,ilat,khem,npgg, &
                           k,nupr,nc4to8("ICTP"),n,gll,wrks)
                   call pkzeros2(o_clisccp(1,it,ip),ijpak,1)
                   n = n + 1
                end do ! it
             end do ! ip
          end if

          if (lfracout) then
! array too large to be saved globally
             o_frac_out = 0.0
          end if

! calipso fields
          if (lclhcalipso) then
             call putgg(o_clhcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLHC"),1,gll,wrks)
             call pkzeros2(o_clhcalipso,ijpak,1)

             call putgg(ocnt_clhcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLHM"),1,gll,wrks)
             call pkzeros2(ocnt_clhcalipso,ijpak,1)
          end if

          if (lclmcalipso) then
             call putgg(o_clmcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLMC"),1,gll,wrks)
             call pkzeros2(o_clmcalipso,ijpak,1)

             call putgg(ocnt_clmcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLMM"),1,gll,wrks)
             call pkzeros2(ocnt_clmcalipso,ijpak,1)
          end if

          if (lcllcalipso) then
             call putgg(o_cllcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLLC"),1,gll,wrks)
             call pkzeros2(o_cllcalipso,ijpak,1)

             call putgg(ocnt_cllcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLLM"),1,gll,wrks)
             call pkzeros2(ocnt_cllcalipso,ijpak,1)
          end if

          if (lcltcalipso) then
             call putgg(o_cltcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLTC"),1,gll,wrks)
             call pkzeros2(o_cltcalipso,ijpak,1)

             call putgg(ocnt_cltcalipso,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CLTM"),1,gll,wrks)
             call pkzeros2(ocnt_cltcalipso,ijpak,1)
          end if

          if (lclhcalipsoliq) then
             call putgg(o_clhcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL1C"),1,gll,wrks)
             call pkzeros2(o_clhcalipsoliq,ijpak,1)

             call putgg(ocnt_clhcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL1M"),1,gll,wrks)
             call pkzeros2(ocnt_clhcalipsoliq,ijpak,1)
          end if

          if (lclmcalipsoliq) then
             call putgg(o_clmcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL2C"),1,gll,wrks)
             call pkzeros2(o_clmcalipsoliq,ijpak,1)

             call putgg(ocnt_clmcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL2M"),1,gll,wrks)
             call pkzeros2(ocnt_clmcalipsoliq,ijpak,1)
          end if

          if (lcllcalipsoliq) then
             call putgg(o_cllcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL3C"),1,gll,wrks)
             call pkzeros2(o_cllcalipsoliq,ijpak,1)

             call putgg(ocnt_cllcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL3M"),1,gll,wrks)
             call pkzeros2(ocnt_cllcalipsoliq,ijpak,1)
          end if

          if (lcltcalipsoliq) then
             call putgg(o_cltcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL4C"),1,gll,wrks)
             call pkzeros2(o_cltcalipsoliq,ijpak,1)

             call putgg(ocnt_cltcalipsoliq,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CL4M"),1,gll,wrks)
             call pkzeros2(ocnt_cltcalipsoliq,ijpak,1)
          end if

          if (lclhcalipsoice) then
             call putgg(o_clhcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI1C"),1,gll,wrks)
             call pkzeros2(o_clhcalipsoice,ijpak,1)

             call putgg(ocnt_clhcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI1M"),1,gll,wrks)
             call pkzeros2(ocnt_clhcalipsoice,ijpak,1)
          end if

          if (lclmcalipsoice) then
             call putgg(o_clmcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI2C"),1,gll,wrks)
             call pkzeros2(o_clmcalipsoice,ijpak,1)

             call putgg(ocnt_clmcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI2M"),1,gll,wrks)
             call pkzeros2(ocnt_clmcalipsoice,ijpak,1)
          end if

          if (lcllcalipsoice) then
             call putgg(o_cllcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI3C"),1,gll,wrks)
             call pkzeros2(o_cllcalipsoice,ijpak,1)

             call putgg(ocnt_cllcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI3M"),1,gll,wrks)
             call pkzeros2(ocnt_cllcalipsoice,ijpak,1)
          end if

          if (lcltcalipsoice) then
             call putgg(o_cltcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI4C"),1,gll,wrks)
             call pkzeros2(o_cltcalipsoice,ijpak,1)

             call putgg(ocnt_cltcalipsoice,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CI4M"),1,gll,wrks)
             call pkzeros2(ocnt_cltcalipsoice,ijpak,1)
          end if


          if (lclhcalipsoun) then
             call putgg(o_clhcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU1C"),1,gll,wrks)
             call pkzeros2(o_clhcalipsoun,ijpak,1)

             call putgg(ocnt_clhcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU1M"),1,gll,wrks)
             call pkzeros2(ocnt_clhcalipsoun,ijpak,1)
          end if

          if (lclmcalipsoun) then
             call putgg(o_clmcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU2C"),1,gll,wrks)
             call pkzeros2(o_clmcalipsoun,ijpak,1)

             call putgg(ocnt_clmcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU2M"),1,gll,wrks)
             call pkzeros2(ocnt_clmcalipsoun,ijpak,1)
          end if

          if (lcllcalipsoun) then
             call putgg(o_cllcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU3C"),1,gll,wrks)
             call pkzeros2(o_cllcalipsoun,ijpak,1)

             call putgg(ocnt_cllcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU3M"),1,gll,wrks)
             call pkzeros2(ocnt_cllcalipsoun,ijpak,1)
          end if

          if (lcltcalipsoun) then
             call putgg(o_cltcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU4C"),1,gll,wrks)
             call pkzeros2(o_cltcalipsoun,ijpak,1)

             call putgg(ocnt_cltcalipsoun,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CU4M"),1,gll,wrks)
             call pkzeros2(ocnt_cltcalipsoun,ijpak,1)
          end if

! need to handle potential of different vertical methods
          if (use_vgrid) then   ! interpolate to specific heights
             if (lclcalipso) then
                do l=1,nlr
                   call putgg(o_clcalipso(1,l),lon1,ilat,khem,npgg,k, &
                           nupr,nc4to8("CLCP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipso(1,l),lon1,ilat,khem,npgg &
                           ,k,nupr,nc4to8("CLCM"),llidar(l),gll, &
                           wrks)
                end do
                call pkzeros2(o_clcalipso,ijpak,nlr)
                call pkzeros2(ocnt_clcalipso,ijpak,nlr)
             end if

             if (lclcalipsoliq) then
                do l=1,nlr
                   call putgg(o_clcalipsoliq(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CLPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoliq(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CLPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoliq,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoliq,ijpak,nlr)
             end if

             if (lclcalipsoice) then
                do l=1,nlr
                   call putgg(o_clcalipsoice(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CIPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoice(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CIPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoice,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoice,ijpak,nlr)
             end if

             if (lclcalipsoun) then
                do l=1,nlr
                   call putgg(o_clcalipsoun(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CUPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoun(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CUPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoun,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoun,ijpak,nlr)
             end if

          else                  ! output on gcm layers
             if (lclcalipso) then
                do l=1,ilev
                   call putgg(o_clcalipso(1,l),lon1,ilat,khem,npgg,k, &
                            nupr,nc4to8("CLCP"),llidar(l),gll,wrks)
                   call putgg(ocnt_clcalipso(1,l),lon1,ilat,khem,npgg &
                           ,k,nupr,nc4to8("CLCM"),llidar(l),gll, &
                           wrks)
                end do
                call pkzeros2(o_clcalipso,ijpak,ilev)
                call pkzeros2(ocnt_clcalipso,ijpak,ilev)
             end if

             if (lclcalipsoliq) then
                do l=1,nlr
                   call putgg(o_clcalipsoliq(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CLPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoliq(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CLPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoliq,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoliq,ijpak,nlr)
             end if

             if (lclcalipsoice) then
                do l=1,nlr
                   call putgg(o_clcalipsoice(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CIPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoice(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CIPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoice,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoice,ijpak,nlr)
             end if

             if (lclcalipsoun) then
                do l=1,nlr
                   call putgg(o_clcalipsoun(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CUPP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipsoun(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CUPM"),llidar(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsoun,ijpak,nlr)
                call pkzeros2(ocnt_clcalipsoun,ijpak,nlr)
             end if

          end if

             if (lclcalipsotmp) then
                do l=1,lidar_ntemp
                   call putgg(o_clcalipsotmp(1,l),lon1,ilat,khem,npgg, &
                           k,nupr,nc4to8("CT1P"),llidartemp(l),gll, &
                           wrks)

                   call putgg(ocnt_clcalipsotmp(1,l),lon1,ilat,khem, &
                          npgg,k,nupr,nc4to8("CT1M"),llidartemp(l), &
                           gll,wrks)
                end do
                call pkzeros2(o_clcalipsotmp,ijpak,lidar_ntemp)
                call pkzeros2(ocnt_clcalipsotmp,ijpak,lidar_ntemp)
             end if

             if (lclcalipsotmpliq) then
                do l=1,lidar_ntemp
                   call putgg(o_clcalipsotmpliq(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CT2P"), &
                           llidartemp(l),gll,wrks)

                   call putgg(ocnt_clcalipsotmpliq(1,l),lon1,ilat, &
                           khem,npgg,k,nupr,nc4to8("CT2M"), &
                           llidartemp(l),gll,wrks)
                end do
                call pkzeros2(o_clcalipsotmpliq,ijpak,lidar_ntemp)
                call pkzeros2(ocnt_clcalipsotmpliq,ijpak,lidar_ntemp)
             end if

             if (lclcalipsotmpice) then
                do l=1,lidar_ntemp
                   call putgg(o_clcalipsotmpice(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CT3P"), &
                           llidartemp(l),gll,wrks)

                   call putgg(ocnt_clcalipsotmpice(1,l),lon1,ilat, &
                           khem,npgg,k,nupr,nc4to8("CT3M"), &
                           llidartemp(l),gll,wrks)
                end do
                call pkzeros2(o_clcalipsotmpice,ijpak,lidar_ntemp)
                call pkzeros2(ocnt_clcalipsotmpice,ijpak,lidar_ntemp)
             end if

             if (lclcalipsotmpun) then
                do l=1,lidar_ntemp
                   call putgg(o_clcalipsotmpun(1,l),lon1,ilat,khem, &
                           npgg,k,nupr,nc4to8("CT4P"), &
                           llidartemp(l),gll,wrks)

                   call putgg(ocnt_clcalipsotmpun(1,l),lon1,ilat, &
                           khem,npgg,k,nupr,nc4to8("CT4M"), &
                           llidartemp(l),gll,wrks)
                end do
                call pkzeros2(o_clcalipsotmpun,ijpak,lidar_ntemp)
                call pkzeros2(ocnt_clcalipsotmpun,ijpak,lidar_ntemp)
             end if
          if (use_vgrid) then   ! interpolate to specific heights
             if (lcfadlidarsr532) then
                do n = 1, sr_bins
                   call name2(nam,'SR',n)
                   do l=1,nlr
                      call putgg(o_cfad_lidarsr532(1,l,n),lon1,ilat, &
                           khem,npgg,k,nupr,nam,llidar(l),gll,wrks)
                   end do
                   call pkzeros2(o_cfad_lidarsr532(1,1,n),ijpak,nlr)
                end do
             end if
          else                  ! output on gcm layers
             if (lcfadlidarsr532) then
                do n = 1, sr_bins
                   call name2(nam,'SR',n)
                   do l=1,ilev
                      call putgg(o_cfad_lidarsr532(1,l,n),lon1,ilat, &
                           khem,npgg,k,nupr,nam,llidar(l),gll,wrks)
                   end do
                   call pkzeros2(o_cfad_lidarsr532(1,1,n),ijpak,ilev)
                end do
             end if
          end if

          if (use_vgrid) then   ! interpolate to specific heights
             if (llidarbetamol532) then
                do l=1,nlr
                   call putgg(o_beta_mol532(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("LDBS"),llidar(l),gll,wrks)
                end do
                call pkzeros2(o_beta_mol532,ijpak,nlr)
             end if
          else                  ! output on gcm layers
             if (llidarbetamol532) then
                do l=1,ilev
                   call putgg(o_beta_mol532(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("LDBS"),llidar(l),gll,wrks)
                end do
                call pkzeros2(o_beta_mol532,ijpak,ilev)
             end if
          end if

          if (latb532) then
! array too large to be saved globally
             o_atb532 = 0.0
          end if

! cloudsat fields
          if (use_vgrid) then   ! interpolate to specific heights
             if (lcfaddbze94) then
                do n = 1, dbze_bins
                   call name2(nam,'ZE',n)
                   do l=1,nlr
                      call putgg(o_cfad_dbze94(1,l,n),lon1,ilat,khem, &
                               npgg,k,nupr,nam,lradar(l),gll,wrks)
                   end do
                   call pkzeros2(o_cfad_dbze94(1,1,n),ijpak,nlr)
                end do
             end if
          else                  ! output on gcm layers
             if (lcfaddbze94) then
                do n = 1, dbze_bins
                   call name2(nam,'ZE',n)
                   do l=1,ilev
                      call putgg(o_cfad_dbze94(1,l,n),lon1,ilat,khem, &
                              npgg,k,nupr,nam,lradar(l),gll,wrks)
                   end do
                   call pkzeros2(o_cfad_dbze94(1,1,n),ijpak,ilev)
                end do
             end if
          end if

          if (use_vgrid) then   ! interpolate to specific heights
             if (ldbze94) then
! array too large to save globally
                o_dbze94 = 0.0
             end if
          else                  ! output on gcm layers
             if (ldbze94) then
! array too large to save globally
                o_dbze94 = 0.0
             end if
          end if

! cloudsat+calipso fields
          if (lcltlidarradar) then
             call putgg(o_cltlidarradar,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CCTC"),1,gll,wrks)
             call pkzeros2(o_cltlidarradar,ijpak,1)

             call putgg(ocnt_cltlidarradar,lon1,ilat,khem,npgg,k,nupr, &
                     nc4to8("CCTM"),1,gll,wrks)
             call pkzeros2(ocnt_cltlidarradar,ijpak,1)
          end if

          if (use_vgrid) then   ! interpolate to specific heights
             if (lclcalipso2) then
                do l=1,nlr
                   call putgg(o_clcalipso2(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CCCP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipso2(1,l),lon1,ilat,khem, &
                          npgg,k,nupr,nc4to8("CCCM"),llidar(l),gll, &
                          wrks)
                end do
                call pkzeros2(o_clcalipso2,ijpak,nlr)
                call pkzeros2(ocnt_clcalipso2,ijpak,nlr)
             end if
          else                  ! output on gcm layers
             if (lclcalipso2) then
                do l=1,ilev
                   call putgg(o_clcalipso2(1,l),lon1,ilat,khem,npgg, &
                          k,nupr,nc4to8("CCCP"),llidar(l),gll,wrks)

                   call putgg(ocnt_clcalipso2(1,l),lon1,ilat,khem, &
                          npgg,k,nupr,nc4to8("CCCM"),llidar(l),gll, &
                          wrks)
                end do
                call pkzeros2(o_clcalipso2,ijpak,ilev)
                call pkzeros2(ocnt_clcalipso2,ijpak,ilev)
             end if
          end if
! cosp height mask
          if (use_vgrid) then   ! interpolate to specific heights
             if (lcfaddbze94 .or. lcfadlidarsr532) then
                do l=1,nlr
                   call putgg(o_cosp_height_mask(1,l),lon1,ilat,khem, &
                          npgg,k,nupr,nc4to8("CHMK"),llidar(l),gll, &
                          wrks)
                end do
                call pkzeros2(o_cosp_height_mask,ijpak,nlr)
             end if
          else ! output on gcm layers
             if (lcfaddbze94 .or. lcfadlidarsr532) then
                do l=1,ilev
                   call putgg(o_cosp_height_mask(1,l),lon1,ilat,khem, &
                          npgg,k,nupr,nc4to8("CHMK"),llidar(l),gll, &
                          wrks)
                end do
                call pkzeros2(o_cosp_height_mask,ijpak,ilev)
             end if
          end if
! parasol
          if (lparasolrefl) then
             do n = 1, parasol_nrefl
                call name2(nam,'PL',n)
                call putgg(o_parasol_refl(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nam,1,gll,wrks)

                call name2(nam,'ML',n)
                call putgg(ocnt_parasol_refl(1,n),lon1,ilat,khem,npgg &
                        ,k,nupr,nam,1,gll,wrks)
             end do
             call pkzeros2(o_parasol_refl,ijpak,parasol_nrefl)
             call pkzeros2(ocnt_parasol_refl,ijpak,parasol_nrefl)
          end if

          if (lceres_sim) then
             do n = 1, nceres
                call putgg(o_ceres_cf(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCCF"),n,gll,wrks)

                call putgg(o_ceres_cnt(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCNT"),n,gll,wrks)

                call putgg(o_ceres_ctp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCTP"),n,gll,wrks)

                call putgg(o_ceres_tau(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCCT"),n,gll,wrks)

                call putgg(o_ceres_lntau(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCLT"),n,gll,wrks)

                call putgg(o_ceres_cfl(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCFL"),n,gll,wrks)

                call putgg(o_ceres_cfi(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCFI"),n,gll,wrks)

                call putgg(o_ceres_lwp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CLWP"),n,gll,wrks)

                call putgg(o_ceres_iwp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CIWP"),n,gll,wrks)

                call putgg(o_ceres_cntl(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CNLC"),n,gll,wrks)

                call putgg(o_ceres_cnti(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CNIC"),n,gll,wrks)

                call putgg(o_ceres_rel(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CREL"),n,gll,wrks)

                call putgg(o_ceres_cdnc(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCDD"),n,gll,wrks)

                call putgg(o_ceres_clm(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CCFM"),n,gll,wrks)

                call putgg(o_ceres_cntlm(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CNLM"),n,gll,wrks)
             end do

             call pkzeros2(o_ceres_cf,ijpak,nceres)
             call pkzeros2(o_ceres_cnt,ijpak,nceres)
             call pkzeros2(o_ceres_ctp,ijpak,nceres)
             call pkzeros2(o_ceres_tau,ijpak,nceres)
             call pkzeros2(o_ceres_lntau,ijpak,nceres)
             call pkzeros2(o_ceres_lwp,ijpak,nceres)
             call pkzeros2(o_ceres_iwp,ijpak,nceres)
             call pkzeros2(o_ceres_cfl,ijpak,nceres)
             call pkzeros2(o_ceres_cfi,ijpak,nceres)
             call pkzeros2(o_ceres_cntl,ijpak,nceres)
             call pkzeros2(o_ceres_cnti,ijpak,nceres)
             call pkzeros2(o_ceres_rel,ijpak,nceres)
             call pkzeros2(o_ceres_cdnc,ijpak,nceres)
             call pkzeros2(o_ceres_clm,ijpak,nceres)
             call pkzeros2(o_ceres_cntlm,ijpak,nceres)

          end if

          if (lceres_sim .and. lswath) then
             do n = 1, nceres
                call putgg(os_ceres_cf(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS01"),n,gll,wrks)

                call putgg(os_ceres_cnt(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS02"),n,gll,wrks)

                call putgg(os_ceres_ctp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS03"),n,gll,wrks)

                call putgg(os_ceres_tau(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS04"),n,gll,wrks)

                call putgg(os_ceres_lntau(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS05"),n,gll,wrks)

                call putgg(os_ceres_cfl(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS06"),n,gll,wrks)

                call putgg(os_ceres_cfi(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS07"),n,gll,wrks)

                call putgg(os_ceres_lwp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS08"),n,gll,wrks)

                call putgg(os_ceres_iwp(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS09"),n,gll,wrks)

                call putgg(os_ceres_cntl(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS10"),n,gll,wrks)

                call putgg(os_ceres_cnti(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS11"),n,gll,wrks)

                call putgg(os_ceres_rel(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS12"),n,gll,wrks)

                call putgg(os_ceres_cdnc(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS13"),n,gll,wrks)

                call putgg(os_ceres_clm(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS14"),n,gll,wrks)

                call putgg(os_ceres_cntlm(1,n),lon1,ilat,khem,npgg,k, &
                        nupr,nc4to8("CS15"),n,gll,wrks)

             end do

             call pkzeros2(os_ceres_cf,ijpak,nceres)
             call pkzeros2(os_ceres_cnt,ijpak,nceres)
             call pkzeros2(os_ceres_ctp,ijpak,nceres)
             call pkzeros2(os_ceres_tau,ijpak,nceres)
             call pkzeros2(os_ceres_lntau,ijpak,nceres)
             call pkzeros2(os_ceres_lwp,ijpak,nceres)
             call pkzeros2(os_ceres_iwp,ijpak,nceres)
             call pkzeros2(os_ceres_cfl,ijpak,nceres)
             call pkzeros2(os_ceres_cfi,ijpak,nceres)
             call pkzeros2(os_ceres_cntl,ijpak,nceres)
             call pkzeros2(os_ceres_cnti,ijpak,nceres)
             call pkzeros2(os_ceres_rel,ijpak,nceres)
             call pkzeros2(os_ceres_cdnc,ijpak,nceres)
             call pkzeros2(os_ceres_clm,ijpak,nceres)
             call pkzeros2(os_ceres_cntlm,ijpak,nceres)

          end if

! misr
          if (lclmisr) then
             call putgg(o_misr_cldarea,lon1,ilat,khem,npgg,k, &
                     nupr,nc4to8("MSTC"),1,gll,wrks)
             call putgg(o_misr_mean_ztop,lon1,ilat,khem,npgg,k, &
                    nupr,nc4to8("MSMZ"),1,gll,wrks)

             call pkzeros2(o_misr_cldarea,ijpak,1)
             call pkzeros2(o_misr_mean_ztop,ijpak,1)

             nbox = 16
             do ibox = 1, nbox
             call putgg(o_dist_model_layertops(1,ibox),lon1,ilat,khem, &
                     npgg,k,nupr,nc4to8("MSDT"),ibox,gll,wrks)
             end do ! ibox
             call pkzeros2(o_dist_model_layertops,ijpak,nbox)

             nbox = 7*16
             do ibox = 1, nbox
                call putgg(o_fq_misr_tau_v_cth(1,ibox),lon1,ilat,khem, &
                        npgg,k,nupr,nc4to8("MSTZ"),ibox,gll,wrks)
             end do ! ibox
             call pkzeros2(o_fq_misr_tau_v_cth,ijpak,nbox)

          end if ! lclmisr

! modis
          if (lcltmodis) then
          call putgg(o_modis_cloud_fraction_total_mean,lon1,ilat,khem, &
                  npgg,k,nupr,nc4to8("MDTC"),1,gll,wrks)
          call pkzeros2(o_modis_cloud_fraction_total_mean,ijpak,1)
          end if

          if (lclwmodis) then
          call putgg(o_modis_cloud_fraction_water_mean,lon1,ilat,khem, &
                  npgg,k,nupr,nc4to8("MDWC"),1,gll,wrks)
          call pkzeros2(o_modis_cloud_fraction_water_mean,ijpak,1)
          end if

          if (lclimodis) then
            call putgg(o_modis_cloud_fraction_ice_mean,lon1,ilat,khem, &
                   npgg,k,nupr,nc4to8("MDIC"),1,gll,wrks)
            call pkzeros2(o_modis_cloud_fraction_ice_mean,ijpak,1)
          end if

          if (lclhmodis) then
           call putgg(o_modis_cloud_fraction_high_mean,lon1,ilat,khem, &
                  npgg,k,nupr,nc4to8("MDHC"),1,gll,wrks)
           call pkzeros2(o_modis_cloud_fraction_high_mean,ijpak,1)
          end if

          if (lclmmodis) then
            call putgg(o_modis_cloud_fraction_mid_mean,lon1,ilat,khem, &
                    npgg,k,nupr,nc4to8("MDMC"),1,gll,wrks)
            call pkzeros2(o_modis_cloud_fraction_mid_mean,ijpak,1)
          end if

          if (lcllmodis) then
            call putgg(o_modis_cloud_fraction_low_mean,lon1,ilat,khem, &
                    npgg,k,nupr,nc4to8("MDLC"),1,gll,wrks)
            call pkzeros2(o_modis_cloud_fraction_low_mean,ijpak,1)
          end if

          if (ltautmodis) then
            call putgg(o_modis_optical_thickness_total_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDTT"),1,gll,wrks)
            call pkzeros2(o_modis_optical_thickness_total_mean,ijpak,1)
          end if

          if (ltauwmodis) then
            call putgg(o_modis_optical_thickness_water_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDWT"),1,gll,wrks)
            call pkzeros2(o_modis_optical_thickness_water_mean,ijpak,1)
          end if

          if (ltauimodis) then
             call putgg(o_modis_optical_thickness_ice_mean,lon1,ilat, &
                     khem,npgg,k,nupr,nc4to8("MDIT"),1,gll,wrks)
             call pkzeros2(o_modis_optical_thickness_ice_mean,ijpak,1)
          end if

          if (ltautlogmodis) then
          call putgg(o_modis_optical_thickness_total_logmean,lon1, &
                  ilat,khem,npgg,k,nupr,nc4to8("MDGT"),1,gll,wrks)
          call pkzeros2(o_modis_optical_thickness_total_logmean,ijpak,1)
          end if

          if (ltauwlogmodis) then
          call putgg(o_modis_optical_thickness_water_logmean,lon1, &
                 ilat,khem,npgg,k,nupr,nc4to8("MDGW"),1,gll,wrks)
          call pkzeros2(o_modis_optical_thickness_water_logmean,ijpak,1)
          end if

          if (ltauilogmodis) then
           call putgg(o_modis_optical_thickness_ice_logmean,lon1,ilat, &
                   khem,npgg,k,nupr,nc4to8("MDGI"),1,gll,wrks)
           call pkzeros2(o_modis_optical_thickness_ice_logmean,ijpak,1)
          end if

          if (lreffclwmodis) then
          call putgg(o_modis_cloud_particle_size_water_mean,lon1,ilat, &
                  khem,npgg,k,nupr,nc4to8("MDRE"),1,gll,wrks)
          call pkzeros2(o_modis_cloud_particle_size_water_mean,ijpak,1)
          end if

          if (lreffclimodis) then
            call putgg(o_modis_cloud_particle_size_ice_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDRI"),1,gll,wrks)
            call pkzeros2(o_modis_cloud_particle_size_ice_mean,ijpak,1)
          end if

          if (lpctmodis) then
           call putgg(o_modis_cloud_top_pressure_total_mean,lon1,ilat, &
                   khem,npgg,k,nupr,nc4to8("MDPT"),1,gll,wrks)
           call pkzeros2(o_modis_cloud_top_pressure_total_mean,ijpak,1)
          end if

          if (llwpmodis) then
            call putgg(o_modis_liquid_water_path_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDWP"),1,gll,wrks)
            call pkzeros2(o_modis_liquid_water_path_mean,ijpak,1)
          end if

          if (liwpmodis) then
            call putgg(o_modis_ice_water_path_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDIP"),1,gll,wrks)
            call pkzeros2(o_modis_ice_water_path_mean,ijpak,1)
          end if

          if (llcdncmodis) then
            call putgg(o_modis_liq_cdnc_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MLN1"),1,gll,wrks)
            call putgg(o_modis_liq_cdnc_gcm_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MLN2"),1,gll,wrks)
            call putgg(o_modis_cloud_fraction_warm_mean,lon1,ilat, &
                    khem,npgg,k,nupr,nc4to8("MDLQ"),1,gll,wrks)

            call pkzeros2(o_modis_liq_cdnc_mean,ijpak,1)
            call pkzeros2(o_modis_liq_cdnc_gcm_mean,ijpak,1)
            call pkzeros2(o_modis_cloud_fraction_warm_mean,ijpak,1)
          end if

          if (lclmodis) then
           nbox = (a_nummodistaubins+1)*a_nummodispressurebins
           do ibox = 1, nbox
           call putgg(o_modis_optical_thickness_vs_cloud_top_pressure( &
                                                            1,ibox) &
          ,lon1,ilat,khem,npgg,k,nupr,nc4to8("MDTP"),ibox,gll,wrks)
           end do ! ibox
           call pkzeros2(o_modis_optical_thickness_vs_cloud_top_pressure &
                    ,ijpak,nbox)
          end if

          if (lcrimodis) then
           nbox = (a_nummodistaubins+1)*a_nummodisrefficebins
           do ibox = 1, nbox
           call putgg(o_modis_optical_thickness_vs_reffice(1,ibox), &
           lon1,ilat,khem,npgg,k,nupr,nc4to8("MDTI"),ibox,gll,wrks)
           end do ! ibox
           call pkzeros2(o_modis_optical_thickness_vs_reffice, &
                    ijpak,nbox)
          end if

          if (lcrlmodis) then
           nbox = (a_nummodistaubins+1)*a_nummodisreffliqbins
           do ibox = 1, nbox
           call putgg(o_modis_optical_thickness_vs_reffliq(1,ibox), &
           lon1,ilat,khem,npgg,k,nupr,nc4to8("MDTL"),ibox,gll,wrks)
           end do ! ibox
           call pkzeros2(o_modis_optical_thickness_vs_reffliq, &
                    ijpak,nbox)
          end if

#endif
#if defined (aodpth)
!
          call putgg(exb1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXB1") &
                          ,1,gll,wrks)
          call putgg(exb2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXB2") &
                          ,1,gll,wrks)
          call putgg(exb3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXB3") &
                          ,1,gll,wrks)
          call putgg(exb4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXB4") &
                          ,1,gll,wrks)
          call putgg(exb5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXB5") &
                          ,1,gll,wrks)
          call putgg(exbtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXBT") &
                          ,1,gll,wrks)
          call putgg(odb1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODB1") &
                          ,1,gll,wrks)
          call putgg(odb2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODB2") &
                          ,1,gll,wrks)
          call putgg(odb3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODB3") &
                          ,1,gll,wrks)
          call putgg(odb4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODB4") &
                          ,1,gll,wrks)
          call putgg(odb5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODB5") &
                          ,1,gll,wrks)
          call putgg(odbtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODBT") &
                          ,1,gll,wrks)
          call putgg(odbvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODBV") &
                          ,1,gll,wrks)
          call putgg(ofb1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFB1") &
                          ,1,gll,wrks)
          call putgg(ofb2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFB2") &
                          ,1,gll,wrks)
          call putgg(ofb3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFB3") &
                          ,1,gll,wrks)
          call putgg(ofb4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFB4") &
                          ,1,gll,wrks)
          call putgg(ofb5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFB5") &
                          ,1,gll,wrks)
          call putgg(ofbtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFBT") &
                          ,1,gll,wrks)
          call putgg(abb1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABB1") &
                          ,1,gll,wrks)
          call putgg(abb2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABB2") &
                          ,1,gll,wrks)
          call putgg(abb3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABB3") &
                          ,1,gll,wrks)
          call putgg(abb4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABB4") &
                          ,1,gll,wrks)
          call putgg(abb5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABB5") &
                          ,1,gll,wrks)
          call putgg(abbtpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABBT") &
                          ,1,gll,wrks)
!
          call putgg(exs1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXS1") &
                          ,1,gll,wrks)
          call putgg(exs2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXS2") &
                          ,1,gll,wrks)
          call putgg(exs3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXS3") &
                          ,1,gll,wrks)
          call putgg(exs4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXS4") &
                          ,1,gll,wrks)
          call putgg(exs5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXS5") &
                          ,1,gll,wrks)
          call putgg(exstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("EXST") &
                          ,1,gll,wrks)
          call putgg(ods1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODS1") &
                          ,1,gll,wrks)
          call putgg(ods2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODS2") &
                          ,1,gll,wrks)
          call putgg(ods3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODS3") &
                          ,1,gll,wrks)
          call putgg(ods4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODS4") &
                          ,1,gll,wrks)
          call putgg(ods5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODS5") &
                          ,1,gll,wrks)
          call putgg(odstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODST") &
                          ,1,gll,wrks)
          call putgg(odsvpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ODSV") &
                          ,1,gll,wrks)
          call putgg(ofs1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFS1") &
                          ,1,gll,wrks)
          call putgg(ofs2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFS2") &
                          ,1,gll,wrks)
          call putgg(ofs3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFS3") &
                          ,1,gll,wrks)
          call putgg(ofs4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFS4") &
                          ,1,gll,wrks)
          call putgg(ofs5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFS5") &
                          ,1,gll,wrks)
          call putgg(ofstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("OFST") &
                          ,1,gll,wrks)
          call putgg(abs1pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABS1") &
                          ,1,gll,wrks)
          call putgg(abs2pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABS2") &
                          ,1,gll,wrks)
          call putgg(abs3pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABS3") &
                          ,1,gll,wrks)
          call putgg(abs4pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABS4") &
                          ,1,gll,wrks)
          call putgg(abs5pak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABS5") &
                          ,1,gll,wrks)
          call putgg(abstpak,lon1,ilat,khem,npgg,k,nupr,nc4to8("ABST") &
                          ,1,gll,wrks)
!
          call pkzeros2(exb1pak,ijpak,   1)
          call pkzeros2(exb2pak,ijpak,   1)
          call pkzeros2(exb3pak,ijpak,   1)
          call pkzeros2(exb4pak,ijpak,   1)
          call pkzeros2(exb5pak,ijpak,   1)
          call pkzeros2(exbtpak,ijpak,   1)
          call pkzeros2(odb1pak,ijpak,   1)
          call pkzeros2(odb2pak,ijpak,   1)
          call pkzeros2(odb3pak,ijpak,   1)
          call pkzeros2(odb4pak,ijpak,   1)
          call pkzeros2(odb5pak,ijpak,   1)
          call pkzeros2(odbtpak,ijpak,   1)
          call pkzeros2(odbvpak,ijpak,   1)
          call pkzeros2(ofb1pak,ijpak,   1)
          call pkzeros2(ofb2pak,ijpak,   1)
          call pkzeros2(ofb3pak,ijpak,   1)
          call pkzeros2(ofb4pak,ijpak,   1)
          call pkzeros2(ofb5pak,ijpak,   1)
          call pkzeros2(ofbtpak,ijpak,   1)
          call pkzeros2(abb1pak,ijpak,   1)
          call pkzeros2(abb2pak,ijpak,   1)
          call pkzeros2(abb3pak,ijpak,   1)
          call pkzeros2(abb4pak,ijpak,   1)
          call pkzeros2(abb5pak,ijpak,   1)
          call pkzeros2(abbtpak,ijpak,   1)
!
          call pkzeros2(exs1pak,ijpak,   1)
          call pkzeros2(exs2pak,ijpak,   1)
          call pkzeros2(exs3pak,ijpak,   1)
          call pkzeros2(exs4pak,ijpak,   1)
          call pkzeros2(exs5pak,ijpak,   1)
          call pkzeros2(exstpak,ijpak,   1)
          call pkzeros2(ods1pak,ijpak,   1)
          call pkzeros2(ods2pak,ijpak,   1)
          call pkzeros2(ods3pak,ijpak,   1)
          call pkzeros2(ods4pak,ijpak,   1)
          call pkzeros2(ods5pak,ijpak,   1)
          call pkzeros2(odstpak,ijpak,   1)
          call pkzeros2(odsvpak,ijpak,   1)
          call pkzeros2(ofs1pak,ijpak,   1)
          call pkzeros2(ofs2pak,ijpak,   1)
          call pkzeros2(ofs3pak,ijpak,   1)
          call pkzeros2(ofs4pak,ijpak,   1)
          call pkzeros2(ofs5pak,ijpak,   1)
          call pkzeros2(ofstpak,ijpak,   1)
          call pkzeros2(abs1pak,ijpak,   1)
          call pkzeros2(abs2pak,ijpak,   1)
          call pkzeros2(abs3pak,ijpak,   1)
          call pkzeros2(abs4pak,ijpak,   1)
          call pkzeros2(abs5pak,ijpak,   1)
          call pkzeros2(abstpak,ijpak,   1)
#endif
#if defined (explvol)
      if (ivtau == 2) then
         call putgg(w055_vtau_sa_pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("W055"),1,gll,wrks)
         call putgg(w110_vtau_sa_pak,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("W110"),1,gll,wrks)

         call pkzeros2(w055_vtau_sa_pak,ijpak,   1)
         call pkzeros2(w110_vtau_sa_pak,ijpak,   1)

          do l=1,ilev
             call putgg(w055_ext_gcm_sa_pak(1,l),lon1,ilat,khem,npgg, &
                     k,nupr,nc4to8("EV55"),lh(l),gll,wrks)
             call putgg(w110_ext_gcm_sa_pak(1,l),lon1,ilat,khem,npgg, &
                     k,nupr,nc4to8("EV11"),lh(l),gll,wrks)
          end do
          call pkzeros2(w055_ext_gcm_sa_pak,ijpak,ilev)
          call pkzeros2(w110_ext_gcm_sa_pak,ijpak,ilev)
      end if ! ivtau
#endif
end if
!
!     * save gwd diagnostic fields under control of switch.
!
if (lsinst_extragwd) then
  kk = kount_inst_extragwd
  do l=1,ilev
    call putgg(ftoxpak(1,l),lon1,ilat,khem,npgg,kk,nupr, &
                     nc4to8("FTOX"),lhb(l),gll,wrks)
  end do
  do l=1,ilev
    call putgg(ftoypak(1,l),lon1,ilat,khem,npgg,kk,nupr, &
                     nc4to8("FTOY"),lhb(l),gll,wrks)
  end do
  do l=1,ilev
    call putgg(tdoxpak(1,l),lon1,ilat,khem,npgg,kk,nupr, &
                     nc4to8("TDOX"),lh(l),gll,wrks)
  end do
  do l=1,ilev
    call putgg(tdoypak(1,l),lon1,ilat,khem,npgg,kk,nupr, &
                     nc4to8("TDOY"),lh(l),gll,wrks)
  end do
end if

#if defined rad_flux_profs
!     * save lw/sw all-sky/clear-sky up/down radiative fluxes at every model
!     * level, every israd steps then reset accumulated field to zero
      if (lsrad) then
       k = kount

        do l=1,ilev+2

          !--- upward all-sky shortwave flux
          call putgg(fsaupak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FSAU"),lrp(l),gll,wrks)
          !--- downward all-sky shortwave flux
          call putgg(fsadpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FSAD"),lrp(l),gll,wrks)
          !--- upward all-sky longwave flux
          call putgg(flaupak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FLAU"),lrp(l),gll,wrks)
          !--- downward all-sky longwave flux
          call putgg(fladpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FLAD"),lrp(l),gll,wrks)


          !--- upward clear-sky shortwave flux
          call putgg(fscupak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FSCU"),lrp(l),gll,wrks)
          !--- downward clear-sky shortwave flux
          call putgg(fscdpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FSCD"),lrp(l),gll,wrks)
          !--- upward clear-sky longwave flux
          call putgg(flcupak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FLCU"),lrp(l),gll,wrks)
          !--- downward clear-sky longwave flux
          call putgg(flcdpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                           nc4to8("FLCD"),lrp(l),gll,wrks)

       end do ! l

       call pkzeros2(fsaupak,ijpak,ilev+2)
       call pkzeros2(fsadpak,ijpak,ilev+2)
       call pkzeros2(flaupak,ijpak,ilev+2)
       call pkzeros2(fladpak,ijpak,ilev+2)
       call pkzeros2(fscupak,ijpak,ilev+2)
       call pkzeros2(fscdpak,ijpak,ilev+2)
       call pkzeros2(flcupak,ijpak,ilev+2)
       call pkzeros2(flcdpak,ijpak,ilev+2)

      end if ! lsrad
#endif

#if defined radforce
!
!     * save lw/sw all-sky/clear-sky radiative fluxes at every model
!     * level, every israd steps then reset accumulated field to zero

      if (lsrad) then
        k = kount

!
!.......save perturbation temperature and fluxes from the diagnostics radiation calls
!
        do nrf=1,nrfp

          write(rfp_id,'(I2.2)') nrf

!.........t_* - t_o at all model levels
          do l=1,ilev
            !--- t_* - t_o
            rfname="DT"//rfp_id(1:2)
            call putgg(rdtpak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                    k,nupr,nc4to8(rfname(1:4)),lh(l),gll,wrks)
          end do

          write(rfp_id,'(I1.1)') nrf

          do l=1,ilev+2

            !--- upward all-sky shortwave flux
            rfname="SAU"//rfp_id(1:1)
            call putgg(fsaupak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- downward all-sky shortwave flux
            rfname="SAD"//rfp_id(1:1)
            call putgg(fsadpak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- upward all-sky longwave flux
            rfname="LAU"//rfp_id(1:1)
            call putgg(flaupak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- downward all-sky longwave flux
            rfname="LAD"//rfp_id(1:1)
            call putgg(fladpak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)


            !--- upward clear-sky shortwave flux
            rfname="SCU"//rfp_id(1:1)
            call putgg(fscupak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- downward clear-sky shortwave flux
            rfname="SCD"//rfp_id(1:1)
            call putgg(fscdpak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- upward clear-sky longwave flux
            rfname="LCU"//rfp_id(1:1)
            call putgg(flcupak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)

            !--- downward clear-sky longwave flux
            rfname="LCD"//rfp_id(1:1)
            call putgg(flcdpak_r(1,l,nrf),lon1,ilat,khem,npgg, &
                  k,nupr,nc4to8(rfname(1:4)),lrp(l),gll,wrks)
         end do ! l

         rdtpam_r(:,:,nrf) = rdtpak_r(:,:,nrf)
         call pkzeros2(rdtpak_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(fsaupak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(fsadpak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(flaupak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(fladpak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(fscupak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(fscdpak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(flcupak_r(1,1,nrf),ijpak,ilev+2)
         call pkzeros2(flcdpak_r(1,1,nrf),ijpak,ilev+2)
       end do ! nrf

      end if
#endif
#if defined tprhs

!     * save physics temperature tendency every israd steps and
!     * reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(ttppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8(" TTP"),lh(l),gll,wrks)
  end do ! loop 565
          call pkzeros2(ttppak,ijpak,ilev)
      end if
#endif
#if defined qprhs

!     * save physics moisture tendency every israd steps and
!     * reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(qtppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8(" QTP"),lh(l),gll,wrks)
  end do ! loop 566
          call pkzeros2(qtppak,ijpak,ilev)
      end if
#endif
#if defined uprhs

!     * save physics u-wind tendency every israd steps and
!     * reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(utppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8(" UTP"),ls(l),gll,wrks)
  end do ! loop 567
          call pkzeros2(utppak,ijpak,ilev)
      end if
#endif
#if defined vprhs

!     * save physics v-wind tendency every israd steps and
!     * reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(vtppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8(" VTP"),ls(l),gll,wrks)
  end do ! loop 568
          call pkzeros2(vtppak,ijpak,ilev)
      end if
#endif
#if defined qconsav

!     * save physics moisture correction tendency every israd steps and
!     * reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          call putgg(qaddpak(1,1),lon1,ilat,khem,npgg,k,nutd, &
                         nc4to8("QADD"),1,gll,wrks)
          call putgg(qaddpak(1,2),lon1,ilat,khem,npgg,k,nutd, &
                         nc4to8("QADD"),2,gll,wrks)
          call putgg(qtphpak(1,1),lon1,ilat,khem,npgg,k,nutd, &
                         nc4to8("QTPH"),1,gll,wrks)
          call putgg(qtphpak(1,2),lon1,ilat,khem,npgg,k,nutd, &
                         nc4to8("QTPH"),2,gll,wrks)
          do l=1,ilev
              call putgg(qtpfpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("QTPF"),lh(l),gll,wrks)
  end do ! loop 569
          call pkzeros2(qaddpak,ijpak,   2)
          call pkzeros2(qtphpak,ijpak,   2)
          call pkzeros2(qtpfpak,ijpak,ilev)
      end if
#endif
#if defined tprhsc

!     * save physics component temperature tendencies every israd steps
!     * and reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(ttpvpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPV"),lh(l),gll,wrks)
              call putgg(ttpmpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPM"),lh(l),gll,wrks)
              call putgg(ttpcpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPC"),lh(l),gll,wrks)
              call putgg(ttpppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPP"),lh(l),gll,wrks)
              call putgg(ttpkpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPK"),lh(l),gll,wrks)
              call putgg(ttpnpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPN"),lh(l),gll,wrks)
  end do ! loop 570

          call pkzeros2(ttpvpak,ijpak,ilev)
          call pkzeros2(ttpmpak,ijpak,ilev)
          call pkzeros2(ttpcpak,ijpak,ilev)
          call pkzeros2(ttpppak,ijpak,ilev)
          call pkzeros2(ttpkpak,ijpak,ilev)
          call pkzeros2(ttpnpak,ijpak,ilev)
       end if
#endif
#if (defined (tprhsc) || defined (radforce))
      if (lsrad) then
          k = kount

          do l=1,ilev
              call putgg(ttpspak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPS"),lh(l),gll,wrks)
              call putgg(ttplpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTPL"),lh(l),gll,wrks)
              call putgg(ttscpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTSC"),lh(l),gll,wrks)
              call putgg(ttlcpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("TTLC"),lh(l),gll,wrks)
          end do ! l

          call pkzeros2(ttpspak,ijpak,ilev)
          call pkzeros2(ttplpak,ijpak,ilev)
          call pkzeros2(ttscpak,ijpak,ilev)
          call pkzeros2(ttscpak,ijpak,ilev)
       end if
#endif

#if defined (radforce)
!     * save the heating rate diagnostic calculations which will allow attribution
!     * of heating rates to different components.
!     * since it is requested for volmip also save the fluxes at the surface and
!     * toa.
       if (lsgg) then
          k = kount

!     * the unperturbed heating rates and flux calculations are saved here.
!     * this is a bit wasteful since the heating rate is saved to the file at
!     * lsgg frequency.
!     * but this is required since they need to be sampled at exactly the same
!     * frequency for the instantaneous calculations.

           do l=1,ilev

            call putgg(hrlpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("HRLR"),lh(l),gll,wrks)
            call putgg(hrspak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("HRSR"),lh(l),gll,wrks)
            call putgg(hrlcpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("HLCR"),lh(l),gll,wrks)
            call putgg(hrscpak(1,l),lon1,ilat,khem,npgg,k,nupr, &
                                 nc4to8("HSCR"),lh(l),gll,wrks)

          end do ! l

          call putgg(fsrpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSRR"),1,gll,wrks)
          call putgg(fsrcpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FRCR"),1,gll,wrks)
          call putgg(fsopal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSOR"),1,gll,wrks)
          call putgg(olrpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("OLRR"),1,gll,wrks)
          call putgg(olrcpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("OLCR"),1,gll,wrks)
          call putgg(fslopal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSLR"),1,gll,wrks)
          call putgg(fsgpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSGR"),1,gll,wrks)
          call putgg(csbpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FCSR"),1,gll,wrks)
          call putgg(flgpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FLGR"),1,gll,wrks)
          call putgg(clbpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FCLR"),1,gll,wrks)
          call putgg(fsspal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSSR"),1,gll,wrks)
          call putgg(fdlpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FDLR"),1,gll,wrks)
          call putgg(fsscpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FSCR"),1,gll,wrks)
          call putgg(fdlcpal,lon1,ilat,khem,npgg,k,nupr, &
                  nc4to8("FLCR"),1,gll,wrks)

          do nrf=1,nrfp

           write(rfp_id,'(I1.1)') nrf

           do l=1,ilev

            rfname="HRS"//rfp_id(1:1)
            call putgg(hrspak_r(1,l,nrf),lon1,ilat,khem,npgg,k,nupr, &
                    nc4to8(rfname(1:4)),lh(l),gll,wrks)

            rfname="HRL"//rfp_id(1:1)
            call putgg(hrlpak_r(1,l,nrf),lon1,ilat,khem,npgg,k,nupr, &
                    nc4to8(rfname(1:4)),lh(l),gll,wrks)

            rfname="HSC"//rfp_id(1:1)
            call putgg(hrscpak_r(1,l,nrf),lon1,ilat,khem,npgg,k,nupr, &
                    nc4to8(rfname(1:4)),lh(l),gll,wrks)

            rfname="HLC"//rfp_id(1:1)
            call putgg(hrlcpak_r(1,l,nrf),lon1,ilat,khem,npgg,k,nupr, &
                    nc4to8(rfname(1:4)),lh(l),gll,wrks)

         end do ! l

         rfname="FSR"//rfp_id(1:1)
         call putgg(fsrpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FRC"//rfp_id(1:1)
         call putgg(fsrcpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FSO"//rfp_id(1:1)
         call putgg(fsopal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="OLR"//rfp_id(1:1)
         call putgg(olrpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="OLC"//rfp_id(1:1)
         call putgg(olrcpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FSL"//rfp_id(1:1)
         call putgg(fslopal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)

         rfname="FSG"//rfp_id(1:1)
         call putgg(fsgpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FCS"//rfp_id(1:1)
         call putgg(csbpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FLG"//rfp_id(1:1)
         call putgg(flgpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FCL"//rfp_id(1:1)
         call putgg(clbpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)

         rfname="FSS"//rfp_id(1:1)
         call putgg(fsspal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FDL"//rfp_id(1:1)
         call putgg(fdlpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FSC"//rfp_id(1:1)
         call putgg(fsscpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
         rfname="FLC"//rfp_id(1:1)
         call putgg(fdlcpal_r(1,nrf),lon1,ilat,khem,npgg,k,nupr, &
                 nc4to8(rfname(1:4)),1,gll,wrks)
       end do ! nrfp

       hrspak_r(:,:,:)  = 0.0
       hrlpak_r(:,:,:)  = 0.0
       hrscpak_r(:,:,:) = 0.0
       hrlcpak_r(:,:,:) = 0.0

      end if
#endif

#if defined qprhsc

!     * save physics component spec. hum. tendencies every israd steps
!     * and reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(qtpvpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("QTPV"),lh(l),gll,wrks)
              call putgg(qtpmpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("QTPM"),lh(l),gll,wrks)
              call putgg(qtpcpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("QTPC"),lh(l),gll,wrks)
              call putgg(qtpppak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("QTPP"),lh(l),gll,wrks)
  end do ! loop 571
          call pkzeros2(qtpvpak,ijpak,ilev)
          call pkzeros2(qtpmpak,ijpak,ilev)
          call pkzeros2(qtpcpak,ijpak,ilev)
          call pkzeros2(qtpppak,ijpak,ilev)
      end if
#endif
#if defined uprhsc

!     * save physics component u-wind tendencies every israd steps
!     * and reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(utpvpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("UTPV"),lh(l),gll,wrks)
              call putgg(utpgpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("UTPG"),lh(l),gll,wrks)
              call putgg(utpcpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("UTPC"),lh(l),gll,wrks)
              call putgg(utpspak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("UTPS"),lh(l),gll,wrks)
              call putgg(utpnpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("UTPN"),lh(l),gll,wrks)
  end do ! loop 572
          call pkzeros2(utpvpak,ijpak,ilev)
          call pkzeros2(utpgpak,ijpak,ilev)
          call pkzeros2(utpcpak,ijpak,ilev)
          call pkzeros2(utpspak,ijpak,ilev)
          call pkzeros2(utpnpak,ijpak,ilev)
      end if
#endif
#if defined vprhsc

!     * save physics component v-wind tendencies every israd steps
!     * and reset accumulated field to zero after saving.

      if (lsrad) then
          k = kount
          do l=1,ilev
              call putgg(vtpvpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("VTPV"),lh(l),gll,wrks)
              call putgg(vtpgpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("VTPG"),lh(l),gll,wrks)
              call putgg(vtpcpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("VTPC"),lh(l),gll,wrks)
              call putgg(vtpspak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("VTPS"),lh(l),gll,wrks)
              call putgg(vtpnpak(1,l),lon1,ilat,khem,npgg,k,nutd, &
                                 nc4to8("VTPN"),lh(l),gll,wrks)
  end do ! loop 573
          call pkzeros2(vtpvpak,ijpak,ilev)
          call pkzeros2(vtpgpak,ijpak,ilev)
          call pkzeros2(vtpcpak,ijpak,ilev)
          call pkzeros2(vtpspak,ijpak,ilev)
          call pkzeros2(vtpnpak,ijpak,ilev)
      end if
#endif

!       * save physics component tracer tendencies every israd steps
!       * and reset accumulated field to zero after saving.

if (lsrad) then
  k = kount
  do n=1,ntrac
#if defined xconsav

              call name2(nam,'PA',n)
              call putgg(xaddpak(1,1,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,1,gll,wrks)
              call putgg(xaddpak(1,2,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,2,gll,wrks)

              call name2(nam,'PH',n)
              call putgg(xtphpak(1,1,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,1,gll,wrks)
              call putgg(xtphpak(1,2,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,2,gll,wrks)
#endif
    do l=1,ilev
#if defined xprhs

              call name2(nam,'PT',n)
              call putgg(xtppak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined xconsav

              call name2(nam,'PF',n)
              call putgg(xtpfpak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined xprhsc

              call name2(nam,'PM',n)
              call putgg(xtpmpak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)

              call name2(nam,'PC',n)
              call putgg(xtpcpak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)

              call name2(nam,'PP',n)
              call putgg(xtpppak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)

              call name2(nam,'PV',n)
              call putgg(xtpvpak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x01

              call name2(nam,'X1',n)
              call putgg(x01pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x02

              call name2(nam,'X2',n)
              call putgg(x02pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x03

              call name2(nam,'X3',n)
              call putgg(x03pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x04

              call name2(nam,'X4',n)
              call putgg(x04pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x05

              call name2(nam,'X5',n)
              call putgg(x05pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x06

              call name2(nam,'X6',n)
              call putgg(x06pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x07

              call name2(nam,'X7',n)
              call putgg(x07pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x08

              call name2(nam,'X8',n)
              call putgg(x08pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
#if defined x09

              call name2(nam,'X9',n)
              call putgg(x09pak(1,l,n),lon1,ilat,khem,npgg,k,nutd, &
                      nam,lh(l),gll,wrks)
#endif
    end do ! loop 574

#if defined xprhs
            call pkzeros2(xtppak(1,1,n),ijpak,ilev)
#endif
#if defined xconsav
            call pkzeros2(xaddpak(1,1,n),ijpak,   2)
            call pkzeros2(xtphpak(1,1,n),ijpak,   2)
            call pkzeros2(xtpfpak(1,1,n),ijpak,ilev)
#endif
#if defined xprhsc
            call pkzeros2(xtpvpak(1,1,n),ijpak,ilev)
            call pkzeros2(xtpmpak(1,1,n),ijpak,ilev)
            call pkzeros2(xtpcpak(1,1,n),ijpak,ilev)
            call pkzeros2(xtpppak(1,1,n),ijpak,ilev)
#endif
#if defined x01
            call pkzeros2(x01pak(1,1,n),ijpak,ilev)
#endif
#if defined x02
            call pkzeros2(x02pak(1,1,n),ijpak,ilev)
#endif
#if defined x03
            call pkzeros2(x03pak(1,1,n),ijpak,ilev)
#endif
#if defined x04
            call pkzeros2(x04pak(1,1,n),ijpak,ilev)
#endif
#if defined x05
            call pkzeros2(x05pak(1,1,n),ijpak,ilev)
#endif
#if defined x06
            call pkzeros2(x06pak(1,1,n),ijpak,ilev)
#endif
#if defined x07
            call pkzeros2(x07pak(1,1,n),ijpak,ilev)
#endif
#if defined x08
            call pkzeros2(x08pak(1,1,n),ijpak,ilev)
#endif
#if defined x09
            call pkzeros2(x09pak(1,1,n),ijpak,ilev)
#endif
  end do ! loop 575
end if
!
#if defined isavdts
      if (nsecs==0) then
!
!        * re-set "IMDH" back to current value.
!
         imdh=imdhn
      end if
#endif
#ifdef cf_sites
      if (lsst) then
! save the data from a select number of points to file nusite
         k=kount

! write fields to a file

! approach is to gather the fields for each level and then extract
! the data from the fields.  the variable stnpak contains an integer
! which associates a point with a station number.

! go through and write out the fields to nusite

! gcm longitudes
         var_name = "GLON"
         nname = nc4to8(var_name)
         l = 1
         call write_station_info(dlonpak,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! gcm longitudes
         var_name = "GLAT"
         nname = nc4to8(var_name)
         l = 1
         call write_station_info(dlatpak,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2d fields
         l = 1

! 2m temperature
        var_name ="  ST"
        nname = nc4to8(var_name)
        call write_station_info(stpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! skin temperature
         var_name ="  GT"
         nname = nc4to8(var_name)
         call write_station_info(gtpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! mean sea level pressure
         var_name ="PMSL"
         nname = nc4to8(var_name)
         call write_station_info(pmslipal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! surface pressure (to add)
          var_name ="  PS"
          nname = nc4to8(var_name)
          call write_station_info(pspal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2m east wind speed
         var_name ="  SU"
         nname = nc4to8(var_name)
         call write_station_info(supal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2m north wind speed
         var_name ="  SV"
         nname = nc4to8(var_name)
         call write_station_info(svpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2m wind speed
         var_name ="  WS"
         nname = nc4to8(var_name)
         call write_station_info(swpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2m relative humidity
         var_name =" SRH"
         nname = nc4to8(var_name)
         call write_station_info(srhpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 2m specific humidity
         var_name ="  SQ"
         nname = nc4to8(var_name)
         call write_station_info(sqpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! total precipitation
         var_name =" PCP"
         nname = nc4to8(var_name)
         call write_station_info(pcppal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! snow
         var_name ="PCPN"
         nname = nc4to8(var_name)
         call write_station_info(pcpnpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! convective precipitation
         var_name ="PCPC"
         nname = nc4to8(var_name)
         call write_station_info(pcpcpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! surface evaporation
         var_name =" QFS"
         nname = nc4to8(var_name)
         call write_station_info(qfsipal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! surface sublimation
         var_name =" QFN"
         nname = nc4to8(var_name)
         call write_station_info(qfnpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

         var_name ="QFVF"
         nname = nc4to8(var_name)
         call write_station_info(qfvfpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! downward eastward windstress
         var_name =" UFS"
         nname = nc4to8(var_name)
         call write_station_info(ufspal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! downward northward windstress
         var_name =" VFS"
         nname = nc4to8(var_name)
         call write_station_info(vfspal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! latent heat flux
         var_name =" HFL"
         nname = nc4to8(var_name)
         call write_station_info(hflpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! sensible heat flux
         var_name =" HFS"
         nname = nc4to8(var_name)
         call write_station_info(hfspal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! lw downward at surface
         var_name =" FDL"
         nname = nc4to8(var_name)
         call write_station_info(fdlpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! net lw downward at surface
         var_name =" FLG"
         nname = nc4to8(var_name)
         call write_station_info(flgpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! sw downward at surface
         var_name =" FSS"
         nname = nc4to8(var_name)
         call write_station_info(fsspal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! net sw downward at surface
         var_name =" FSG"
         nname = nc4to8(var_name)
         call write_station_info(fsgpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! clear-sky sw downward at surface
         var_name ="FSSC"
         nname = nc4to8(var_name)
         call write_station_info(fsscpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! clear-sky net sw downward at surface
         var_name ="FSGC"
         nname = nc4to8(var_name)
         call write_station_info(fsgcpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! clear-sky lw downward at surface
         var_name ="FDLC"
         nname = nc4to8(var_name)
         call write_station_info(fdlcpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! sw downward at toa
         var_name =" FSO"
         nname = nc4to8(var_name)
         call write_station_info(fsopal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! sw upward at toa
         var_name =" FSR"
         nname = nc4to8(var_name)
         call write_station_info(fsrpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! lw upward at toa
         var_name =" OLR"
         nname = nc4to8(var_name)
         call write_station_info(olrpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! clear-sky lw upward at toa
         var_name ="OLRC"
         nname = nc4to8(var_name)
         call write_station_info(olrcpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! clear-sky sw upward at toa
         var_name ="FSRC"
         nname = nc4to8(var_name)
         call write_station_info(srcpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! precipitable water
         var_name ="PWAT"
         nname = nc4to8(var_name)
         call write_station_info(pwatpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! total cloud fraction
         var_name ="CLDT"
         nname = nc4to8(var_name)
         call write_station_info(cldtpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! liquid cloud water path (grid-mean)
         var_name ="CLWT"
         nname = nc4to8(var_name)
         call write_station_info(clwtpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! ice cloud water path (grid-mean)
         var_name ="CICT"
         nname = nc4to8(var_name)
         call write_station_info(cictpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! net radiative flux (sw+lw) at toa
         var_name ="BALT"
         nname = nc4to8(var_name)
         call write_station_info(baltpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! pressure at convection base
         var_name =" BCD"
         nname = nc4to8(var_name)
         call write_station_info(bcdpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! pressure at convection top
         var_name =" TCD"
         nname = nc4to8(var_name)
         call write_station_info(tcdpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! fraction of time convection occurs
         var_name ="CDCB"
         nname = nc4to8(var_name)
         call write_station_info(cdcbpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! fraction of time shallow convection occurs
         var_name ="CSCB"
         nname = nc4to8(var_name)
         call write_station_info(cscbpal,cf_site_ind,nname,kount, & ! input
 lon1,ilat,l,num_sites,nusite)

! 3d fields
      do l = 1, ilev

! cloud amount
         var_name =" CLD"
         nname = nc4to8(var_name)
         call write_station_info(cldpak(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! liquid cloud water (grid mean)
         var_name =" CLW"
         nname = nc4to8(var_name)
         call write_station_info(clwpak(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! ice cloud water (grid mean)
         var_name =" CIC"
         nname = nc4to8(var_name)
         call write_station_info(cicpak(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! net convective mass flux
         var_name =" DMC"
         nname = nc4to8(var_name)
         call write_station_info(dmcpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature
         var_name ="   T"
         nname = nc4to8(var_name)
         call write_station_info(tapal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! eastward wind
         var_name ="   U"
         nname = nc4to8(var_name)
         call write_station_info(uapal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! northward wind
         var_name ="   V"
         nname = nc4to8(var_name)
         call write_station_info(vapal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! specific humidity
         var_name ="   Q"
         nname = nc4to8(var_name)
         call write_station_info(qapal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! relative humidity
         var_name ="  RH"
         nname = nc4to8(var_name)
         call write_station_info(rhpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! omega
         var_name ="OMET"
         nname = nc4to8(var_name)
         call write_station_info(ometpak(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! geopotential height
         var_name ="  ZG"
         nname = nc4to8(var_name)
         call write_station_info(zgpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! eddy viscosity coefficient for momentum
         var_name =" RKM"
         nname = nc4to8(var_name)
         call write_station_info(rkmpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! eddy viscosity coefficient for temperature
         var_name =" RKH"
         nname = nc4to8(var_name)
         call write_station_info(rkhpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! clear-sky lw heating
         var_name ="TTLC"
         nname = nc4to8(var_name)
         call write_station_info(ttlcpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! clear-sky sw heating
         var_name ="TTSC"
         nname = nc4to8(var_name)
         call write_station_info(ttscpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! stratiform liquid cloud effective radius
         var_name ="RLIQ"
         nname = nc4to8(var_name)
         call write_station_info(rliqpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="CLIQ"
         nname = nc4to8(var_name)
         call write_station_info(cliqpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! stratiform ice cloud effective radius
         var_name ="RICE"
         nname = nc4to8(var_name)
         call write_station_info(ricepal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="CICE"
         nname = nc4to8(var_name)
         call write_station_info(cicepal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature tendency due to physics
         var_name =" TTP"
         nname = nc4to8(var_name)
         call write_station_info(ttppal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature tendency due to strat. cloud and pbl mixing
         var_name ="TTPP"
         nname = nc4to8(var_name)
         call write_station_info(ttpppal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="TTPV"
         nname = nc4to8(var_name)
         call write_station_info(ttpvpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature tendency due to lw heating
         var_name ="TTPL"
         nname = nc4to8(var_name)
         call write_station_info(ttplpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature tendency due to sw heating
         var_name ="TTPS"
         nname = nc4to8(var_name)
         call write_station_info(ttpspal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! temperature tendency due to convection
         var_name ="TTPC"
         nname = nc4to8(var_name)
         call write_station_info(ttpcpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="TTPM"
         nname = nc4to8(var_name)
         call write_station_info(ttpmpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! specific humidity tendency due to convection
         var_name ="QTPC"
         nname = nc4to8(var_name)
         call write_station_info(qtpcpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="QTPM"
         nname = nc4to8(var_name)
         call write_station_info(qtpmpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! specific humidity tendency due to strat. cloud and pbl mixing
         var_name ="QTPP"
         nname = nc4to8(var_name)
         call write_station_info(qtpppal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

         var_name ="QTPV"
         nname = nc4to8(var_name)
         call write_station_info(qtpvpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)

! specific humidity tendency due to physics
         var_name =" QTP"
         nname = nc4to8(var_name)
         call write_station_info(qtppal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lh(l),num_sites,nusite)
       end do  ! l

! 3d radiative flux fields
      do l = 1, ilev+2
! lw upward
         var_name ="FLAU"
         nname = nc4to8(var_name)
         call write_station_info(flaupal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

! sw upward
         var_name ="FSAU"
         nname = nc4to8(var_name)
         call write_station_info(fsaupal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

! clear-sky lw upward
         var_name ="FLCU"
         nname = nc4to8(var_name)
         call write_station_info(flcupal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

! clear-sky sw upward
         var_name ="FSCU"
         nname = nc4to8(var_name)
         call write_station_info(fscupal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

! clear-sky lw downward
         var_name ="FLCD"
         nname = nc4to8(var_name)
         call write_station_info(flcdpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

! clear-sky sw downward
         var_name ="FSCD"
         nname = nc4to8(var_name)
         call write_station_info(fscdpal(1,l),cf_site_ind,nname,kount, & ! input
 lon1,ilat,lrp(l),num_sites,nusite)

       end do ! l

      end if ! lsst

#endif ! cf_sites
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
