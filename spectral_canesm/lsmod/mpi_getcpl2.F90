#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine mpi_getcpl2(ggpak,nlon,nlat,nl,tag)
  !
  !     * apr 26/10 - m.lazare.     new version for gcm15i:
  !     *                           - remove unnecessary call
  !     *                             to taginfo_h.
  !     * sep 03/04 - d.robitaille. original version mpi_getcpl.
  !
  use com_cpl, only: recv_fld
  implicit none
  integer :: iday
  integer:: ilat
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon

  !
  include 'mpif.h'
  !
  integer, intent(in), dimension(3) :: tag !< Variable description\f$[units]\f$
  integer :: ibuf(8) !<
  integer :: k !<

  real, intent(in), dimension(1) :: ggpak !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer, intent(in) :: nl !<

  real, allocatable , dimension(:) :: gg !<

  !
  common /mpilat/ ilat

  integer*4 :: mynode !<
  common /mpinfo/ mynode

  !     * common block to hold the number of nodes and communicator.

  integer*4 :: nnode !<
  integer*4 :: agcm_commwrld !<
  integer*4 :: my_cmplx_type !<
  integer*4 :: my_real_type !<
  integer*4 :: my_int_type !<
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  integer*4 :: sz !<
  integer*4 :: master !<

#include "coupling_h.h"
  !-----------------------------------------------------------------------

  master = 0
  sz     = tag(3)

  allocate (gg(tag(3)))

  if (mynode == 0) then
    call recv_fld (gg, ibuf, coupler, tag)

    if (ibuf(5)/=nlon .or. ibuf(6)/=nlat) then
      !
      !     * error exit - stop if grid is the wrong size.
      !
      900 write(6,6020) iday,tag(2),nlon,nlat
      call xit('MPI_GETCPL2',-1)
    end if

  end if

  if (nnode  > 1) then
#if defined use_canam_32bit
          call mpi_bcast(gg, sz, mpi_double_precision, &
                  master, agcm_commwrld, ierr)
#else
          call mpi_bcast(gg, sz, my_real_type, &
                  master, agcm_commwrld, ierr)
#endif
  end if

  !
  !     * convert to ordered s-n pairs.
  !
  do k = 1, nl
    call rgtocg2_cpl(ggpak((k-1)*(nlon-1)*ilat+1:k*(nlon-1)*ilat), &
                         gg((k-1)*nlon*nlat+1:k*nlon*nlat), &
                         nlon-1,nlat,ilat)
  end do

  deallocate(gg)

  return
  !-----------------------------------------------------------------------
  6020 format('0GRID',i10,1x,i6,5x,'SIZE',2i6,5x,'NOT FOUND')
end

subroutine rgtocg2_cpl(pako,paki,lon,nlat,ilat)

  !     * june 30, 2003 - m.lazare.
  !
  !     * this routine takes a normal s-n grid field, "PAKI"
  !     * and converts it into ordered s-n pairs, "PAKO",
  !     * which are used within the model ** on each node **.

  !     * ** note ** the cyclic longitude is stripped off !
  !     * ** note ** the variable "MYNODE" passed through the
  !     *            common deck "MPINFO" is used to select the
  !     *            chunk for a given node.

  !     * this routine is typically called when reading-in external
  !     * fields from the "AN" file.
  !     * "RGTOCG" should ** not ** be used.
  !
  implicit none
  integer :: i
  integer, intent(in) :: ilat
  integer :: j
  integer :: jend
  integer :: jne
  integer :: jnp
  integer :: jse
  integer :: jsp
  integer :: jstart
  integer, intent(in) :: lon
  integer :: ni
  integer :: nl
  integer, intent(in) :: nlat
  integer :: nlath
  integer :: nlatq
  !
  real, intent(inout) :: pako(lon*ilat) !<
  real, intent(in) :: paki(lon+1,nlat) !<
  integer*4 :: mynode !<
  logical :: lrcm !<
  !
  common /mpinfo/ mynode
  common /model/  lrcm
  !==================================================================
  if (.not.lrcm) then
    !
    !**************************************************************************
    !     * gcm with reorganized latitudes and possible mpi.
    !**************************************************************************
    !
    !      * "NI" represents the counter for each grid point written into "PAKO".
    !      * "NL" represents the counter for latitude in the model structure
    !      * (ie s-n quartets starting at poles,equator).
    !
    ni=0
    nl=0
    if (mod(nlat,4)/=0)       call xit('RGTOCG2_CPL',-1)
    nlath=nlat/2
    nlatq=nlat/4
    !
    !      * mpi hook.
    !
    jstart=mynode*ilat+1
    jend  =mynode*ilat+ilat
    !
    do j=1,nlatq
      jsp=j
      nl=nl+1
      if (nl>=jstart .and. nl<=jend) then
        do i=1,lon
          ni=ni+1
          pako(ni) = paki (i,jsp)
        end do
      end if
      !
      jnp=nlat-j+1
      nl=nl+1
      if (nl>=jstart .and. nl<=jend) then
        do i=1,lon
          ni=ni+1
          pako(ni) = paki (i,jnp)
        end do
      end if
      !
      jse=nlath-j+1
      nl=nl+1
      if (nl>=jstart .and. nl<=jend) then
        do i=1,lon
          ni=ni+1
          pako(ni) = paki (i,jse)
        end do
      end if
      !
      jne=nlath+j
      nl=nl+1
      if (nl>=jstart .and. nl<=jend) then
        do i=1,lon
          ni=ni+1
          pako(ni) = paki (i,jne)
        end do
      end if
    end do
    !
  else
    !
    !**************************************************************************
    !      * rcm with usual latitudes and no mpi.
    !**************************************************************************
    !
    ni=0
    do j=1,nlat
      do i=1,lon
        ni=ni+1
        pako(ni)=paki(i,j)
      end do
    end do
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
